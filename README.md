# Fallenswen - New Lands
The isometric strategy inspired by Stronghold and Empire Earth series

### Technology
C++17, SFML 2.5.0, Lua, Sol2

### CMAKE
Create file **/cmake-modules/Config.cmake** and set your SFML ROOT directory inside, eg.
```cmake
set(SFML_ROOT ../SFML-2.5.0/)
set(LUA_ROOT ../Lua-5.1/)
set(SOL2_ROOT ../Sol2/)
set(IMGUI_ROOT ../imgui/) 

#For windows
set(LUA_LIBRARY ${LUA_ROOT}liblua5.1.a)
```
After this click File -> Reload CMake Project in CLion

### Code style
```cpp
// Header
/**
 * Class for sth, doing sth - I think, we can write it when it's really necessary
 */

class ClassName {
private:
    int foo;
    
protected:
    char foo2;
    
public:
    int func(int a, char b);
}

// Source
#include "sth.h"

/**
 * This function do sth - this description is optional, required only with some big chunk of code 
 * @param int a 
 * @param char b 
 * @return int
 */
int ClassName::func(int a, char b) {
    if (sth == sth2) {
        sth3();
    }
    
    else if (foo != foo2) {
        sth4();
    }
    
    for (int i = 0; i < 10; i++) {
        foo[i] = 5;
    }
}

// TAB - 4 spaces
// Hard wrap at 120 characters (Editor -> Code Style -> Hard wrap at)

/**
 * This is really long description for out thisIsReallyLongFunction() function. This is really long description for
 * our thisIsReallyLongFunction() function.
 */
int thisIsReallyLongFunction(int functionArgument1, int functionArgument2, int functionArgument3, int functionArgument4,
                             int functionArgument5, int functionArgument6) {
    doSth();
}

```