#pragma once
#include <inttypes.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>

#include <vector>

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Rect.hpp>

typedef uint32_t uint32;
typedef uint16_t uint16;
typedef uint8_t uint8;

typedef int32_t int32;
typedef int16_t int16;
typedef int8_t int8;

inline sf::Vector2f vec2(float x, float y) {
    return sf::Vector2f(x, y);
}

inline sf::Vector2f rectPos(const sf::FloatRect &rect) {
    return sf::Vector2f(rect.left, rect.top);
}

inline sf::Vector2f rectSize(const sf::FloatRect &rect) {
    return sf::Vector2f(rect.width, rect.height);
}

inline sf::FloatRect Rect(float x, float y, float w, float h) {
    return sf::FloatRect(x, y, w, h);
}

inline sf::FloatRect Rect(const sf::Vector2f &position, const sf::Vector2f &size) {
    return sf::FloatRect(position, size);
}

bool testOverlap(const sf::FloatRect &a, const sf::FloatRect &b);
