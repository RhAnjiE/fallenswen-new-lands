#ifndef IZOGAME_PLAYERDATA_H
#define IZOGAME_PLAYERDATA_H

#include <SFML/Graphics.hpp>
#include "../maps/objects/people/Human.h"
#include "../gui/hud/MessageLog.h"

class MapBuilding;
class MapCityHall;
class Map;
class Hud;

struct ModifiableData {
    unsigned int level = 1;
    float efficiency = 2;
};

class PlayerData {
public:
    PlayerData(sf::Color color, int money, Map* mapHandle, Hud* hudHandle);

    void update();
    void reset();

    static void registerForLua();

    void addMoney(int money);
    void addPeople(int people);
    void addMaxPeople(int people);
    void addBasicBuildings();

    void registerBuilding(MapBuilding* building);
    void deleteBuilding(MapBuilding* building);

    //TODO: Better resource system
    void setInitialResources();
    int getResource(const std::string& id);
    void addResource(const std::string& id, int amount);
    void setResource(const std::string& id, int amount);

    void modifyFoodConsuming(int percent);
    void modifyFoodConsumingInWinter(int percent);

    float getFoodConsuming();

    void addHuman(const std::string& id, Human* human);
    void removeHuman(const std::string& id);

    void levelUpBuildings(const std::string& type = "");
    void updateEfficiency(const std::string& type, int value);
    void addNewBuilding(const std::string& type);

    sf::Color getColor();
    int getMoney();
    int getPeople();
    int getMaxPeople();

    MapBuilding* getCityHall();
    Map* getMapHandle();
    MessageLog &getMessageLog();

    //TODO: Add getter
    std::map<std::string, Human*>humans;
protected:
    MessageLog messageLog;
    std::vector<MapBuilding*>buildings;
    //std::map<std::string, Human*>humans;
    sf::Color color = sf::Color::White;

    MapBuilding* cityHall = nullptr;
    Map* mapHandle = nullptr;
    Hud* hudHandle = nullptr;

    std::map<std::string, int>resources;
    int money = 0;
    int people = 0;
    int maxPeople = 5;

    float normalMultiplier = 4;
    float winterMultiplier = 0.2;

    std::map<std::string, ModifiableData>buildingsData;
    void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif //IZOGAME_PLAYERDATA_H
