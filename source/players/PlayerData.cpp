#include "PlayerData.h"
#include "../maps/objects/buildings/MapBuilding.h"
#include "../gui/hud/Hud.h"

PlayerData::PlayerData(sf::Color color, int money, Map* mapHandle, Hud* hudHandle) {
    this->color = color;

    this->mapHandle = mapHandle;
    this->hudHandle = hudHandle;

    this->setInitialResources();
}

void PlayerData::registerForLua() {
    sol::state& lua = *ResourceManager::getInstance().getLuaHandle();
    lua.new_usertype<PlayerData>(
            "PlayerData", sol::constructors<>(),
            "money", sol::readonly_property(&PlayerData::getMoney),
            "people", sol::readonly_property(&PlayerData::getPeople),
            "maxPeople", sol::readonly_property(&PlayerData::getMaxPeople),
            "foodMultiplier", sol::readonly_property(&PlayerData::getFoodConsuming),
            "cityHall", sol::readonly_property(&PlayerData::getCityHall),
            "messageLog", sol::readonly_property(&PlayerData::getMessageLog),

            "addBasicBuildings", &PlayerData::addBasicBuildings,
            "addNewBuilding", &PlayerData::addNewBuilding,
            "levelUpBuildings", &PlayerData::levelUpBuildings,

            "getResource", &PlayerData::getResource,
            "addResource", &PlayerData::addResource,
            "setResource", &PlayerData::setResource,

            "updateEfficiency", &PlayerData::updateEfficiency,
            "addMoney", &PlayerData::addMoney,
            "addPeople", &PlayerData::addPeople,
            "addMaxPeople", &PlayerData::addMaxPeople,
            "modifyFoodConsuming", &PlayerData::modifyFoodConsuming,
            "modifyFoodConsumingInWinter", &PlayerData::modifyFoodConsumingInWinter
            //TODO: Register all properties and methods
    );

    //TODO: Problem with message arguments
    lua.set_function("pushMessage", [](MessageLog& messageLog, std::string format, sol::variadic_args args) {
        messageLog.pushMessage(format.c_str(), sol::as_args(args));
    });
}

void PlayerData::setInitialResources() {
    money = 0;

    //Building resources
    resources["wood"] = 1000;
    resources["stone"] = 1000;

    //Other resources
    resources["wheat"] = 0;
    resources["flour"] = 0;
    resources["food"] = 60;
}

void PlayerData::update() {
    for(auto building : buildings)
        building->update();
}

void PlayerData::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    /*for(auto human : humans)
        target.draw(*human);*/
}

void PlayerData::registerBuilding(MapBuilding* building) {
    bool success = false;

    if (building->getType() == "cityHall" && cityHall == nullptr) {
        cityHall = building;
        success = true;
    }

    else if (building->getType() != "cityHall" && cityHall != nullptr)
        success = true;


    if (success) {
        building->init();
        buildings.emplace_back(building);

        std::string type = building->getType();
        if (buildingsData.find(type) != buildingsData.end()) {
            buildings.back()->setLevel(buildingsData[type].level);
            buildings.back()->setEfficiency(buildingsData[type].efficiency);
        }
    }
}

void PlayerData::deleteBuilding(MapBuilding *building) {
    for (int i = 0; i < buildings.size(); i++) {
        if (buildings[i] == building) {
            buildings.erase(buildings.begin() + i);

            return;
        }
    }
}

void PlayerData::addHuman(const std::string& id, Human* human) {
    humans[id] = human;
}

void PlayerData::removeHuman(const std::string& id) {
    if (humans.find(id) != humans.end())
        humans.erase(id);
}

void PlayerData::addMoney(int money) {
    this->money += money;
}

void PlayerData::addPeople(int people) {
    this->people += people;
}

void PlayerData::addMaxPeople(int people) {
    this->maxPeople += people;
}

sf::Color PlayerData::getColor() {
    return color;
}

int PlayerData::getMoney() {
    return money;
}

int PlayerData::getPeople() {
    return people;
}

int PlayerData::getMaxPeople() {
    return maxPeople;
}

int PlayerData::getResource(const std::string& id) {
    if (resources.find(id) == resources.end()) {
        Logger::error("Resource with given id not found!");

        return -1;
    }

    return resources[id];
}

void PlayerData::addResource(const std::string& id, int amount) {
    if (resources.find(id) != resources.end())
        resources[id] += amount;
}

void PlayerData::setResource(const std::string& id, int amount) {
    if (resources.find(id) != resources.end())
        resources[id] = amount;
}

MapBuilding* PlayerData::getCityHall() {
    return cityHall;
}

Map *PlayerData::getMapHandle() {
    return mapHandle;
}

void PlayerData::reset() {
    buildings.clear();
    humans.clear();
    buildingsData.clear();

    cityHall = nullptr;
}

MessageLog &PlayerData::getMessageLog() {
    return messageLog;
}

void PlayerData::levelUpBuildings(const std::string &type) {
    if (type.empty()) {
        for(auto& [id, data] : buildingsData) {
            data.level++;
        }
    }

    else if (buildingsData.find(type) != buildingsData.end()) {
        buildingsData[type].level++;
    }

    for (auto& building : buildings) {
        if(type.empty() || building->getType() == type) {
            building->levelUp();
        }
    }
}

void PlayerData::updateEfficiency(const std::string &type, int value) {
    if (buildingsData.find(type) != buildingsData.end()) {
        buildingsData[type].efficiency -= ((float)value / 100);
    }

    for (auto& building : buildings) {
        if (type.empty() || type == building->getType()) {
            building->modifyEfficiency(-((float)value / 100));
        }
    }
}

void PlayerData::addNewBuilding(const std::string& type) {
    if (buildingsData.find(type) == buildingsData.end()) {
        buildingsData[type] = ModifiableData();

        std::string iconId = "icon";
        sol::state* lua = ResourceManager::getInstance().getLuaHandle();
        if ((*lua)[type] != sol::nil) {
            sol::table data = (*lua)[type];

            if (data["iconTexture"] != sol::nil)
                iconId = data["iconTexture"];
        }

        hudHandle->pushBuildMenuItem(type, iconId);
    }
}

void PlayerData::addBasicBuildings() {
    hudHandle->resetBuildingsList();

    this->addNewBuilding("house");
    this->addNewBuilding("woodCamp");
}

void PlayerData::modifyFoodConsuming(int percent) {
    normalMultiplier += (percent / 100);

    if (normalMultiplier < 0.2)
        normalMultiplier = 0.2;
}

void PlayerData::modifyFoodConsumingInWinter(int percent) {
    winterMultiplier += (percent / 100);

    if (winterMultiplier < 0.2)
        winterMultiplier = 0.2;
}

float PlayerData::getFoodConsuming() {
    return normalMultiplier;
}
