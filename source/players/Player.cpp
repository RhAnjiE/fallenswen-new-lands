#include "Player.h"

Player::Player(sf::Color color, int money, Map* mapHandle, Hud* hudHandle)
    : PlayerData(color, money, mapHandle, hudHandle) {


}

void Player::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    PlayerData::draw(target, states);
}
