#ifndef IZOGAME_PLAYER_H
#define IZOGAME_PLAYER_H

#include "PlayerData.h"

//TODO:
class Player final: public PlayerData, public sf::Drawable {
public:
    Player(sf::Color color, int money, Map* mapHandle, Hud* hudHandle);


private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};

#endif //IZOGAME_PLAYER_H
