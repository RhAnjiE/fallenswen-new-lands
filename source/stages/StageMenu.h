#ifndef IZOGAME_STAGEMENU_H
#define IZOGAME_STAGEMENU_H

#include "Stage.h"
#include "../maps/Map.h"

class StageMenu: public Stage {
public:
    explicit StageMenu(sf::RenderWindow* window);

	void start() override;
	void exit() override;
    void pollEvent(GameManager &gameManager) override;
    void serve(GameManager &gameManager) override;
private:
    //...
	sf::Font font;
	sf::Text text;

	// Input
	bool right_pressed = false;
	bool left_pressed = false;

	// Map Configuration
	std::pair<std::string, Map::Size> mapSize = std::make_pair("SMALL", Map::SMALL);
    std::pair<std::string, Map::Type> terrainType = std::make_pair("ISLANDS", Map::ISLANDS);
    std::pair<std::string, Map::ResourcesNum> resourcesNum = std::make_pair("LITTLE", Map::LITTLE);

	int mapSizeSelection = 0;
	int terrainTypeSelection = 0;
	int numOfResSelection = 0;

	// Animation
	float animation = 0.0f;

	// Layout Group
	float text_y_offset = 0.0f;
	int text_id = 0;
	int selection = 0;
	
	void setMapConfiguration();
	int pushMenuElement(sf::String string, int element_selection, int max,
		int characterSize = 64);
	void drawMenu();
};

#endif //IZOGAME_STAGEMENU_H
