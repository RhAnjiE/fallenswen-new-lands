	#include "StageGameplay.h"

#include "../GameManager.h"

#include <imgui.h>
#include <imgui-SFML.h>

StageGameplay::StageGameplay(sf::RenderWindow *window)
    : Stage(window)
    , player(sf::Color::Red, 100, &map, &hud)
    , map(Map::SMALL, Map::ISLANDS, Map::LITTLE, &player, &hud) {

    normalView = window->getDefaultView();

    UICreateState();

    UILoadFont("assets/fonts/Inconsolata-Regular.ttf");
	UISetProjection(1366, 768);
}

void StageGameplay::start() {
	this->resetGameState();

	normalView.setSize(sf::Vector2f(window->getSize()));
    normalView.setCenter(window->getSize().x / 2, window->getSize().y / 2);

	camera.getView().setSize(sf::Vector2f(window->getSize()));
	debug.camera = camera;

	hud.init(map, window->getSize(), player);
    map.getInteraction().setChosenObject("cityHall");

    Logger::debug("Stage changed to gameplay!");
}

void StageGameplay::exit() {

}

void StageGameplay::setMapSettings(GameplayOptions &gameplayOptions) {
    map.setOptions(gameplayOptions.mapSize, gameplayOptions.terrainType, gameplayOptions.resourcesNum);
}

void StageGameplay::pollEvent(GameManager &gameManager) {
    DeltaTime::get().update();

    while (window->pollEvent(event)) {
		ImGui::SFML::ProcessEvent(event);
		UIProcessEvent(event);
		hud.processEvent(event);

        if (event.type == sf::Event::Closed)
            window->close();

        if (event.type == sf::Event::LostFocus)
            isActive = false;

        if (event.type == sf::Event::GainedFocus)
            isActive = true;

        if (event.type == sf::Event::Resized) {
            normalView.setSize(event.size.width, event.size.height);
            normalView.setCenter(event.size.width / 2, event.size.height / 2);

            camera.getView().setSize(event.size.width, event.size.height);
            hud.updateSize(sf::Vector2u(event.size.width, event.size.height));
        }

        if (isActive) {
            windowMousePosition = window->mapPixelToCoords(sf::Mouse::getPosition(*window), normalView);

			if (hud.interact(map, windowMousePosition) == false) {
                (debug.enableDebugCamera ? debug.camera.handleEvent(event) : camera.handleEvent(event));
			}

            if (event.type == sf::Event::MouseButtonPressed && event.mouseButton.button == sf::Mouse::Left) {
                if (hud.interact(map, windowMousePosition) == false)
                    map.interact(player);
            }

            else if (event.type == sf::Event::MouseButtonPressed && event.mouseButton.button == sf::Mouse::Right)
                map.getInteraction().setChosenObject("");

            if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Right) {
                MapObject *object = map.getChosenObject();
                if (object) {
                    map.getInteraction().setChosenObject(object->getType());
                }
            }
        }
    }
}

void StageGameplay::serve(GameManager &gameManager) {
    if (isActive) {
        float deltaTime = DeltaTime::getDt();

        this->updatePauseMenu(gameManager);

		/* IMGUI */
		ImGui::SFML::Update(*window, sf::seconds(DeltaTime::get().getDt()));

		/* UPDATE GAME */
        fixedMousePosition = window->mapPixelToCoords(sf::Mouse::getPosition(*window), camera.getView());
        centerTilePosition = Camera::getMapPosition(sf::Vector2f(camera.getView().getCenter()));        

        hud.getMinimap().moveMinimap((unsigned int)centerTilePosition.x, (unsigned int)centerTilePosition.y);

		this->updateCamera();
        player.update();
        map.update(fixedMousePosition, camera.getView());
        hud.update(deltaTime);

		/* RENDER GAME */
		window->clear(sf::Color(0, 150, 255));

        map.render(*window);
        window->draw(player);

		this->drawCameraRegion();

		/* HUD */
        window->setView(normalView);
        window->draw(hud);

		UIDraw(*window);

		ImGui::SFML::Render(*window);

        window->display();
    }
}

void StageGameplay::resetGameState() {

}

void StageGameplay::updateCamera() {
    if (debug.enableDebugCamera != hud.isEnableDebugCamera()) {
        debug.enableDebugCamera = hud.isEnableDebugCamera();
        debug.camera = camera;
    }

    if (debug.enableDebugCamera) {
        window->setView(debug.camera.getView());

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
            camera.handleInput(fixedMousePosition);

        else debug.camera.handleInput(fixedMousePosition);

        fixedMousePosition = window->mapPixelToCoords(sf::Mouse::getPosition(*window), debug.camera.getView());
        centerTilePosition = Camera::getMapPosition(sf::Vector2f(debug.camera.getView().getCenter()));
    }

    else {
        window->setView(camera.getView());
        camera.handleInput(fixedMousePosition);

        fixedMousePosition = window->mapPixelToCoords(sf::Mouse::getPosition(*window), camera.getView());
        centerTilePosition = Camera::getMapPosition(sf::Vector2f(camera.getView().getCenter()));
    }
}

void StageGameplay::drawCameraRegion() {
    if (debug.enableDebugCamera) {
        sf::View view = camera.getView();

        sf::RectangleShape shape;
        shape.setFillColor(sf::Color(255, 0, 255, 100));
        shape.setSize(view.getSize());
        shape.setPosition(view.getCenter() - shape.getSize() / 2.0f);

        window->draw(shape);
    }
}

void StageGameplay::drawEditor() {
	DeltaTime &dt = DeltaTime::get();
	ImGui::Begin("Debug");
	ImGui::Text("Performance:");
	ImGui::Text("FPS %i", dt.getFps());
	ImGui::Text("DT %f", dt.getDt());

	ImGui::Spacing();
	ImGui::Separator();
	ImGui::Spacing();

	ImGui::Text("Camera");
	if (ImGui::Checkbox("Enable Debug Camera", &debug.enableDebugCamera)) {
		if (debug.enableDebugCamera)
			debug.camera = camera;
	}

	if (ImGui::Checkbox("Culling", &debug.culling)) {
		//TODO: Finish it
	}

	ImGui::End();
}

void StageGameplay::updatePauseMenu(GameManager &gameManager) {
    switch (hud.getMenuButtonStatus()) {
        case Hud::SAVE:
            //gameManager.save("name")
            break;
        case Hud::LOAD:
            //gameManager.load("name")
            break;
        case Hud::MAIN_MENU:
            gameManager.switchToMenu();
            break;
        case Hud::EXIT:
            window->close();
            break;
        default:
            break;
    }
}
