#include "StageMenu.h"

#include "../GameManager.h"

StageMenu::StageMenu(sf::RenderWindow *window): Stage(window) {
	this->start();
}

void StageMenu::start() {
	font.loadFromFile("assets/fonts/VT323-Regular.ttf");

    window->setView(sf::View(sf::FloatRect(0.0f, 0.0f, window->getSize().x, window->getSize().y)));

	Logger::debug("Stage changed to menu!");
}

void StageMenu::exit() {
    //...
}

void StageMenu::pollEvent(GameManager &gameManager) {
    //Update Animation
    DeltaTime::get().update();

    while (window->pollEvent(event)) {
        if (event.type == sf::Event::Closed)
            window->close();

        //Set Correct Aspect Ratio
        if (event.type == sf::Event::Resized)
            window->setView(sf::View(sf::FloatRect(0.0f, 0.0f, event.size.width, event.size.height)));

		if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Return) {
		    GameplayOptions options = {};
		    options.mapSize         = mapSize.second;
		    options.terrainType     = terrainType.second;
		    options.resourcesNum    = resourcesNum.second;

			gameManager.switchToGameplay(options);
		}

		if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::W)
			selection--;
		if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::S)
			selection++;

		if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::D)
			right_pressed = true;
		if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::A)
			left_pressed = true;
    }
}

void StageMenu::serve(GameManager &gameManager) {
	float animation_speed = 1.2f;
	animation += DeltaTime::getDt() * animation_speed;

    // Reset Layout
    text_y_offset = 0.0f;
    text_id = 0;

    // Clamp Selection
    int selection_min = 1, selection_max = 3;
    selection = selection < selection_min ? selection_min
        : selection > selection_max ? selection_max
        : selection;

	// ...
    this->setMapConfiguration();

    window->clear();

    // Draw
    sf::RectangleShape background;

    background.setSize(window->getView().getSize());
    background.setFillColor(sf::Color(0, 150, 255));
    window->draw(background);

    this->drawMenu();
    window->display();

    //window->setView(window->getDefaultView());

    // Reset Input
    right_pressed = false;
    left_pressed = false;

    //ONLY DEBUG
	//gameManager.switchToGameplay(mapSize, terrainType, numOfResources);
}

void StageMenu::setMapConfiguration() {
	std::pair<std::string, Map::Size> mapSizeElements[4] = {
	        std::make_pair("SMALL", Map::SMALL),
	        std::make_pair("MEDIUM", Map::MEDIUM),
	        std::make_pair("BIG", Map::BIG),
	        std::make_pair("HUGE", Map::HUGE),
    };

    std::pair<std::string, Map::Type> terrainTypeElements[4] = {
            std::make_pair("CONTINENTS", Map::CONTINENTS),
            std::make_pair("LAKES", Map::LAKES),
            std::make_pair("ISLANDS", Map::ISLANDS),
            std::make_pair("DESERT", Map::DESERT)
    };

    std::pair<std::string, Map::ResourcesNum> ResNumberElements[3] = {
            std::make_pair("LITTLE", Map::LITTLE),
            std::make_pair("NORMAL", Map::NORMAL),
            std::make_pair("RICH", Map::RICH)
    };

    mapSize = mapSizeElements[mapSizeSelection];
    terrainType = terrainTypeElements[terrainTypeSelection];
    resourcesNum = ResNumberElements[numOfResSelection];
}

int StageMenu::pushMenuElement(sf::String string, int element_selection, int max, int characterSize) {
	text.setOutlineThickness(4.0f);
	text.setCharacterSize(characterSize);
	text.setFont(font);

	if (text_id == selection) {
		string.insert(0, "> ");
		string.insert(string.getSize(), " <");

		text.setCharacterSize(characterSize + 4);

		if (right_pressed) {
			element_selection++;
			if (element_selection > max - 1)
				element_selection = max - 1;
		}

		if (left_pressed) {
			element_selection--;
			if (element_selection < 0)
				element_selection = 0;
		}
	}

	text.setString(string);

	sf::Vector2f windowSize = (sf::Vector2f)window->getView().getSize();
	sf::Vector2f windowHalfSize = windowSize / 2.0f;
	sf::FloatRect textBounds = text.getGlobalBounds();

	sf::Vector2f position;
	position.x = windowHalfSize.x - (textBounds.width / 2.0f);
	position.y = text_y_offset;
	text.setPosition(ceil(position.x), ceil(position.y));

	text_y_offset += text.getLocalBounds().height * 2.0f;
	text_id++;

	window->draw(text);

	return element_selection;
}

void StageMenu::drawMenu() {
	// Title
	pushMenuElement("FALLESWEN: NEW LAND", 0, 0, 150);
	text_y_offset += 40.0f;

	// Configuration
	mapSizeSelection = pushMenuElement("MAP SIZE: " + mapSize.first, mapSizeSelection, 4);
	terrainTypeSelection = pushMenuElement("TERRAIN TYPE: " + terrainType.first, terrainTypeSelection, 4);
    numOfResSelection = pushMenuElement("NUMBER OF RESOURCES: " + resourcesNum.first, numOfResSelection, 3);
	text_y_offset += 150.0f;

	// ...
	pushMenuElement("Press ENTER to start the game!", 0, 85);
}