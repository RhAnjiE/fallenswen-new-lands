#ifndef IZOGAME_STAGEGAMEPLAY_H
#define IZOGAME_STAGEGAMEPLAY_H

#include "Stage.h"
#include "StageMenu.h"
#include "../utilities/Camera.h"
#include "../maps/Map.h"
#include "../players/Player.h"
#include "../gui/hud/Hud.h"
#include "sol.hpp"

#include "../gui/hud/MessageLog.h"

struct GameplayOptions{
    Map::Size mapSize;
    Map::Type terrainType;
    Map::ResourcesNum resourcesNum;
};

class StageGameplay: public Stage {
public:
    explicit StageGameplay(sf::RenderWindow* window);

	void start() override;
	void exit() override;
    void pollEvent(GameManager &gameManager) override;
    void serve(GameManager &gameManager) override;
	void resetGameState();

	void setMapSettings(GameplayOptions &gameplayOptions);

private:
	Camera camera;
    Player player;
    Map map;
    Hud hud;

    sf::View normalView;

    sf::Vector2f fixedMousePosition;
    sf::Vector2f windowMousePosition;
    sf::Vector2i centerTilePosition;

	void drawEditor();
	void drawCameraRegion();
	void updateCamera();
    void updatePauseMenu(GameManager &gameManager);

	struct {
        Camera camera;

		bool enableDebugCamera = false;
		bool culling = true;
	} debug;
};

#endif //IZOGAME_STAGEGAMEPLAY_H
