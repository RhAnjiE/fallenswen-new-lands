#ifndef IZOGAME_GAMEMANAGER_H
#define IZOGAME_GAMEMANAGER_H

#include <SFML/Graphics/RenderWindow.hpp>
#include "utilities/Logger.h"
#include "utilities/graphics/ResourceManager.h"
#include "stages/Stage.h"

#include "stages/StageMenu.h"
#include "stages/StageGameplay.h"

//TODO: How can I remove this declaration?
class Stage;

class GameManager {
    friend Stage;

public:
    GameManager(unsigned int w, unsigned int h, const std::string& title);

    void runGame();
    void setStage(Stage* newStage);
    inline Stage* getStage();
	
	void switchToMenu();
	void switchToGameplay(GameplayOptions &options);
protected:
    sf::RenderWindow window;
    sf::ContextSettings settings;

private:
	StageMenu *menu         = nullptr;
	StageGameplay *gameplay = nullptr;
	Stage *stage            = nullptr;
};

#endif //IZOGAME_GAMEMANAGER_H