#include "Minimap.h"

void Minimap::generateImage(Map &map, sf::Vector2u windowSize) {
    sf::Vector2u mapSize = sf::Vector2u(map.getMapSize());
    MapTile* tile = nullptr;

    image.create(mapSize.x, mapSize.y, sf::Color::White);
    for (unsigned int y = 0; y < mapSize.y; y++) {
        for (unsigned int x = 0; x < mapSize.x; x++) {
            tile = map.getTile(y * mapSize.x + x);

            if (tile != nullptr) {
                this->clearPixel(x, y, tile);

                if (tile->getObjectOnTile() != nullptr)
                    image.setPixel(x, y, sf::Color(0, 150, 0, alpha));
            }
        }
    }

    this->mapSize = mapSize;

    texture = sf::Texture();
    texture.loadFromImage(image);

    sprite = sf::Sprite();
    sprite.setTexture(texture, true);

    this->scale(3);
    this->setWindowSize(windowSize);
}

void Minimap::setPixel(unsigned int x, unsigned int y, sf::Color color) {
    image.setPixel(x, y, sf::Color(color.r, color.g, color.b, alpha));

    texture.loadFromImage(image);
    sprite.setTexture(texture, true);
}

void Minimap::clearPixel(unsigned int x, unsigned int y, MapTile *tile) {
    sf::Color color = tile->getSprite().getColor();
    color.a = alpha;

    if (tile->getType() == "water") {
        color -= sf::Color(100, 100, 30, 0);
    }

    image.setPixel(x, y, color);

    texture.loadFromImage(image);
    sprite.setTexture(texture, true);
}

void Minimap::setAlpha(unsigned int alpha) {
    this->alpha = (sf::Uint8) alpha;

    sf::Color newColor = sprite.getColor();
    newColor.a = this->alpha;

    sprite.setColor(newColor);
}

void Minimap::scale(float scale) {
    if (scale < 1 && sprite.getScale().x < 2)
        return;

    if (scale > 1 && sprite.getScale().x > 10)
        return;

    sprite.scale(scale, scale);

    auto mWidth  = (unsigned int)(round((double)hudSize.x / sprite.getScale().x));
    auto mHeight = (unsigned int)(round((double)hudSize.y / sprite.getScale().y));

    if (mWidth > mapSize.x || mHeight > mapSize.y) {
        if (mWidth > mapSize.x)
            mWidth = mapSize.x;

        if (mHeight > mapSize.y)
            mHeight = mapSize.y;

        sprite.setScale((float)hudSize.x / mWidth, (float)hudSize.y / mHeight);
    }

    sprite.setTextureRect(sf::IntRect(0, 0, mWidth, mHeight));
    textureRect = (sf::FloatRect)sf::IntRect(0, 0, mWidth, mHeight);

    this->setMinimapSize(sf::Vector2u(mWidth, mHeight));
}

void Minimap::setWindowSize(sf::Vector2u windowSize) {
    this->windowSize = windowSize;

    this->update();
}

void Minimap::update() {
}

void Minimap::moveMinimap(int x, int y) {
    if (x < minimapSize.x / 2 || x < 0)
        x = minimapSize.x / 2;
    if (y < minimapSize.y / 2 || y < 0)
        y = minimapSize.y / 2;

    if (x > texture.getSize().x - (minimapSize.x / 2))
        x = texture.getSize().x - (minimapSize.x / 2);
    if (y > texture.getSize().y - (minimapSize.y / 2))
        y = texture.getSize().y - (minimapSize.y / 2);

    sprite.setTextureRect(sf::IntRect(x - (minimapSize.x / 2), y - (minimapSize.y / 2), minimapSize.x, minimapSize.y));
    textureRect = (sf::FloatRect)sf::IntRect(x - (minimapSize.x / 2), y - (minimapSize.y / 2), minimapSize.x, minimapSize.y);
}

sf::Sprite &Minimap::getSprite() {
    return sprite;
}

sf::Sprite Minimap::getSpriteCopy() {
    return sprite;
}

void Minimap::setMinimapSize(sf::Vector2u minimapSize) {
    this->minimapSize = minimapSize;

    this->update();
}

sf::FloatRect Minimap::getTextureRect() {
    return textureRect;
}