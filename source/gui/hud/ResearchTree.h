#ifndef IZOGAME_RESEARCHTREE_H
#define IZOGAME_RESEARCHTREE_H

#include <sol.hpp>
#include "ResearchElement.h"
#include "../../players/PlayerData.h"

class ResearchTree{
public:
    explicit ResearchTree(UIWindow *window);

    std::map<std::string, ResearchElement*> &getElements();

    void setPointersForLua(PlayerData* data);
    PlayerData* getData();

    void setChosenElement(ResearchElement* chosenElement);
    ResearchElement* getChosenElement();

private:
    UIWindow *window = nullptr;
    std::map<std::string, ResearchElement*>elements;
    ResearchElement* chosenElement = nullptr;

    sf::Vector2f elementSize = vec2(134.0f, 64.0f);

    // -- Pointers for lua --
    PlayerData* data;

    void loadScript();
    void updateAllPositions(ResearchElement* element);
};

#endif //IZOGAME_RESEARCHTREE_H
