#ifndef IZOGAME_RESEARCHELEMENT_H
#define IZOGAME_RESEARCHELEMENT_H

#include <SFML/Graphics.hpp>

#include "../Gui.h"
#include "../gui_layout.h"
#include "../../shared.h"
#include "../../utilities/graphics/ResourceManager.h"

class ResearchTree;
class ResearchElement {
public:
    enum Type{SUMMER, WINTER, BOTH};
    ResearchElement(const std::string& id, sf::FloatRect rect, UIWindow *window, sol::table& data, ResearchTree* tree);

    void addParent(ResearchElement* parent);
    void addChildren(ResearchElement* children);

    std::vector<ResearchElement*>& getParents();
    std::vector<ResearchElement*>& getChildrens();

    void update(float scrollValue);
    void activate();

    void updatePosition();
    bool checkIfParentsAreActive();

    std::string getId();
    Type getType();
    int getLevel();
    bool isChecked();

    std::string getName();
    std::string getDesc();

private:
    std::vector<ResearchElement*>parents;
    std::vector<ResearchElement*>childrens;

    ResearchTree* tree;
    UIWindow *window = nullptr;
    sf::Texture* texture = nullptr;
    sf::FloatRect rect;
    bool isActive = false;
    bool checked = false;

    std::string id = "";
    std::string name = "";
    std::string desc = "";
    Type type = SUMMER;
    int level = 1;

    sol::protected_function effectFunction;

    void loadFromLua(sol::table& data);
    void doEffect();
};

#endif //IZOGAME_RESEARCHELEMENT_H
