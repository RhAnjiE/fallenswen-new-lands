#ifndef IZOGAME_MINIMAP_H
#define IZOGAME_MINIMAP_H

#include <SFML/Graphics.hpp>
#include "../../utilities/Extensions.h"
#include "../../maps/Map.h"

class Minimap {
public:
    void generateImage(Map& map, sf::Vector2u windowSize);
    void setPixel(unsigned int x, unsigned int y, sf::Color color);
    void clearPixel(unsigned int x, unsigned int y, MapTile* tile);
    void setAlpha(unsigned int alpha);

    void scale(float scale);
    void moveMinimap(int x, int y);

    void setWindowSize(sf::Vector2u windowSize);
    void setMinimapSize(sf::Vector2u minimapSize);

    sf::Sprite& getSprite();
    sf::Sprite getSpriteCopy();

    sf::FloatRect getTextureRect();
private:
    sf::Image image;
    sf::Texture texture;
    sf::Sprite sprite;
    sf::RectangleShape shape;

    sf::Vector2u windowSize;
    sf::Vector2u minimapSize;
    sf::Uint8 alpha = 200;

    sf::Vector2u mapSize = sf::Vector2u(35, 35);
    sf::Vector2u hudSize = sf::Vector2u(189, 177);
    sf::Vector2i offset  = sf::Vector2i(-42, -17);

    sf::FloatRect textureRect;

    void update();
};

#endif //IZOGAME_MINIMAP_H
