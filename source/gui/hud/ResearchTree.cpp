#include "ResearchTree.h"

ResearchTree::ResearchTree(UIWindow *window) {
    this->window = window;

    this->loadScript();
}

void ResearchTree::loadScript() {
    sol::state* lua = ResourceManager::getInstance().getLuaHandle();
    lua->script_file("assets/scripts/Research.lua");

    if ((*lua)["research"] != sol::nil) {
        sol::table table = (*lua)["research"];

        for(const auto& [rawId, data] : table) {
            std::string id = rawId.as<std::string>();
            sf::FloatRect rect = sf::FloatRect(30, window->rect.height / 2, elementSize.x, elementSize.y);

            sol::table elementData = (*lua)["research"][id];
            elements[id] = new ResearchElement(id, rect, window, elementData, this);
        }

        for(auto& [id, element] : elements) {
            if (table[id]["parents"] != sol::nil) {
                sol::table parents = table[element->getId()]["parents"];

                for(auto& pair : parents) {
                    std::string parentId = pair.second.as<std::string>();
                    element->addParent(elements[parentId]);

                    elements[parentId]->addChildren(element);
                }
            }
        }

        //Find the first element without parents
        ResearchElement* firstElement = nullptr;
        for(auto& [id, element] : elements) {
            if (element->getParents().empty()) {
                firstElement = element;
                break;
            }
        }

        if (firstElement != nullptr) {
            this->updateAllPositions(firstElement);
        }
    }
}

std::map<std::string, ResearchElement*> &ResearchTree::getElements() {
    return elements;
}

void ResearchTree::updateAllPositions(ResearchElement *element) {
    while (element != nullptr) {
        element->updatePosition();

        if (!element->getChildrens().empty()) {
            if (element->getChildrens().size() > 1) {
                for(int i = 1; i < element->getChildrens().size(); i++) {
                    bool canUpdateRightNow = element->getChildrens()[i]->checkIfParentsAreActive();

                    if (canUpdateRightNow)
                        this->updateAllPositions(element->getChildrens()[i]);
                }
            }

            bool canUpdateRightNow = element->getChildrens()[0]->checkIfParentsAreActive();

            if (canUpdateRightNow)
                element = element->getChildrens()[0];

            else element = nullptr;
        }

        else element = nullptr;
    }
}

void ResearchTree::setPointersForLua(PlayerData *data) {
    this->data = data;
}

PlayerData* ResearchTree::getData() {
    return data;
}

ResearchElement *ResearchTree::getChosenElement() {
    return chosenElement;
}

void ResearchTree::setChosenElement(ResearchElement* chosenElement) {
    this->chosenElement = chosenElement;
}

