#include "MessageLog.h"

MessageLog::MessageLog() {

}

void MessageLog::update(float deltaTime, UIWindow *window) {
    time += deltaTime;

    auto message = messageQueue.begin();
    //if (message != messageQueue.end()) {
    //    if (time - message->timestamp > messageLifetime) {
    //        messageQueue.erase(message);
    //    }
    //}

    this->drawHud(window);
}

void MessageLog::pushMessage(const char *format, ...) {

    va_list argList;
    va_start(argList, format);

    char message_buffer[1024];
    vsprintf(message_buffer, format, argList);
    va_end(argList);

    Message message = {};
    message.timestamp = time;
    message.message = message_buffer;

    static int messageType = 0;
    message.type = messageType++ % 6;

    if (messageQueue.size() >= 50) {
        messageQueue.erase(messageQueue.begin());
    }

    if (messageQueue.size() > maxMessagCount) {
        scroll_value = 1.0f;
    }

    messageQueue.push_back(message);
}

void MessageLog::drawHud(UIWindow *window) {
    if (!window->show) {
        return;
    }

    {
        UITheme theme = UIGetState()->theme;
        UIGetState()->theme.character_size = 13;

        UIRawLabel(window, vec2(0.0F, 5.0F), "MESSAGE LOG", UIAlign_Center);

        UIGetState()->theme = theme;
    }

    sf::FloatRect view_rect = Rect(0.0f, 41.0f, 336.0f, 92.0f);
    //UIRect(window, view_rect, sf::Color::Green);

    {
 
        float text_height = 10.0f;
        float spacing = text_height;

        float item_height = text_height + spacing;

        float content_size = (item_height) * messageQueue.size();
        float scroll_offset = (content_size - view_rect.height)  * scroll_value;

        float y = view_rect.top;
        float view_rect_height = window->rect.height - y;

        size_t start_index = (size_t)(scroll_offset / (item_height));
        for (size_t messageIndex = start_index; messageIndex < start_index + maxMessagCount && messageIndex < messageQueue.size(); messageIndex++) {
            Message *message = &messageQueue[messageIndex];     

            UIRawLabel(window, vec2(0.0f, y), message->message, UIAlign_Center);
            y += item_height;
        }

        UISlider(window, &scroll_value, view_rect_height, content_size);
    }

    UIRawLabel(window, sf::Vector2f(5.0f, 24.0f), "scroll_value = " + std::to_string(scroll_value) + "f " + "message_count = " + std::to_string(messageQueue.size()));
}

void MessageLog::registerForLua() {
    sol::state& lua = *ResourceManager::getInstance().getLuaHandle();

    lua.new_usertype<MessageLog>(
            "MessageLog", sol::constructors<>()
            //"pushMessage", &MessageLog::pushMessage //TODO: Problem with '...' argument
    );

    //lua.set_function("pushMessage", &MessageLog::pushMessage);
}
