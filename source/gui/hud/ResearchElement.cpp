#include "ResearchElement.h"
#include "ResearchTree.h"

ResearchElement::ResearchElement(const std::string &id, sf::FloatRect rect, UIWindow *window, sol::table& data, ResearchTree* tree) {
    this->id = id;
    this->window = window;
    this->type = Type::SUMMER; //data["type"];
    this->rect = rect;
    this->tree = tree;

    this->loadFromLua(data);
}

void ResearchElement::loadFromLua(sol::table& data) {
    name = data["name"];
    desc = data["description"];

    if (data["textureName"] != sol::nil)
        texture = &ResourceManager::getTexture(data["textureName"]);

    effectFunction = data["effect"];
}

void ResearchElement::update(float scrollValue) {
    sf::Vector2f windowSize = vec2(window->rect.width, window->rect.height);

    //TODO: Get content size when research tree was based on grid system
    sf::Rect fixedRect = rect;
    //fixedRect.top += (zoom - 0.5) * 100;
    fixedRect.left += scrollValue * 500;

    ButtonState state = UIRawButton(window, fixedRect, "", &ResourceManager::getTexture("research"), nullptr);

    switch(state) {
        case ButtonState::HOVERED:
            tree->setChosenElement(this);
            break;

        case ButtonState::CLICKED:
            this->activate();
            break;

        default:
            break;
    }

    sf::Color color = sf::Color(255, 255, 255, 30);
    if (isActive)
        color = sf::Color(255, 255, 255, 255);

    UIRawImage(window, fixedRect, *texture, nullptr, color);
    UIRect(window, sf::FloatRect(fixedRect.left, fixedRect.top + 42, fixedRect.width, 18), sf::Color(0, 0, 0, 200));

    sf::Vector2f centerTextPosition = sf::Vector2f(fixedRect.left + fixedRect.width / 2 - name.length() * 4, fixedRect.top + 42);
    UIRawLabel(window, centerTextPosition, name);
}

//TODO: Improve or change this system
void ResearchElement::updatePosition() {
    int maxLevel = 0;

    for (auto& parent : parents) {
        if (maxLevel < parent->getLevel())
            maxLevel = parent->getLevel();
    }

    level = maxLevel + 1;
    checked = true;

    rect.left = 30 + (level - 1) * (rect.width + 10);

    int size = (int)childrens.size();
    if (size > 1) {
        for(int i = 0; i < size; i++) {
            int height = (int)getChildrens()[i]->rect.height + 10;

            getChildrens()[i]->rect.top = rect.top;
            getChildrens()[i]->rect.top += i * height;
            getChildrens()[i]->rect.top -= (size / 2) * height;
            //getChildrens()[i]->rect.top += ((size + 1) % 2) * (height / 2);
        }
    }

    else if (size == 1)
        getChildrens()[0]->rect.top = rect.top;
}

void ResearchElement::activate() {
    if (!isActive) {
        for (auto const& parent : parents) {
            if (!parent->isActive) {
                return;
            }
        }

        isActive = true;
        this->doEffect();
    }
}

void ResearchElement::doEffect() {
    if (effectFunction) {
        auto result = effectFunction(tree->getData());

        if (!result.valid()) {
            sol::error err = result;
            Logger::error(err.what());
        }
    }

    else Logger::warning("Not found method body in lua script");
}

void ResearchElement::addParent(ResearchElement *parent) {
    parents.emplace_back(parent);
}

void ResearchElement::addChildren(ResearchElement *children) {
    childrens.emplace_back(children);
}

std::vector<ResearchElement *> &ResearchElement::getParents() {
    return parents;
}

std::vector<ResearchElement *> &ResearchElement::getChildrens() {
    return childrens;
}

std::string ResearchElement::getId() {
    return id;
}

ResearchElement::Type ResearchElement::getType() {
    return type;
}

int ResearchElement::getLevel() {
    return level;
}

bool ResearchElement::isChecked() {
    return checked;
}

bool ResearchElement::checkIfParentsAreActive() {
    for(auto& parent : this->getParents()) {
        if (!parent->isChecked()) {
            return false;
        }
    }

    return true;
}

std::string ResearchElement::getName() {
    return name;
}

std::string ResearchElement::getDesc() {
    return desc;
}
