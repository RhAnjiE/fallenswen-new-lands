#include "Hud.h"

#define NOT_IMPLEMENTED assert(0)

void Hud::init(Map& map, sf::Vector2u windowSize, PlayerData &playerData) {
    UIFree();

	uint32_t width = 1366, height = 768;

    this->SetupDefaultTheme();

    memset(&win_layout, 0, sizeof(win_layout));
    this->SetupLayout((float)width, (float)height);

    sf::Vector2u minimap_size = sf::Vector2u(210, 210);
    minimap.setMinimapSize(minimap_size);
    minimap.generateImage(map, windowSize);

    this->playerData = &playerData;
    this->map = &map;
    this->target = nullptr;
    this->researchTree = new ResearchTree(win_layout.research);
    this->researchTree->setPointersForLua(this->playerData);

    unlocked_items.clear();
    pushBuildMenuItem("cityHall", "icon_cityhall");

    debugMode = true;
}

void Hud::updateSize(sf::Vector2u windowSize) {
    auto screen_size = (sf::Vector2f)windowSize;

    UISetProjection(windowSize.x, windowSize.y);
    SetupLayout((float)windowSize.x, (float)windowSize.y);

    //windows.toolbar->rect = Rect(0.0f, 0.0f, screen_size.x, windows.toolbar->rect.height);
}

void Hud::processEvent(const sf::Event &event) {
    if (playerData->getCityHall() != nullptr) {
        if (event.type == sf::Event::KeyPressed) {
            if (event.key.code == sf::Keyboard::B)
                UIToggleWindow(win_layout.build);
            if (event.key.code == sf::Keyboard::M)
                UIToggleWindow(win_layout.map);
            if (event.key.code == sf::Keyboard::Tab)
                UIToggleWindow(win_layout.log);
            if (event.key.code == sf::Keyboard::R)
                UIToggleWindow(win_layout.research);
            if (event.key.code == sf::Keyboard::Escape)
                UIToggleWindow(win_layout.pause);
            if (event.key.code == sf::Keyboard::Tilde)
                UIToggleWindow(win_layout.debug);
        }
    }
}

void Hud::update(float deltaTime) {
    this->DrawToolbarWndow();
    this->DrawSelectionWindow();
    this->DrawTimeWindow();
    this->DrawMinimapWindow();
    this->DrawPauseWindow();
    this->DrawResearchWindow();
    this->DrawBuildWindow();
    this->DrawDebugWindow();

    playerData->getMessageLog().update(deltaTime, win_layout.log);

    this->DrawOverlay();

    //this->TestAutoLayout();
}

void Hud::draw(sf::RenderTarget &target, sf::RenderStates states) const {

}

void Hud::SetupDefaultTheme() {
    UITheme theme = UICreateDefaultTheme();

    theme.colors[UIColor_Button] = sf::Color(60, 60, 60, 255);
    theme.colors[UIColor_ButtonHovered] = sf::Color(0, 122, 204);
    theme.colors[UIColor_ButtonPressed] = sf::Color(104, 33, 122);

    theme.colors[UIColor_Text] = sf::Color::White;

    theme.colors[UIColor_Window] = sf::Color(45, 45, 45, 200);

    UISetTheme(theme);
}

void Hud::pushBuildMenuItem(const std::string &name, const std::string &texture_name) {
    unlocked_items.push_back({ name, ResourceManager::getTexture(texture_name) });
}

void Hud::setTargetInfo(const std::string &name, const std::string &desc, MapBuilding* building) {
    target = building;
    
    UIWindow *window = win_layout.selection;

    target ? UIWindowShow(window) : UIWindowHide(window);
}

bool Hud::interact(Map& map, sf::Vector2f mousePosition) {
    return UIGetIO()->captured;
}

Minimap &Hud::getMinimap() {
    return minimap;
}

void Hud::resetBuildingsList() {
    unlocked_items.clear();
    pushBuildMenuItem("delete", "icon_delete");
}

void Hud::setStatement(const std::string &message) {

}

void Hud::SetupLayout(float width, float height) {
    {
        UITheme *theme = UIPushTheme();
        theme->colors[UIColor_Window] = sf::Color(45, 45, 45);

        UI::GetWindow(0.0f, 0.0f, width, 25.0f, "Toolbar", 0, &win_layout.toolbar);

        UIPopTheme();
    }

    UI::GetWindow(37.0f, 486.0f, 227.0f, 227.0f, "Map", 0, &win_layout.map);
    //UIWindowHide(win_layout.map);r

    UI::GetWindow(37.0f, height - 37.0f - 18.0f, 227.0f, 37.0f, "Time", 0, &win_layout.time);

    UI::GetWindow(width / 2.0f - 542.0f / 2.0f, 586.0f, 542.0f, 161.0f, "Selection", UIWindow_CloseButton, &win_layout.selection);
    UIWindowHide(win_layout.selection);

    {
        sf::Vector2f window_size = vec2(256.0f, 275.0f), offset = vec2(34.0f, 37.0f);
        
        UI::GetWindow(width - window_size.x - offset.x, height - window_size.y - offset.y, window_size.x, window_size.x, "Build", 0, &win_layout.build);
        //UIWindowHide(win_layout.build);
    }

    {
        sf::Vector2f size = vec2(250.0f, 138.0f);
        sf::Vector2f position = vec2(width / 2.0f - size.x / 2.0f, height / 2.0f - size.y / 2.0f);

        UI::GetWindow(position.x, position.y, size.x, size.y, "Pause Menu", UIWindow_CloseButton, &win_layout.pause);
        UIWindowHide(win_layout.pause);
    }

    UI::GetWindow(1000.0f, 39.0f, 336.0f, 143.0f, "Message Log", 0, &win_layout.log);
    UIWindowHide(win_layout.log);

    {
        UITheme *theme = UIPushTheme();
        theme->colors[UIColor_Window] = sf::Color(45, 45, 45);

        sf::Vector2f size = vec2((float)width * 0.75f, (float)height * 0.75f);
        sf::Vector2f position = vec2((float)width / 2.0f - size.x / 2.0f, (float)height / 2.0f - size.y / 2.0f);

        UI::GetWindow(position.x, position.y, size.x, size.y, "Research", UIWindow_CloseButton, &win_layout.research);
        UIWindowHide(win_layout.research);

        UIPopTheme();
    }

    {
        UITheme *theme = UIPushTheme();
        theme->colors[UIColor_Window] = sf::Color(45, 45, 45);

        UI::GetWindow(0.0f, 25.0f, 250.0f, 400.0f, "Debug Menu", 0, &win_layout.debug);
        win_layout.debug->show = false;

        UIPopTheme();
    }
}

void Hud::DrawToolbarWndow() {
    UIWindow *window = win_layout.toolbar;

    {
        char people[64] = { "0/10" };
        sprintf(people, "%i/%i", playerData->getPeople(), playerData->getMaxPeople());

        UILayout layout = UIInitLayout(vec2(5.0f, 2.0f));
        UIImage(window, &layout, vec2(20.0f, 20.0f), ResourceManager::getTexture("woodcutter"));
        UILabel(window, &layout, people);

        char food[64] = { "0" };
        if (playerData->getCityHall() != nullptr)
            sprintf(food, "%i", playerData->getResource("food"));

        else sprintf(food, "%i", 0);

        UIImage(window, &layout, vec2(20.0f, 20.0f), ResourceManager::getTexture("iconResourceFood"));
        UILabel(window, &layout, food);
    }
    
    if(debugMode) {
        UITheme *theme = UIPushTheme();
        theme->colors[UIColor_Button] = theme->colors[UIColor_Window];
        theme->colors[UIColor_Button].a = 255;

        std::string text = "DEBUG MODE ENABLED, PRESS [~] TO OPEN DEBUG MENU";
        if (UIRawButton(window, UIWindowGetCenter(window, sf::Vector2f(350.0f, window->rect.height), 0.0f), text) == ButtonState::CLICKED) {
            UIToggleWindow(win_layout.debug);
        }

        UIPopTheme();
    }

    {
        sf::Vector2f button_size = vec2(90.0f, 19.0f);
        UILayout layout = UIInitLayout(vec2(985.0f, 3.0f), UILayout_Horizontal);

        if (playerData->getCityHall() != nullptr) {
            if (UIButton(window, button_size, &layout, "Map [M]") == ButtonState::CLICKED) {
                UIToggleWindow(win_layout.map);
            }
            if (UIButton(window, button_size, &layout, "Log [TAB]") == ButtonState::CLICKED) {
                UIToggleWindow(win_layout.log);
            }
            if (UIButton(window, button_size, &layout, "Research [R]") == ButtonState::CLICKED) {
                UIWindowHide(win_layout.pause);
                UIToggleWindow(win_layout.research);
            }
            if (UIButton(window, button_size, &layout, "Menu [ESC]") == ButtonState::CLICKED) {
                UIWindowHide(win_layout.research);
                UIToggleWindow(win_layout.pause);
            }
        }
    }
}

void Hud::DrawSelectionWindow() {
    UIWindow *window = win_layout.selection;

    if (target) {
        const sf::Texture &texture = *target->getSprite().getTexture();
        auto texture_rect = (sf::FloatRect)target->getSprite().getTextureRect();

        //UIImage(window, UIWindowGetCenter(window, sf::Vector2f(100.0f, 100.0f), 55.0f), texture, &texture_rect);
        //UIImage(window, Rect(vec2(21.0f, 28.0f), vec2(120.0f, 120.0f)), texture, &texture_rect);

        UILayout layout = UIInitLayout(vec2(0.0f, 55.0f), UILayout_Vertical);
        target->drawUI(window, &layout);

        UITheme *theme = UIPushTheme();
        
        theme->character_size = 15;
        UIRawLabel(window, vec2(0.0f, 8.0f), target->getName(), UIAlign_Center);

        theme->character_size = 13;
        UIRawLabel(window, vec2(154.0f, 31.0f), target->getDesc(), UIAlign_Center);

        UIPopTheme();
    }
}

void Hud::DrawBuildWindow() {
    UIWindow *window = win_layout.build;
    sf::Vector2f window_pos = rectPos(window->rect), window_size = rectSize(window->rect);

    /*TOGGLE BUTTON*/
    {
        UITheme *theme = UIPushTheme();
        theme->colors[UIColor_Button] = sf::Color(45, 45, 45, 200);

        if (UIRawButton(nullptr, Rect(window_pos.x, window_pos.y + window_size.y, window_size.x, 37.0f), "Build [B]") == ButtonState::CLICKED) {
            UIToggleWindow(window);
        }

        UIPopTheme();
    }

    UIRawLabel(window, vec2(0.0f, 10.0f), "BUILD MENU", UIAlign_Center);
 
    /*RESOURCES*/
    {     
        UILayout layout = UIInitLayout(vec2(win_layout.resource_bar_width, 35.0f), UILayout_Horizontal);
        layout.spacing = 13.0f;
        
        sf::Vector2f image_size = vec2(20.0f, 19.0f);

        char money[64] = { "0" }, wood[64] = { "0" }, stone[64] = { "0" };
        if (playerData) {
            sprintf(money, "%i", playerData->getMoney());
            sprintf(wood, "%i", playerData->getResource("wood"));
            sprintf(stone, "%i", playerData->getResource("stone"));
        }

        UIImage(window, &layout, image_size, ResourceManager::getTexture("iconResourceMoney"));
        UILabel(window, &layout, money);

        UIImage(window, &layout, image_size, ResourceManager::getTexture("iconResourceWood"));
        UILabel(window, &layout, wood);

        UIImage(window, &layout, image_size, ResourceManager::getTexture("iconResourceStone"));
        UILabel(window, &layout, stone);

        win_layout.resource_bar_width = window->rect.width / 2.0f - layout.width / 2.0f;
    }

    /*BUILD GRID, FOR DEBUG PURPOSES*/
    {   
        int max_row_count = 4;
        max_row_count = std::min<float>(max_row_count, unlocked_items.size());

        float spacing = 5.0f;

        sf::Vector2f button_size = vec2(40.0f, 40.0f);

        float row_width = -spacing + (max_row_count * (button_size.x + spacing));
        sf::Vector2f at = vec2(window_size.x / 2.0f - row_width / 2.0f, 60.0f);

        UILayout layout = UIInitLayout(at, UILayout_Horizontal);
        layout.spacing = spacing;       

        int count = 0;
        for (const BuildMenuItem &item : unlocked_items) {
            std::string tooltip = item.name;

            if (UIButton(window, button_size, &layout, std::string(), &item.texture, &tooltip) == ButtonState::CLICKED) {
                map->getInteraction().setChosenObject(item.name);
            }

            count++;
            if (count >= max_row_count) {
                count = 0;

                layout.at.x = at.x; layout.at.y += button_size.y + layout.spacing;
            }
        }
    }
}

void Hud::DrawTimeWindow() {
    UIWindow *window = win_layout.time;

    UIRawLabel(window, vec2(0.0f, 10.0f), "5 AD July, 8:53 AM (SUMMER)", UIAlign_Center);
}

void Hud::DrawMinimapWindow() {
    UIWindow *window = win_layout.map;

    sf::Vector2f window_size = vec2(window->rect.width, window->rect.height);
    sf::Vector2f minimap_size = vec2(210.0f, 210.0f);
    sf::Vector2f at = window_size / 2.0f - minimap_size / 2.0f;

    sf::Vector2f window_position = vec2(window->rect.left, window->rect.top);
    sf::Vector2f minimap_position_in_screen_space = window_position + at;
    minimap.getSprite().setPosition(minimap_position_in_screen_space);

    const sf::Texture *texture = minimap.getSprite().getTexture();
    sf::FloatRect texture_rect = minimap.getTextureRect();
    UIRawImage(win_layout.map, Rect(at, minimap_size), *texture, &texture_rect);

    UIInputOutput *io = UIGetIO();
    if (io->mouse.wheel_moved) {
        if (sf::FloatRect(minimap_position_in_screen_space, minimap_size).contains(io->mouse.position)) {
            if (io->mouse.wheel_delta == 1)
                minimap.scale(1.1f);

            else if (io->mouse.wheel_delta == -1)
                minimap.scale(0.9f);
        }
    }
}

void Hud::DrawPauseWindow() {
    UIWindow *window = win_layout.pause;

    sf::Vector2f window_size = vec2(window->rect.width, window->rect.height);
    sf::Vector2f button_size = vec2(window->rect.width * 0.70f, 25.0f);

    UILayout layout = UIInitLayout(vec2(window_size.x / 2.0f - button_size.x / 2.0f, 10.0f), UILayout_Vertical);

    menuButtonStatus = NONE;
    if (UIButton(window, button_size, &layout, "Save") == ButtonState::CLICKED) {
        menuButtonStatus = SAVE;
    }

    if (UIButton(window, button_size, &layout, "Load") == ButtonState::CLICKED) {
        menuButtonStatus = LOAD;
    }
    
    if (UIButton(window, button_size, &layout, "Main Menu") == ButtonState::CLICKED) {
        menuButtonStatus = MAIN_MENU;
    }

    if (UIButton(window, button_size, &layout, "Exit game") == ButtonState::CLICKED) {
        menuButtonStatus = EXIT;
    }
}

void Hud::DrawResearchWindow() {
    UIWindow *window = win_layout.research;
    sf::Vector2f windowSize = vec2(window->rect.width, window->rect.height);

    UITheme *theme = UIPushTheme();
    theme->character_size = 15;

    UIRawLabel(window, vec2(window->rect.width / 2.0f - 120.0f / 2.0f, 7.0f), "RESEARCH CENTER");

    static float value = 0.0f;

    for (auto const& [id, element] : researchTree->getElements()) {
        element->update(value);
    }

    ResearchElement* chosenElement = researchTree->getChosenElement();
    if (chosenElement != nullptr) {
        sf::FloatRect descArea = sf::FloatRect(0, window->rect.height - 100, window->rect.width, 100);
        UIRect(window, descArea, sf::Color(0, 0, 0, 120));

        theme->character_size = 20;
        UIRawLabel(window, vec2(30.0f, descArea.top + 10), chosenElement->getName());
        theme->character_size = 15;
        UIRawLabel(window, vec2(30.0f, descArea.top + 35), chosenElement->getDesc());
    }

    UISlider(window, &value, 100.0f, 200.0f);
    UIPopTheme();
}

Hud::MenuButtonStatus Hud::getMenuButtonStatus() {
    return menuButtonStatus;
}

void Hud::DrawDebugWindow() {
    UIWindow *window = win_layout.debug;
    if (!window->show || !debugMode) {
        return;
    }

    UILayout layout = UIInitLayout(vec2(0.0f, 8.0f), UILayout_Vertical);
    
    UILabel(window, &layout, "DEBUG MENU", UIAlign_Center);
    layout.at.y += 10.0f;

    UISeparator(window, &layout);

    UILabel(window, &layout, "PERFORMANCE", UIAlign_Center);
    layout.at.y += 10.0f;

    
    UILabel(window, &layout, std::to_string(DeltaTime::getFps()) + "FPS", UIAlign_Center);
    layout.at.y += 10.0f;

    float button_width = window->rect.width * 0.95f;
    layout.at.x = window->rect.width / 2.0f - button_width / 2.0f;

    std::string buttonName = (!enableDebugCamera ? "Enable Debug Camera" : "Disable Debug Camera");
    if (UIButton(window, vec2(button_width, 20.0f), &layout, buttonName) == ButtonState::CLICKED)
        enableDebugCamera = !enableDebugCamera;

    UISeparator(window, &layout);

    UILabel(window, &layout, "MESSAGE LOG", UIAlign_Center);
    layout.at.y += 10.0f;

    {
        MessageLog &messageLog = playerData->getMessageLog();

        {
            static uint32_t messageIndex = 0;
            if (UIButton(window, vec2(button_width, 20.0f), &layout, "Push Test Message") == ButtonState::CLICKED) {
                messageLog.pushMessage("Message %i", messageIndex++);
            }
        }
    }

    UISeparator(window, &layout);

    UILabel(window, &layout, "MAP EDITOR", UIAlign_Center);
    UILabel(window, &layout, "[TODO]", UIAlign_Center);
}

void Hud::DrawOverlay() {
    UIWindow *window = UI::GetOverlay();

    if (!UIGetIO()->captured) {
        MapObject *object = map->getChosenObject();
        if (object) {
            sf::Vector2f offset(10.0f, 10.0f);

            UIRawLabel(&UIGetState()->overlay, UIGetIO()->mouse.position + offset, object->getType());
        }
    }
}

void Hud::TestUI() {

}

bool Hud::isEnableDebugCamera() {
    return enableDebugCamera;
}
