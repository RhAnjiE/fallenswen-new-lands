#ifndef IZOGAME_ICON_H
#define IZOGAME_ICON_H

#include <SFML/Graphics.hpp>

class Icon : public sf::Sprite {
public:
    Icon(const std::string& buildingId, sf::Texture& texture, sf::Vector2f position, sf::Vector2f size);

    void setAlpha(int alpha);

    std::string getBuildingId();
private:
    std::string buildingId = "";

};

#endif //IZOGAME_ICON_H
