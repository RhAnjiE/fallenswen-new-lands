#include "Icon.h"

Icon::Icon(const std::string& buildingId, sf::Texture& texture, sf::Vector2f position, sf::Vector2f size)
        : Sprite() {
    this->buildingId = buildingId;

    this->setTexture(texture);
    this->setPosition(position);
    this->setAlpha(150);

    this->setScale(size.x / this->getTexture()->getSize().x, size.y / this->getTexture()->getSize().y);
}

std::string Icon::getBuildingId() {
    return buildingId;
}

void Icon::setAlpha(int alpha) {
    if(this->getColor().a != alpha) {
        sf::Color color = this->getColor();
        color.a = alpha;

        this->setColor(color);
    }
}
