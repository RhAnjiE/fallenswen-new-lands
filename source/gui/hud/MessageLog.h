#pragma once
#include "../../shared.h"
#include "../Gui.h"

class MessageLog {
public:
    MessageLog();

    void update(float deltaTime, UIWindow *window);
    void pushMessage(const char *format, ...);

    static void registerForLua();
private:
    struct Message {
        int type;
        float timestamp;

        std::string message;
    };

    int maxMessagCount = 4;
    float messageLifetime = 20.0f;
    float time = 0.0f;

    float scroll_value = 0.0f;

    std::vector<Message>messageQueue;

    void drawHud(UIWindow *window);
};