#ifndef IZOGAME_HUD_H
#define IZOGAME_HUD_H

#include <SFML/Graphics.hpp>
#include "Minimap.h"
#include "Icon.h"
#include "../../maps/objects/buildings/MapBuilding.h"

#include "../Gui.h"
#include "../gui_layout.h"
#include "ResearchElement.h"

#include "MessageLog.h"
#include "ResearchTree.h"

class Map;
class Hud : public sf::Drawable {
public:
    enum MenuButtonStatus {SAVE, LOAD, MAIN_MENU, EXIT, NONE};

    void init(Map& map, sf::Vector2u windowSize, PlayerData &playerData);
    void update(float DeltaTime);
    void updateSize(sf::Vector2u windowSize);
	void processEvent(const sf::Event &event);
	//receive the pointers of window, player, buidlings manager
	//void setPointersForLua();

    bool interact(Map& map, sf::Vector2f mousePosition);

    void pushBuildMenuItem(const std::string &name, const std::string &texture_name);
    void resetBuildingsList();

    void setTargetInfo(const std::string &name, const std::string &desc, MapBuilding* building = nullptr);
    void setStatement(const std::string &message);
    MenuButtonStatus getMenuButtonStatus();

    Minimap &getMinimap();
    bool isEnableDebugCamera();

    UIWindow *GetMessageLogWindow();
private:
    sf::Vector2u windowSize;

    Minimap minimap;

    Map *map = nullptr;
    PlayerData *playerData = nullptr;
    MapBuilding* target = nullptr;
    ResearchTree* researchTree = nullptr;

    bool debugMode;
    bool enableDebugCamera = false;

    MenuButtonStatus menuButtonStatus = NONE;   

    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    void SetupDefaultTheme();

    struct BuildMenuItem {
        std::string name;
        const sf::Texture &texture;
    };

    std::vector<BuildMenuItem>unlocked_items;

    struct WindowLayout {
        UIWindow *toolbar, *map, *time, *selection, *pause, *research, *log, *debug, *build;
        float resource_bar_width = 0.0f;
    };

    WindowLayout win_layout;

    void SetupLayout(float width, float height);

    void DrawToolbarWndow();
    void DrawSelectionWindow();
    void DrawTimeWindow();
    void DrawMinimapWindow();
    void DrawPauseWindow();
    void DrawResearchWindow();
    void DrawDebugWindow();
    void DrawBuildWindow();
    void DrawOverlay();

    UIWindow *testWindow;
    void TestUI();
};

static float value = 0.0f;

#endif //IZOGAME_HUD_H
