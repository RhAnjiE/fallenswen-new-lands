#ifndef GUI_H
#define GUI_H

#include <vector>
#include <string>
#include <SFML/Graphics.hpp>
#include "../utilities/graphics/ResourceManager.h"

struct UIBatch {
    sf::FloatRect clipRect;

	sf::VertexArray vertices;
	const sf::Texture *texture;
};

struct UIDrawList {
	std::vector<UIBatch>batches;
};

/*Input*/
struct UIInputMouse {
	sf::Vector2f position;
	bool pressed[3], released[3];
	bool down[3];

    bool moved;

    bool wheel_moved;
    float wheel_delta;
};

struct UIInputOutput {
	UIInputMouse mouse;
	bool captured;
};

enum UIWindowStyle {
    UIWindow_CloseButton = 1 << 0,
    UIWindow_NoBackground = 1 << 1,
    UIWindow_Title = 1 << 2
};

struct UIWindow {
    uint32_t style;

	sf::FloatRect rect;
	sf::Color color;
	std::string name;
	UIDrawList draw_list;
	bool show;

    bool is_dragging;
};

enum UIThemeColors {
	UIColor_Button,
	UIColor_ButtonHovered,
	UIColor_ButtonPressed,

	UIColor_Text,

	UIColor_Window,

	UIColor_Count
};

struct UITheme {
	sf::Color colors[UIColor_Count];
    int character_size;
};

struct UIState {
	UIInputOutput input;

    UITheme theme_stack;
	UITheme theme;

	sf::Font font;

    UIWindow desktop, overlay;
	std::vector<UIWindow*>windows;

	sf::Vector2f viewport;

	bool capture_input_next_frame;
};

enum UIAlign
{
    UIAlign_Left,
    UIAlign_Center
};

void registerGuiElements();

UIState *UICreateState();

UIState *UIGetState();
UIInputOutput *UIGetIO();

void UISetProjection(int width, int height);
void UIProcessEvent(const sf::Event &event);
void UIFree();
bool UILoadFont(const std::string &path);

UITheme *UIPushTheme();
void UIPopTheme();

void UISetTheme(const UITheme &theme);
UITheme UICreateDefaultTheme();

float UIGetTextMetrics(const std::string &string);

void UIDraw(sf::RenderTarget &render_target);
void UIUpdate(const sf::Window &window);

/*Widgets*/
namespace UI {
    void GetWindow(float x, float y, float width, float height, const std::string &name, uint32_t style, UIWindow **window);

    UIWindow *GetOverlay();
}

void UIToggleWindow(UIWindow *window);

void UIWindowShow(UIWindow *window);
void UIWindowHide(UIWindow *window);

bool UIWindowIsVisible(UIWindow *window);

sf::FloatRect UIWindowGetCenter(UIWindow *window, const sf::Vector2f &size, float y);

enum ButtonState {DEFAULT, HOVERED, CLICKED};
ButtonState UIRawButton(UIWindow *window, const sf::FloatRect &position, const std::string &label,
              const sf::Texture *texture = nullptr, const std::string *tooltip = nullptr);
sf::FloatRect UIRawLabel(UIWindow *window, const sf::Vector2f &position, const std::string &format, UIAlign align = UIAlign_Left, ...);
void UIRawImage(UIWindow *window, const sf::FloatRect &rect, const sf::Texture &texture, const sf::FloatRect *texture_rect = nullptr, sf::Color color = sf::Color::White);
void UIRect(UIWindow *window, const sf::FloatRect &rect, const sf::Color &color = sf::Color::White);

void UISlider(UIWindow *window, float *value, float view_size, float contet_size);
void UITextBox(UIWindow *window, const sf::FloatRect &rect, std::string &buffer); //TODO(Arc)

/*Layout*/
enum UILayoutType {
    UILayout_Horizontal,
    UILayout_Vertical,
    UILayout_Grid
};

struct UILayout {
    UILayoutType type;

    sf::Vector2f at;
    float spacing;

    float width;
};

UILayout UIInitLayout(const sf::Vector2f &at, UILayoutType type = UILayout_Horizontal);
void UIAdvanceLayout(UILayout *layout, const sf::Vector2f &size);

ButtonState UIButton(UIWindow *window, const sf::Vector2f &size, UILayout *layout, const std::string &label,
              const sf::Texture *texture = nullptr, const std::string *tooltip = nullptr);
sf::FloatRect UILabel(UIWindow *window, UILayout *layout, const std::string &format, UIAlign align = UIAlign_Left, ...);
void UIImage(UIWindow *window, UILayout *layout, const sf::Vector2f &size, const sf::Texture &texture, sf::Color color = sf::Color::White);

void UISeparator(UIWindow *window, UILayout *layout);

#endif //gui_H