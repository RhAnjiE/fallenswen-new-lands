#include "Gui.h"

#include <SFML/OpenGL.hpp>
#include <cmath>
#include <string.h>

#include <assert.h>

#include "../shared.h"

static UIState *_internal = 0;

UIState *UICreateState() {
    _internal = new UIState();
    
    memset(&_internal->input, 0, sizeof(_internal->input));

    memset(&_internal->theme_stack, 0, sizeof(_internal->theme_stack));
    memset(&_internal->theme, 0, sizeof(_internal->theme));

    memset(&_internal->desktop, 0, sizeof(_internal->desktop));
    memset(&_internal->overlay, 0, sizeof(_internal->overlay));

    _internal->capture_input_next_frame = false;

    return _internal;
}

UIState *UIGetState() {
    //assert(_internal);

	return _internal;
}

UIInputOutput *UIGetIO() {
	return &UIGetState()->input;
}

void UISetProjection(int width, int height) {
    UIState *state = UIGetState();
    state->viewport = vec2((float)width, (float)height);

    UIWindow *window = nullptr;

    window = &state->desktop;
    window->rect = Rect(0.0f, 0.0f, (float)width, (float)height);
    window->style = UIWindow_NoBackground;
    window->show = true;

    window = &state->overlay;
    window->rect = Rect(0.0f, 0.0f, (float)width, (float)height);
    window->style = UIWindow_NoBackground;
    window->show = true;
}

void UIProcessEvent(const sf::Event &event) {
	UIState *state = UIGetState();

	UIInputOutput *input = &state->input;
	UIInputMouse *mouse = &input->mouse;

	switch (event.type) {
		case sf::Event::MouseButtonPressed: {
			mouse->pressed[event.mouseButton.button] = true;
		} break;

		case sf::Event::MouseButtonReleased: {
			mouse->released[event.mouseButton.button] = true;
		} break;

        case sf::Event::MouseWheelMoved: {
            mouse->wheel_moved = true;
            mouse->wheel_delta = event.mouseWheel.delta;
        } break;

        case sf::Event::MouseMoved: {
            mouse->position = sf::Vector2f((float)event.mouseMove.x, (float)event.mouseMove.y);
            mouse->moved = true;
        } break;

        default:
            break;
	}
}

void UIFree() {
	UIState *state = UIGetState();

	for (UIWindow *window : state->windows) {
		delete window;
	}

	state->windows.clear();
}

bool UILoadFont(const std::string &path) {
	return UIGetState()->font.loadFromFile(path);
}

UITheme *UIPushTheme() {
    UIState *state = UIGetState();
    state->theme_stack = state->theme;

    return &state->theme;
}

void UIPopTheme() {
    UIState *state = UIGetState();
    state->theme = state->theme_stack;
    state->theme_stack = {};
}

void UISetTheme(const UITheme &theme) {
	UIGetState()->theme = theme;
}

UITheme UICreateDefaultTheme() {
	UITheme theme = {};
	
	theme.colors[UIColor_Button] = sf::Color::White;
	theme.colors[UIColor_ButtonHovered] = sf::Color::Blue;
	theme.colors[UIColor_ButtonPressed] = sf::Color::Red;

	theme.colors[UIColor_Text] = sf::Color::Black;

	theme.colors[UIColor_Window] = sf::Color::Magenta;

    theme.character_size = 13;

	return theme;
}

float UIGetTextMetrics(const std::string &string) {
    UIState *state = UIGetState();
    UITheme *theme = &state->theme;

    sf::Text text;
    text.setFont(state->font);
    text.setCharacterSize(theme->character_size);
    text.setString(string);

    return text.getLocalBounds().width;
}

static void CaptureInput() {
	UIGetState()->capture_input_next_frame = true;
}

/*gui_draw.cpp*/

static void PushRect(UIDrawList *draw_list, const sf::FloatRect &rect, const sf::Color &color,
                     const sf::Texture *texture = nullptr, const sf::FloatRect *texture_rect = nullptr) {
	sf::VertexArray vertex_array;
	vertex_array.setPrimitiveType(sf::Quads);
	
	sf::Vertex vertices[4] = {
		sf::Vertex(sf::Vector2f(rect.left, rect.top), color),
		sf::Vertex(sf::Vector2f(rect.left + rect.width, rect.top), color),
		sf::Vertex(sf::Vector2f(rect.left + rect.width, rect.top + rect.height), color),
		sf::Vertex(sf::Vector2f(rect.left, rect.top + rect.height), color),
	};

	if (texture) {
		sf::FloatRect bounds = sf::FloatRect(0.0f, 0.0f, rect.width, rect.height);
        if (texture_rect) {
            bounds = *texture_rect;
        }

		vertices[0].texCoords = sf::Vector2f(bounds.left, bounds.top);
		vertices[1].texCoords = sf::Vector2f(bounds.left + bounds.width, bounds.top);
		vertices[2].texCoords = sf::Vector2f(bounds.left + bounds.width, bounds.top + bounds.height);
		vertices[3].texCoords = sf::Vector2f(bounds.left, bounds.top + bounds.height);
	}

	vertex_array.append(vertices[0]);
	vertex_array.append(vertices[1]);
	vertex_array.append(vertices[2]);
	vertex_array.append(vertices[3]);

	UIBatch batch = {};
	batch.vertices = vertex_array;
	batch.texture = texture;
    batch.clipRect = rect;

	draw_list->batches.push_back(batch);
}

static void PushTextSlow(UIDrawList *draw_list, const sf::Text &text, float *text_metrics = 0) {
	const sf::Font &font = UIGetState()->font;

	sf::Color color = text.getFillColor();
    unsigned int character_size = (unsigned int)UIGetState()->theme.character_size;

	sf::Vector2f position = text.getPosition();
	position.y += character_size;

	UIBatch batch = {};
	sf::VertexArray *vertex_array = &batch.vertices;
	vertex_array->setPrimitiveType(sf::Quads);

	const sf::Uint32 *ch = text.getString().getData(), *prev = ch;
	while (*ch) {
		sf::Glyph glyph = font.getGlyph(*ch, character_size, false);

		sf::Vertex vertices[4] = {};

		{
			sf::FloatRect bounds = glyph.bounds;
			vertices[0].position = position + sf::Vector2f(bounds.left, bounds.top);
			vertices[1].position = position + sf::Vector2f(bounds.left + bounds.width, bounds.top);
			vertices[2].position = position + sf::Vector2f(bounds.left + bounds.width, bounds.top + bounds.height);
			vertices[3].position = position + sf::Vector2f(bounds.left, bounds.top + bounds.height);
		}

		{
			sf::FloatRect bounds = (sf::FloatRect)glyph.textureRect;
			vertices[0].texCoords = sf::Vector2f(bounds.left, bounds.top);
			vertices[1].texCoords = sf::Vector2f(bounds.left + bounds.width, bounds.top);
			vertices[2].texCoords = sf::Vector2f(bounds.left + bounds.width, bounds.top + bounds.height);
			vertices[3].texCoords = sf::Vector2f(bounds.left, bounds.top + bounds.height);
		}

        {
            for (sf::Vertex &vertex : vertices) {
                vertex.color = color;
                vertex.position = sf::Vector2f(floorf(vertex.position.x), floorf(vertex.position.y));

                vertex_array->append(vertex);
            }
        }

		float advance = glyph.advance + font.getKerning(*ch, *prev, character_size);
        
        if (text_metrics)
        {
            *text_metrics += advance;
        }

		position.x += advance;

		if (*ch == '\n') {
			position.x = text.getPosition().x;
			position.y += font.getLineSpacing(character_size);
		}

		prev = ch;
		ch++;
	}

	batch.texture = &font.getTexture(character_size);

	draw_list->batches.push_back(batch);
}

static void DrawRectIm(sf::RenderTarget &render_target, const sf::FloatRect &rect, const sf::Color &color) {
	sf::RectangleShape shape;
	shape.setPosition(rect.left, rect.top);
	shape.setSize(sf::Vector2f(rect.width, rect.height));
	shape.setFillColor(color);

	render_target.draw(shape);
}

static void SetClipRect(const sf::FloatRect &rect) {
    UIState *state = UIGetState();

    glScissor(rect.left, state->viewport.y - (rect.height + rect.top), rect.width, rect.height);
}


static void DrawWindow(sf::RenderTarget &render_target, UIWindow *window) {
	if (!window->show) {
		return;
	}

    if (!(window->style & UIWindow_NoBackground)) {
        DrawRectIm(render_target, window->rect, window->color);
    }

    if (window->style & UIWindow_CloseButton) {
        float close_button_size = 25.0f;
        float offset = 7.0f;
    
        sf::Vector2f position;
        if (UIRawButton(window, sf::FloatRect(window->rect.width - close_button_size - offset, offset, close_button_size, close_button_size), "X", nullptr) == ButtonState::CLICKED) {
            window->show = false;
        }
    }

    //glEnable(GL_SCISSOR_TEST);

    SetClipRect(window->rect);

    UIDrawList *draw_list = &window->draw_list;
    for (const UIBatch &batch : draw_list->batches) {
        render_target.draw(batch.vertices, batch.texture);
    }

    glDisable(GL_SCISSOR_TEST);

    draw_list->batches.clear();
}

static void UpdateWindow(UIWindow *window) {
    UIInputOutput *io = UIGetIO();
    if (window->show && window->rect.contains(io->mouse.position)) {
        CaptureInput();
    }
}

void UIDraw(sf::RenderTarget &render_target) {
	UIState *state = UIGetState();
    UIInputOutput *io = UIGetIO();

	sf::Vector2f viewport = state->viewport;	
	
    DrawWindow(render_target, &state->desktop);

	for (UIWindow *window : state->windows) {
	    glEnable(GL_SCISSOR_TEST);
		glScissor((GLint)window->rect.left, (GLint)(viewport.y - (window->rect.height + window->rect.top)), (GLsizei)window->rect.width, (GLsizei)window->rect.height);
        DrawWindow(render_target, window);
        glDisable(GL_SCISSOR_TEST);
	}

    DrawWindow(render_target, &state->overlay);

	memset(io, 0, sizeof(UIInputOutput));
}

void UIUpdate(const sf::Window &window) {
	UIState *state = UIGetState();
	UIInputOutput *io = &state->input;

	UIInputMouse *mouse = &io->mouse;
	mouse->position = (sf::Vector2f)sf::Mouse::getPosition(window);
	mouse->down[0] = sf::Mouse::isButtonPressed(sf::Mouse::Left);
	mouse->down[1] = sf::Mouse::isButtonPressed(sf::Mouse::Right);
	mouse->down[2] = sf::Mouse::isButtonPressed(sf::Mouse::Middle);

	for (UIWindow *currentWindow : state->windows) {
        UpdateWindow(currentWindow);
	}

	if (state->capture_input_next_frame)
		io->captured = true;

	state->capture_input_next_frame = false;
}

/*gui_widgets.cpp*/

static inline sf::Vector2f UIWindowToScreen(UIWindow *window, sf::Vector2f position) {
    return vec2(position.x + window->rect.left, position.y + window->rect.top);
}

static inline sf::Vector2f UIScreenToWindow(UIWindow *window, sf::Vector2f position) {
    return vec2(position.x - window->rect.left, position.y - window->rect.top);
}

void UI::GetWindow(float x, float y, float width, float height, const std::string &name, uint32_t style, UIWindow **window) {
	UIState *state = UIGetState();
	UITheme *theme = &state->theme;

    if (!*window) {
        *window = new UIWindow();
        memset(*window, 0, sizeof(UIWindow));

        (*window)->show = true;
        (*window)->style = style;
        (*window)->name = name;
        (*window)->color = theme->colors[UIColor_Window];

        state->windows.push_back(*window);
    }
    
	(*window)->rect = Rect(x, y, width, height);
}

UIWindow *UI::GetOverlay() {
    UIState *state = UIGetState();

    return &state->overlay;
}

void UIToggleWindow(UIWindow *window) {
    window->show = !window->show;
}

void UIWindowShow(UIWindow *window) {
    window->show = true;
}

void UIWindowHide(UIWindow *window) {
    window->show = false;
}

bool UIWindowIsVisible(UIWindow *window) {
    return window->show;
}

sf::FloatRect UIWindowGetCenter(UIWindow *window, const sf::Vector2f &size, float y) {
    return Rect(window->rect.width / 2.0f - size.x / 2.0f, y, size.x, size.y);
}

ButtonState UIRawButton(UIWindow *window, const sf::FloatRect &position, const std::string &label, const sf::Texture *texture, const std::string *tooltip) {
	UIState *state = UIGetState();

	if (!window) {
		window = &state->desktop;
	}

	if (!window->show) {
		return DEFAULT;
	}

	UITheme *theme = &state->theme;

	sf::Color color = sf::Color::White;
	if (!texture) {
		color = theme->colors[UIColor_Button];
	}

    sf::FloatRect button_rect = Rect(UIWindowToScreen(window, rectPos(position)), rectSize(position));
    ButtonState result = DEFAULT;

	UIInputOutput *input = &state->input;
	if (button_rect.contains(input->mouse.position)) {
		CaptureInput();

        if (tooltip) {
            UIRawLabel(&state->overlay, input->mouse.position + sf::Vector2f(13.0f, 13.0f), *tooltip);
        }

		color = theme->colors[UIColor_ButtonHovered];
		if (input->mouse.down[0]) {
			color = theme->colors[UIColor_ButtonPressed];
		}

        result = ButtonState::HOVERED;
		if (input->mouse.pressed[0]) {
            result = ButtonState::CLICKED;
		}
	}

	UIDrawList *draw_list = &window->draw_list;
	PushRect(draw_list, button_rect, color, texture);

	{
		unsigned int character_size = 13;

		sf::Text text;
		text.setFont(state->font);
		text.setString(label);
		text.setCharacterSize(character_size);
		text.setFillColor(theme->colors[UIColor_Text]);
		text.setPosition(
			(button_rect.left + button_rect.width / 2.0f) - text.getLocalBounds().width / 2.0f,
			(button_rect.top + button_rect.height / 2.0f) - text.getLocalBounds().height);

		PushTextSlow(draw_list, text);
	}

	return result;
}

sf::FloatRect UIRawLabel(UIWindow *window, const sf::Vector2f &position, const std::string &format, UIAlign align, ...) {
	UIState *state = UIGetState();

    unsigned int character_size = UIGetState()->theme.character_size;

	if (!window) {
		window = &state->desktop;
	}

	UITheme *theme = &state->theme;

	sf::Text text;
	text.setFillColor(theme->colors[UIColor_Text]);
	text.setFont(state->font);
    text.setCharacterSize(character_size);
    text.setString(format);

    sf::FloatRect bounds = text.getLocalBounds();
    if (align == UIAlign_Center) {
        text.setPosition(window->rect.left + (window->rect.width / 2.0f - bounds.width / 2.0f), window->rect.top + position.y);
    }
    else
    {
        text.setPosition(window->rect.left + position.x, window->rect.top + position.y);
    }

    PushTextSlow(&window->draw_list, text);

	return text.getLocalBounds();
}

void UIRawImage(UIWindow *window, const sf::FloatRect &rect, const sf::Texture &texture, const sf::FloatRect *texture_rect, sf::Color color) {
	UIState *state = UIGetState();

	if (!window) {
		window = &state->desktop;
	}

	sf::FloatRect image_rect(
		sf::Vector2f(window->rect.left + rect.left, window->rect.top + rect.top),
		sf::Vector2f(rect.width, rect.height)
	);

    sf::FloatRect tex_coords = texture_rect ? *texture_rect : sf::FloatRect(0.0f, 0.0f, rect.width, rect.height);
	PushRect(&window->draw_list, image_rect, color, &texture, &tex_coords);
}

void UIRect(UIWindow *window, const sf::FloatRect &rect, const sf::Color &color) {
    UIState *state = UIGetState();

    if (!window) {
        window = &state->desktop;
    }

    sf::FloatRect image_rect(
        sf::Vector2f(window->rect.left + rect.left, window->rect.top + rect.top),
        sf::Vector2f(rect.width, rect.height)
    );

    PushRect(&window->draw_list, image_rect, color);
}

static void UIBeginDrag(UIWindow *window) {
    UIState *state = UIGetState();
    window->is_dragging = true;
}

static void UIEndDrag(UIWindow *window) {
    UIState *state = UIGetState();
    window->is_dragging = false;
}

static bool UIIsDraggin(UIWindow *window) {
    UIState *state = UIGetState();
    return window->is_dragging;
}

 void UISlider(UIWindow *window, float *value, float view_size, float contet_size) {
    float y_margin = 6.0f;
    float x_margin = 6.0f;

    if (window->style & UIWindow_CloseButton) {
        y_margin = 40.0f;
    }

    float bar_width = 12.0f;
    float bar_height = window->rect.height - x_margin * 2.0f;

    float thumb_height = 30.0f;

    if (contet_size < view_size) {
        thumb_height = bar_height;
    }
    else {
        thumb_height = bar_height * (view_size / contet_size);
    }

    float thumb_y = (bar_height - thumb_height -y_margin) * *value;

    sf::FloatRect bar_rect = Rect(window->rect.width - bar_width - x_margin, y_margin, bar_width, bar_height - y_margin);
    sf::FloatRect thumb_rect = Rect(bar_rect.left, bar_rect.top + thumb_y, bar_rect.width, thumb_height);

    UIInputOutput *io = UIGetIO();
    sf::Vector2f mouse_pos = UIScreenToWindow(window, io->mouse.position);
    if (bar_rect.contains(mouse_pos)) {
        CaptureInput();

        if (io->mouse.pressed[0] && contet_size > view_size)
        {        
            if (mouse_pos.y < thumb_y) {
                *value -= 0.2f;
            }
            else if (mouse_pos.y > thumb_y + y_margin + thumb_rect.height) {
                *value += 0.2f;
            }
            else {
                UIBeginDrag(window);
            }
        }
    }

    if (contet_size < view_size) {
        *value = 0.0f;
    }

    if (io->mouse.released[0]) {
        UIEndDrag(window);
    }

    UITheme *theme = &UIGetState()->theme;

    sf::Color bar_color = sf::Color(245, 245, 245); //TODO(Arc) theme->colors[UIColor_Bar];
    sf::Color thumb_color = theme->colors[UIColor_Button];

    if (UIIsDraggin(window)) {
        thumb_color = theme->colors[UIColor_ButtonPressed];

        if (io->mouse.moved) {

            *value = (mouse_pos.y - y_margin) / bar_rect.height;
        }
    }

    if (thumb_rect.contains(mouse_pos))
    {
        thumb_color = theme->colors[UIColor_ButtonHovered];

        if (io->mouse.down[0]) {
            thumb_color = theme->colors[UIColor_ButtonPressed];
        }
    }

    *value = std::clamp<float>(*value, 0.0f, 1.0f);

    UIRect(window, bar_rect, bar_color);
    UIRect(window, thumb_rect, thumb_color);
}

/*gui_layout.cpp*/
UILayout UIInitLayout(const sf::Vector2f &at, UILayoutType type) {
    UILayout layout = {};
    layout.type = type;
    layout.at = at;
    layout.spacing = 5.0f;
    layout.width = -layout.spacing;

    return layout;
}

void UIAdvanceLayout(UILayout *layout, const sf::Vector2f &size) {
    switch (layout->type) {
        case UILayout_Horizontal: {
            layout->at.x += size.x + layout->spacing;        
        } break;

        case UILayout_Vertical: {
            layout->at.y += size.y + layout->spacing;
        } break;

        default:
            break;
    }

    layout->width += size.x + layout->spacing;
}

ButtonState UIButton(UIWindow *window, const sf::Vector2f &size, UILayout *layout, const std::string &label,
              const sf::Texture *texture, const std::string *tooltip) {
    ButtonState result = UIRawButton(window, sf::FloatRect(sf::Vector2f(layout->at.x, layout->at.y), size),
                           label, texture, tooltip);

    UIAdvanceLayout(layout, size);
    return result;
}

sf::FloatRect UILabel(UIWindow *window, UILayout *layout, const std::string &format, UIAlign align, ...) {
    sf::FloatRect bounds = UIRawLabel(window, layout->at, format, align);
    UIAdvanceLayout(layout, sf::Vector2f(bounds.width, bounds.height));

    return bounds;
}

void UIImage(UIWindow *window, UILayout *layout, const sf::Vector2f &size, const sf::Texture &texture, sf::Color color) {
    UIRawImage(window, sf::FloatRect(layout->at, size), texture, nullptr, color);
    UIAdvanceLayout(layout, size);
}

void UISeparator(UIWindow *window, UILayout *layout) {
    if (layout->type == UILayout_Vertical) {
        layout->at.y += 8.0f;

        sf::FloatRect rect = Rect(0.0f, layout->at.y, window->rect.width, 1.0f);
        UIRect(window, rect, UIGetState()->theme.colors[UIColor_ButtonHovered]);

        layout->at.y += 5.0f;
    }
}

void UITextBox(UIWindow *window, const sf::FloatRect &rect, std::string &buffer) {

}

void registerGuiElements() {
    sol::state& lua = *ResourceManager::getInstance().getLuaHandle();
    lua.new_usertype<UIWindow>(
            "UIWindow", sol::constructors<>(),
            "rect", &UIWindow::rect,
            "color", &UIWindow::color,
            "name", &UIWindow::name,
            "draw_list", &UIWindow::draw_list,
            "show", &UIWindow::show,
            "is_dragging", &UIWindow::is_dragging
    );

    lua.new_usertype<UILayout>(
            "UILayout", sol::constructors<>(),
            "type", &UILayout::type,
            "at", &UILayout::at,
            "spacing", &UILayout::spacing,
            "width", &UILayout::width
    );

    lua.set_function("UIRawButton", UIRawButton);
    lua.set_function("UIButton", UIButton);

    lua.set_function("UIRawLabel", UIRawLabel);
    lua.set_function("UILabel", UILabel);

    lua.set_function("UIRawImage", UIRawImage);
    lua.set_function("UIImage", UIImage);

    lua.set_function("UIRect", UIRect);
    lua.set_function("UISlider", UISlider);
    //lua.set_function("UITextBox", UITextBox);
    lua.set_function("UISeparator", UISeparator);

    lua.set_function("UIInitLayout", UIInitLayout);
    lua.set_function("UIWindowGetCenter", UIWindowGetCenter);
}
