#ifndef IZOGAME_RESOURCEMANAGER_H
#define IZOGAME_RESOURCEMANAGER_H

#include <SFML/Graphics.hpp>
#include <experimental/filesystem>
#include "sol.hpp"
#include "../Logger.h"
#include "ExTexture.h"
#include "ManySprites.h"

class Animation;
class ResourceManager {
public:
    static ExTexture &getTexture(std::string id);
    static ManySprites &getMultiSprite(std::string id);
    static Animation &getSyncAnimation(std::string id);

    ResourceManager(const ResourceManager&) = delete;
    ResourceManager& operator = (const ResourceManager&) = delete;
    static ResourceManager &getInstance();

    static void registerForLua();
    sol::state* getLuaHandle();


    void loadAll();

private:
    std::map<std::string, ExTexture*> textures;
    std::map<std::string, ManySprites*> multiSprite;
    std::map<std::string, Animation*> syncAnimations;

    sol::state luaHandle;

    ResourceManager() = default;
    void registerAllParametersForLua();

    void findAndLoadPngFiles();
    void findAndLoadLuaFiles();
};

#endif //IZOGAME_RESOURCEMANAGER_H
