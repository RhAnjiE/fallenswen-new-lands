#ifndef IZOGAME_MANYSPRITES_H
#define IZOGAME_MANYSPRITES_H

#include <iostream>
#include <SFML/Graphics.hpp>
#include "../Logger.h"
#include "ExTexture.h"

class ManySprites {
public:
    explicit ManySprites(const ExTexture &texture);

    std::vector<std::vector<sf::Sprite>> sprites;

    sf::Sprite *getRandomSprite();
    sf::Sprite *getSprite(unsigned int x, unsigned int y);

    unsigned int getRows();
    unsigned int getColumns();
};

#endif //IZOGAME_MANYSPRITES_H
