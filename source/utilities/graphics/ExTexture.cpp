#include "ExTexture.h"
#include "ResourceManager.h"
#include "sol.hpp"

void ExTexture::init() {
    wFrame = this->getSize().x;
    hFrame = this->getSize().y;
}

void ExTexture::createMultiTexture(std::string type,
                                   unsigned int rows, unsigned int columns,
                                   unsigned int wFrame, unsigned int hFrame) {

    this->type = type;

    this->rows = rows;
    this->columns = columns;

    this->wFrame = wFrame;
    this->hFrame = hFrame;
}

void ExTexture::registerForLua() {
    sol::state& lua = *ResourceManager::getInstance().getLuaHandle();

    lua.new_usertype<ExTexture>(
            "ExTexture", sol::constructors<>(),
            "type", sol::readonly_property(&ExTexture::getType),
            sol::base_classes, sol::bases<sf::Texture>()
    );
}

