#include "ResourceManager.h"
#include "../ConfigParser.h"
#include "../../maps/Map.h"
#include "Animation.h"
#include <string>

namespace fs = std::experimental::filesystem;

void ResourceManager::loadAll() {
    this->registerAllParametersForLua();

    auto auxiliaryTexture = new ExTexture();
    auxiliaryTexture->loadFromFile("assets/default.png");
    textures["default"] = auxiliaryTexture;
    Logger::debug("Texture [default] loaded!");

    auxiliaryTexture = new ExTexture();
    auxiliaryTexture->loadFromFile("assets/delete.png");
    textures["delete"] = auxiliaryTexture;
    Logger::debug("Texture [delete] loaded!");

    this->findAndLoadPngFiles();
    this->findAndLoadLuaFiles();

    Logger::info("All textures are loaded");
}

void ResourceManager::findAndLoadPngFiles() {
    ExTexture* auxiliaryTexture = nullptr;

    for (const auto& entry : fs::recursive_directory_iterator("assets")) {
        std::string file = entry.path().filename().string();

        if (entry.path().string().find("script") != std::string::npos ||
            file.find("no_check") != std::string::npos)
            continue;

        if (file.find(".png") != std::string::npos) {
            file.erase(file.end() - 4, file.end());

            auxiliaryTexture = new ExTexture();
            auxiliaryTexture->loadFromFile(entry.path().string());
            auxiliaryTexture->init();

            textures[file] = auxiliaryTexture;
            Logger::debug("Texture [" + file + "] loaded!");
        }
    }
}

void ResourceManager::findAndLoadLuaFiles() {
    ExTexture* auxiliaryTexture = nullptr;

    for (const auto& entry : fs::recursive_directory_iterator("assets/scripts/resources")) {
        std::string file = entry.path().filename().string();
        if (file.find(".lua") != std::string::npos) {
            file.erase(file.end() - 4, file.end());

            luaHandle.script_file(entry.path().string());

            sol::table table = luaHandle[file];

            for (const auto &rawPair : table) {
                sol::object rawArg = rawPair.first;
                sol::object rawValue = rawPair.second;
            }

            std::string customTextureName = luaHandle[file]["customTextureName"];
            if (textures.find(file) == textures.end() && textures.find(customTextureName) == textures.end()) {
                Logger::warning("Found resource script but without texture! ID: '" + file + "'");
                continue;
            }

            if (textures.find(customTextureName) != textures.end()) {
                if (customTextureName.empty())
                    continue;

                auxiliaryTexture = textures[customTextureName];
                textures[file] = auxiliaryTexture;
            }

            else auxiliaryTexture = textures[file];

            std::string type = "single";
            if (luaHandle[file]["textureType"] != sol::nil)
                type = luaHandle[file]["textureType"];

            if (type == "multi" || type == "animation") {
                unsigned int rows = 1;
                unsigned int cols = 1;

                if (luaHandle[file]["rows"] != sol::nil)
                    rows = luaHandle[file]["rows"];

                if (luaHandle[file]["maxColumns"] != sol::nil)
                    cols = luaHandle[file]["maxColumns"];

                unsigned int wFrame = auxiliaryTexture->getSize().x / cols;
                unsigned int hFrame = auxiliaryTexture->getSize().y / rows;

                auxiliaryTexture->createMultiTexture(type, rows, cols, wFrame, hFrame);

                multiSprite[file] = new ManySprites(*auxiliaryTexture);
                Logger::debug("Multi texture [" + file + "] loaded!");
            }

            if (textures.find(file) == textures.end())
                Logger::debug("Texture [" + file + "] loaded!");

            textures[file] = auxiliaryTexture;

            if (type == "animation") {
                bool temp = luaHandle[file]["synchronizedAnim"];
                if (temp && luaHandle[file]["animations"] != sol::nil) {
                    syncAnimations[file] = new Animation(*textures[file]);
                    Logger::debug("SyncAnimation [" + file + "] loaded!");

                    syncAnimations[file]->loadAnimationsFromLua(file);
                }
            }
        }
    }
}


ExTexture &ResourceManager::getTexture(std::string id) {
    if (ResourceManager::getInstance().textures.find(id) == ResourceManager::getInstance().textures.end()) {
        Logger::warning("Cannot find texture with id: [" + id + "]");

        return *ResourceManager::getInstance().textures["default"];
    }

    return *ResourceManager::getInstance().textures[id];
}

ManySprites &ResourceManager::getMultiSprite(std::string id) {
    if (ResourceManager::getInstance().multiSprite.find(id) == ResourceManager::getInstance().multiSprite.end()) {
        Logger::warning("Cannot find multi sprite with id: [" + id + "]");

        return *new ManySprites(*ResourceManager::getInstance().textures["default"]);
    }

    return *ResourceManager::getInstance().multiSprite[id];
}

Animation& ResourceManager::getSyncAnimation(std::string id) {
    if (ResourceManager::getInstance().syncAnimations.find(id) == ResourceManager::getInstance().syncAnimations.end()) {
        Logger::warning("Cannot find synchronized animation with id: [" + id + "]");

        return *new Animation(*ResourceManager::getInstance().textures["default"]);
    }

    return *ResourceManager::getInstance().syncAnimations[id];
}

ResourceManager &ResourceManager::getInstance() {
    static ResourceManager resourceManager;
    return resourceManager;
}

sol::state *ResourceManager::getLuaHandle() {
    return &luaHandle;
}

void ResourceManager::registerForLua() {
    sol::state& lua = *ResourceManager::getInstance().getLuaHandle();

    /* -- SFML LIBRARY -- */
    lua.new_usertype<sf::Vector2f>(
            "Vector2f", sol::constructors<sf::Vector2f(float, float)>(),
            "x", &sf::Vector2f::x,
            "y", &sf::Vector2f::y
    );

    lua.new_usertype<sf::FloatRect>(
            "FloatRect", sol::constructors<sf::FloatRect(float, float, float, float)>(),
            "left", &sf::FloatRect::left,
            "top", &sf::FloatRect::top,
            "width", &sf::FloatRect::width,
            "height", &sf::FloatRect::height
    );

    lua.new_usertype<sf::Color>(
            "Color", sol::constructors<sf::Color(int, int, int), sf::Color(int, int, int, int)>()
    );

    lua.new_usertype<sf::Time>(
            "Time", sol::constructors<sf::Time()>(),
            "asSeconds", &sf::Time::asSeconds,
            "asMilliseconds", &sf::Time::asMilliseconds,
            "asMicroseconds", &sf::Time::asMicroseconds
    );

    lua.new_usertype<sf::Clock>(
            "Clock", sol::constructors<sf::Clock()>(),
            "getElapsedTime", &sf::Clock::getElapsedTime,
            "restart", &sf::Clock::restart
    );

    lua.set_function("getTexture", getTexture);
    //Todo: Register getMultiSprite and getSyncAnimation
}

//TODO: Find better way
void ResourceManager::registerAllParametersForLua() {
    luaHandle.open_libraries(sol::lib::base, sol::lib::package, sol::lib::math, sol::lib::string);

    ResourceManager::registerForLua();
    ExTexture::registerForLua();

    MapRenderedObject::registerForLua();
    MapTile::registerForLua();
    MapObject::registerForLua();
    MapBuilding::registerForLua();
    PlayerData::registerForLua();
    Human::registerForLua();

    Task::registerForLua();
    TaskGoTo::registerForLua();
    TaskGather::registerForLua();

    registerGuiElements();
    MessageLog::registerForLua();
}