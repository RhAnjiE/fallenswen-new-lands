#include "ExColor.h"

ExColor::ExColor(sf::Color color) {
    r = color.r;
    g = color.g;
    b = color.b;
    a = color.a;
}

sf::Color ExColor::getColor() {
    this->setInRange();

    return sf::Color((sf::Uint8) r, (sf::Uint8) g, (sf::Uint8) b, (sf::Uint8) a);
}

void ExColor::setColor(sf::Color color) {
    r = color.r;
    g = color.g;
    b = color.b;
    a = color.a;
}

void ExColor::setInRange() {
    if (r > 255) r = 255;
    if (g > 255) g = 255;
    if (b > 255) b = 255;

    if (r < 0) r = 0;
    if (g < 0) g = 0;
    if (b < 0) b = 0;
}

