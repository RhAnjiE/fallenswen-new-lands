#ifndef IZOGAME_EXCOLOR_H
#define IZOGAME_EXCOLOR_H

#include <iostream>
#include <SFML/Graphics.hpp>
#include "../Logger.h"

class ExColor {
public:
    int r = 0;
    int g = 0;
    int b = 0;
    int a = 255;

    explicit ExColor(sf::Color color = sf::Color());
    sf::Color getColor();
    void setColor(sf::Color color);

private:
    void setInRange();
};

#endif //IZOGAME_EXCOLOR_H
