#include "Pathfinder.h"
#include "../maps/Map.h"

Pathfinder::Pathfinder(const std::string &id, Map &map) {
    this->id = id;
    this->map = &map;
}

void Pathfinder::update() {
    this->step();
}

void Pathfinder::setStart(MapTile& newStart) {
    start = &newStart;

    if (start != nullptr) {
        start->addNewPathfind(id);
        start->setNodeState(id, Node::Start);
    }
}

void Pathfinder::setTarget(MapTile& newTarget) {
    target = &newTarget;

    if (target != nullptr) {
        target->addNewPathfind(id);
        target->setNodeState(id, Node::Target);
    }
}

void Pathfinder::setRange(unsigned int range) {
    maxCost = range;
}

void Pathfinder::clearStart() {
    start = nullptr;
}

void Pathfinder::clearTarget() {
    target = nullptr;
}

void Pathfinder::step() {
    if (start == nullptr || target == nullptr || status != Pathfinder::Working)
        return;

    if (openList.empty() && closedList.empty()) {
        openList.emplace_back(start);
        return;
    }

    if (!closedList.empty()) {
        if (openList.empty() || countTotalCost(closedList.back()) >= maxCost) {
            status = Pathfinder::NoPath;

            return;
        }

        if (closedList.back() == target) {
            status = Pathfinder::FoundPath;

            return;
        }
    }


    int index = this->findLowestScoreIndex();
    if (index != -1)
        this->exploreNeighbors(index);

    else status = Pathfinder::NoPath;
}

void Pathfinder::exploreNeighbors(int index) {
    MapTile* tile = *(openList.begin() + index);
    openList.erase(openList.begin() + index);
    closedList.emplace_back(tile);
    tile->addNewPathfind(id);
    tile->setNodeState(id, Node::ClosedList);

    //sf::Vector2i position = Camera::getMapPosition(tile->getCenterPosition());
    sf::Vector2i position = map->getMapCoordsFromIndex(tile->getIndex());
    std::vector<sf::Vector2i> neighboringPositions {
            sf::Vector2i(position.x + 1, position.y),
            sf::Vector2i(position.x - 1, position.y),
            sf::Vector2i(position.x, position.y + 1),
            sf::Vector2i(position.x, position.y - 1),

            /*sf::Vector2i(position.x + 1, position.y - 1),
            sf::Vector2i(position.x + 1, position.y + 1),
            sf::Vector2i(position.x - 1, position.y - 1),
            sf::Vector2i(position.x - 1, position.y + 1),*/
    };

    int i = 0;
    int currentCost = 0;
    for (auto &neighboringPosition : neighboringPositions) {
        if (neighboringPosition.x >= 0 && neighboringPosition.x < map->getMapSize().x &&
                neighboringPosition.y >= 0 && neighboringPosition.y < map->getMapSize().y) {

            unsigned int index = map->getIndexFromMapCoords(neighboringPosition);
            MapTile *neighbor = map->getTile(index);

            currentCost = cost;
            if (i >= 4) {
                currentCost = diagonalCost;

                //TODO: check if obstacle has no side gaps (like walls)
            }

            if (neighbor != nullptr) {
                if (neighbor->getState() != MapTile::Collision || neighbor == start || neighbor == target) {
                    if (std::find(closedList.begin(), closedList.end(), neighbor) == closedList.end()) {
                        if (std::find(openList.begin(), openList.end(), neighbor) == openList.end()) {
                            openList.push_back(neighbor);
                            neighbor->addNewPathfind(id);
                            openList.back()->setNodeState(id, Node::OpenList);

                            openList.back()->getNode(id)->setMovementCost(currentCost);
                            openList.back()->getNode(id)->setHeuristicCost(this->countHeuristic(openList.back()));
                            openList.back()->getNode(id)->setParent(closedList.back()->getNode(id));
                            openList.back()->setParent(id, closedList.back());
                        }
                    }
                }
            }
        }
    }
}

int Pathfinder::countTotalCost(MapTile* tile) {
    sf::Vector2i difference = Camera::getMapPosition(target->getCenterPosition()) - Camera::getMapPosition(tile->getCenterPosition());
    difference.x = abs(difference.x) * cost;
    difference.y = abs(difference.y) * cost;

    int heuristic = difference.x + difference.y;
    if (tile->getNode(id)->getParent() != nullptr)
        tile->getNode(id)->setMovementCost(tile->getNode(id)->getParent()->getMovementCost() + cost);
    else tile->getNode(id)->setMovementCost(cost);

    tile->getNode(id)->setHeuristicCost(heuristic);

    return heuristic + tile->getNode(id)->getMovementCost();
}

int Pathfinder::findLowestScoreIndex() {
    int lowestF = maxCost;
    int index = -1;

    for (int i = 0; i < openList.size(); i++) {
        int countedTotalCost = this->countTotalCost(openList[i]);

        if (lowestF > countedTotalCost) {
            lowestF = countedTotalCost;

            index = i;
        }
    }

    return index;
}

Pathfinder::Status Pathfinder::getStatus() {
    return status;
}

int Pathfinder::countHeuristic(MapTile *tile) {
    sf::Vector2i difference = Camera::getMapPosition(target->getCenterPosition()) - Camera::getMapPosition(tile->getCenterPosition());
    difference.x = abs(difference.x) * cost;
    difference.y = abs(difference.y) * cost;

    return difference.x + difference.y;
}

std::vector<MapTile*> &Pathfinder::getFoundPath() {
    path.clear();

    if (status == Pathfinder::FoundPath) {
        MapTile *tile = closedList.back();

        while (tile->getNode(id)->getParent() != nullptr) {
            path.emplace_back(tile);

            tile = tile->getParent(id);
        }

        this->clearAll();
    }

    return path;
}

void Pathfinder::clearAll() {
    this->clearStart();
    this->clearTarget();

    openList.clear();
    closedList.clear();

    status = Status::Working;
}

std::string Pathfinder::getId() {
    return id;
}
