#ifndef IZOGAME_NOISE_H
#define IZOGAME_NOISE_H

// Simplex noise SFML implementation - https://github.com/PawelWorwa/SimplexNoise

#include <utility>

#include <algorithm>
#include <cmath>
#include <random>

class Noise {
private:
    float frequency;
    static const std::pair<float, float> gradient[12];
    unsigned int octaves;
    static const unsigned short originalPermTab[256];
    unsigned short permTab[256];
    float persistence;
    static const float skewingFactor, unskewingFactor;

    float noise(float xPos, float yPos);
    float calculateCornerValue(float x, float y, int gradientIndex);
    float dot(std::pair<float, float> gradient, float x, float y);
    int fastfloor(float x);
    unsigned short hash(int i);
    float signedOctave(float xPos, float yPos);
    void randomizeSeed();

public:
    explicit Noise();
    virtual ~Noise();
    void setFrequency(float freq);
    void setOctaves(int oct);
    void setPersistence(float pers);
    float getValue(float xPos, float yPos);
};

#endif //IZOGAME_NOISE_H
