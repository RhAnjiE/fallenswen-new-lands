#ifndef IZOGAME_EXTENSIONS_H
#define IZOGAME_EXTENSIONS_H

#include <iostream>
#include <chrono>
#include <iomanip>
#include <vector>
#include <cmath>

std::string getCurrentDate(const std::string& format);
std::vector<std::string> split(char delim, std::string const &text);
int randInt(int min, int max);
int randInt(int max);

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Rect.hpp>

float vectorLength(sf::Vector2f vec);
sf::Vector2f normalize(sf::Vector2f vec);
sf::FloatRect maintainAspectRatio(sf::Vector2f windowSize, sf::Vector2f renderSize);

#endif //IZOGAME_EXTENSIONS_H
