#include "Extensions.h"

#include <sstream>

/**
 * Get the current date as a string
 * @param const std::string& format
 * @return std::string
 */
std::string getCurrentDate(const std::string& format) {
    auto currentDate = std::chrono::system_clock::now();
    std::time_t currentTime = std::chrono::system_clock::to_time_t(currentDate);

    std::stringstream stream;
    stream << std::put_time(std::localtime(&currentTime), format.c_str());

    return stream.str();
}

/**
 * Explode (split) string by delimeter (like explode func in PHP ;)
 * @param delim
 * @param text
 * @return std::vector<std::string>
 */
std::vector<std::string> split(char delim, std::string const &text) {
    std::vector<std::string> res;
    std::istringstream iss(text);
    for (std::string token; std::getline(iss, token, delim); ) {
        res.push_back(std::move(token));
    }
    return res;
}

/**
 * Get random number in min-max range (inclusive)
 * @param int min
 * @param int max
 * @return int
 */
int randInt(int min, int max) {
    if (max <= min) return min;
    return rand()%(max - min + 1) + min;
}

/**
 * Get random number from 0 to max (inclusive)
 * @param max
 * @return
 */
int randInt(int max) {
    return randInt(0, max);
}

sf::Vector2f normalize(sf::Vector2f vec)
{
	sf::Vector2f result = {};

	float length = vectorLength(vec);
	if (length > 0.0f)
	{
		result = sf::Vector2f(vec.x / length, vec.y / length);
	}

	return result;
}

float vectorLength(sf::Vector2f vec)
{
	return sqrtf((float)(vec.x * vec.x + vec.y * vec.y));
}

sf::FloatRect maintainAspectRatio(sf::Vector2f windowSize, sf::Vector2f renderSize)
{
	sf::FloatRect result = { 0.0f, 0.0f, 1.0f, 1.0f };

	float renderRatio = renderSize.x / renderSize.y;
	float windowRatio = windowSize.x / windowSize.y;

	if (windowRatio > renderRatio)
	{
		result.width = (windowSize.y * renderRatio) / windowSize.x;
		result.left = 0.5f - result.width / 2.0f;
	}
	else if (windowRatio < renderRatio)
	{
		result.height = (windowSize.x / renderRatio) / windowSize.y;
		result.top = 0.5f - result.height / 2.0f;
	}

	return result;
}