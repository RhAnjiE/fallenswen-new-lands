#include "Noise.h"

void Noise::setFrequency(float freq) {
    frequency = freq;
}

void Noise::setOctaves(int oct) {
    octaves = oct;
}

void Noise::setPersistence(float pers) {
    persistence = pers;
}

Noise::Noise() {
    octaves = 1;
    frequency = 1.f;
    persistence = .5f;
    std::copy(std::begin(originalPermTab), std::end(originalPermTab), std::begin(permTab));
    randomizeSeed();
}

const float Noise::skewingFactor = .5f * (sqrt(3.f) - 1.f);
const float Noise::unskewingFactor = (3.f - sqrt(3.f)) / 6.f;

const unsigned short Noise::originalPermTab[256] = {
    151, 160, 137, 91,  90,  15,  131, 13,  201, 95,  96,  53,  194, 233, 7,
    225, 140, 36,  103, 30,  69,  142, 8,   99,  37,  240, 21,  10,  23,  190,
    6,   148, 247, 120, 234, 75,  0,   26,  197, 62,  94,  252, 219, 203, 117,
    35,  11,  32,  57,  177, 33,  88,  237, 149, 56,  87,  174, 20,  125, 136,
    171, 168, 68,  175, 74,  165, 71,  134, 139, 48,  27,  166, 77,  146, 158,
    231, 83,  111, 229, 122, 60,  211, 133, 230, 220, 105, 92,  41,  55,  46,
    245, 40,  244, 102, 143, 54,  65,  25,  63,  161, 1,   216, 80,  73,  209,
    76,  132, 187, 208, 89,  18,  169, 200, 196, 135, 130, 116, 188, 159, 86,
    164, 100, 109, 198, 173, 186, 3,   64,  52,  217, 226, 250, 124, 123, 5,
    202, 38,  147, 118, 126, 255, 82,  85,  212, 207, 206, 59,  227, 47,  16,
    58,  17,  182, 189, 28,  42,  223, 183, 170, 213, 119, 248, 152, 2,   44,
    154, 163, 70,  221, 153, 101, 155, 167, 43,  172, 9,   129, 22,  39,  253,
    19,  98,  108, 110, 79,  113, 224, 232, 178, 185, 112, 104, 218, 246, 97,
    228, 251, 34,  242, 193, 238, 210, 144, 12,  191, 179, 162, 241, 81,  51,
    145, 235, 249, 14,  239, 107, 49,  192, 214, 31,  181, 199, 106, 157, 184,
    84,  204, 176, 115, 121, 50,  45,  127, 4,   150, 254, 138, 236, 205, 93,
    222, 114, 67,  29,  24,  72,  243, 141, 128, 195, 78,  66,  215, 61, 156,
    180
};

Noise::~Noise() {
}

unsigned short Noise::hash(int i) {
    return permTab[i&255];
}

const std::pair<float, float> Noise::gradient[12] = {
    std::make_pair(-1.0f, -1.0f), std::make_pair(-1.0f, 0.0f), std::make_pair(-1.0f, 0.0f),
    std::make_pair(-1.0f, 1.0f), std::make_pair(0.0f, -1.0f), std::make_pair(0.0f, -1.0f),
    std::make_pair(0.0f, 1.0f), std::make_pair(0.0f, 1.0f), std::make_pair(1.0f, -1.0f),
    std::make_pair(1.0f, 0.0f), std::make_pair(1.0f, 0.0f), std::make_pair(1.0f , 1.0f)
};

int Noise::fastfloor(float x) {
    return (x > 0) ? (int)x : (int)x - 1;
}

float Noise::dot(std::pair<float, float> gradient, float x, float y) {
    return gradient.first * x + gradient.second * y;
}

float Noise::noise(float xPos, float yPos) {
    float nCorner0, nCorner1, nCorner2;

    float skewedCell = (xPos + yPos) * skewingFactor;
    int simplexCellx = fastfloor(xPos + skewedCell);
    int simplexCelly = fastfloor(yPos + skewedCell);

    float unskewedCell = (simplexCellx + simplexCelly) * unskewingFactor;
    float X0 = simplexCellx - unskewedCell;
    float Y0 = simplexCelly - unskewedCell;
    float x0 = xPos - X0;
    float y0 = yPos - Y0;

    int i1 = (x0 > y0) ? 1 : 0;
    int j1 = (x0 > y0) ? 0 : 1;

    float x1 = x0 - i1 + unskewingFactor;
    float y1 = y0 - j1 + unskewingFactor;
    float x2 = x0 - 1.f + 2.f * unskewingFactor;
    float y2 = y0 - 1.f + 2.f * unskewingFactor;

    int ii = simplexCellx & 255;
    int jj = simplexCelly & 255;

    int gradientIndex0 = hash(ii + hash(jj)) % 12;
    int gradientIndex1 = hash(ii + i1 + hash(jj + j1)) % 12;
    int gradientIndex2 = hash(ii + 1 + hash(jj + 1)) % 12;

    nCorner0 = calculateCornerValue(x0, y0, gradientIndex0);
    nCorner1 = calculateCornerValue(x1, y1, gradientIndex1);
    nCorner2 = calculateCornerValue(x2, y2, gradientIndex2);

    return 70.0f * (nCorner0 + nCorner1 + nCorner2);
}

float Noise::signedOctave(float xPos, float yPos) {
    float amplitude = 1.0f;
    float maxValue  = 0.0f;
    float tmpFrequency = frequency;
    float total = 0.0f;

    for (std::size_t i = 0; i < octaves; ++i) {
        total += noise(xPos * tmpFrequency, yPos * tmpFrequency) * amplitude;
        maxValue += amplitude;
        amplitude *= persistence;
        tmpFrequency *= 2;
    }

    return total/maxValue;
}

float Noise::calculateCornerValue(float x, float y, int gradientIndex) {
    float corner = 0.0f;
    float t = 0.5f - x * x - y * y;
    if (t > 0) {
        t *= t;
        corner = t * t * dot(gradient[gradientIndex], x, y);
    }
    return corner;
}

float Noise::getValue(float xPos, float yPos) {
    return signedOctave(xPos, yPos) / 2 + .5f;
}

void Noise::randomizeSeed() {
    std::random_device randomDevice;
    std::mt19937 randomEngine(randomDevice());

    std::shuffle(std::begin(permTab), std::end(permTab), randomEngine);
}
