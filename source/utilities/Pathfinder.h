#ifndef ASTARALGORITHM_PATHFINDER_H
#define ASTARALGORITHM_PATHFINDER_H

//#include "../maps/Map.h"
#include "../maps/tiles/MapTile.h"
#include <cmath>
#include <iostream>
#include <limits.h>

class Map;
class Pathfinder {
public:
    enum Status {Working, FoundPath, NoPath};

    explicit Pathfinder(const std::string &id, Map& map);

    void setStart(MapTile &newStart);
    void setTarget(MapTile &newTarget);
    void setRange(unsigned int range);
    void update();

    std::vector<MapTile*>& getFoundPath();

    void clearAll();
    void clearStart();
    void clearTarget();

    Status getStatus();
    std::string getId();

private:
    std::vector<MapTile*> openList;
    std::vector<MapTile*> closedList;
    //std::priority_queue<MapTile*> openList;
    //std::priority_queue<MapTile*> closedList;
    std::vector<MapTile*>path;

    Status status = Status::Working;

    Map* map        = nullptr;
    MapTile* start  = nullptr;
    MapTile* target = nullptr;

    int cost = 10;
    int diagonalCost = 14;
    int maxCost = 10000;
    std::string id = "";

    void step();
    void exploreNeighbors(int index);

    int countHeuristic(MapTile* tile);
    int countTotalCost(MapTile* tile);
    int findLowestScoreIndex();
};

#endif //ASTARALGORITHM_PATHFINDER_H
