#include <regex>
#include "ConfigParser.h"
#include "Logger.h"

ConfigParser::ConfigParser(const std::string& filename) {
   this->load(filename);
}

void ConfigParser::load(const std::string &filename) {
    if (this->getFileContent(filename)) {
        this->parse();
    }
}

bool ConfigParser::getFileContent(const std::string& filename) {
    std::fstream file;
    file.open(filename, std::ios::in);
    if (file.fail()){
        Logger::error("Cannot open the \"" + filename + "\" file!");
        return false;
    }

    std::string line;
    while (!file.eof()) {
        std::getline(file, line);
        this->content += line + " ";
    }

    file.close();
    return true;
}

void ConfigParser::parse() {
    std::regex rSection(R"(\[([a-zA-Z0-9_-]+)\]([^\[]+)\[/\1\])"),
        rMainAttr(R"(([a-zA-Z0-9_-]+)\s*\{\s*([^\}]+)\s*\})"),
        rAttr(R"(\s*([a-zA-Z0-9_-]+)\s*=\s*([^;]+);\s*)");
    std::cmatch mSection, mMainAttr, mAttr;

    std::string input = this->content;
    while (std::regex_search(input.c_str(), mSection, rSection)) {
        std::string inside = mSection[2].str();

        while (std::regex_search(inside.c_str(), mMainAttr, rMainAttr)) {
            std::string insideAttr = mMainAttr[2].str();

            while (std::regex_search(insideAttr.c_str(), mAttr, rAttr)) {
                this->contentParsed[mSection[1].str()][mMainAttr[1].str()][mAttr[1].str()] = mAttr[2].str();
                insideAttr = mAttr.suffix();
            }
            inside = mMainAttr.suffix();
        }
        input = mSection.suffix();
    }
}

ConfigParserResult ConfigParser::get() {
    return this->contentParsed;
}