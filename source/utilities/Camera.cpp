#include "Camera.h"
#include <cmath>

#include "Extensions.h"

void Camera::handleEvent(sf::Event &event) {
    if (event.type == sf::Event::MouseWheelMoved) {
        if (event.mouseWheel.delta == 1 && view.getSize().x > 600 && view.getSize().y > 350)
            view.zoom(0.9f);
        if (event.mouseWheel.delta == -1 && view.getSize().x < 1800 && view.getSize().y < 1050)
            view.zoom(1.1f);
    }
}

void Camera::handleInput(sf::Vector2f mousePosition) {
	float final_speed = speed;

	sf::Vector2f direction = {};
	float dt = DeltaTime::getDt();

	if (sf::Mouse::isButtonPressed(sf::Mouse::Middle)) {
		view.move((mousePosition.x - getView().getCenter().x) * DeltaTime::getDt(),
			(mousePosition.y - getView().getCenter().y) * DeltaTime::getDt());
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		direction.y = -1.0f;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		direction.y = 1.0f;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		direction.x = 1.0f;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		direction.x = -1.0f;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
		final_speed = speed + 400.0f;

	direction = normalize(direction);

	view.move(direction * final_speed * dt);
}

sf::View &Camera::getView() {
    return view;
}

//TODO: Deprecated method
sf::Vector2f Camera::getIsoPosition(sf::Vector2i mapPosition) {
    return sf::Vector2f(
            (mapPosition.x - mapPosition.y) * (float)Map::TILE_WIDTH  / 2,
            (mapPosition.x + mapPosition.y) * (float)Map::TILE_HEIGHT / 2
    );
}

//TODO: Deprecated method
sf::Vector2i Camera::getMapPosition(sf::Vector2f isoPosition) {
    return sf::Vector2i(
            (int)((isoPosition.x / (Map::TILE_WIDTH  / 2) + (isoPosition.y / (Map::TILE_HEIGHT / 2)) + 1) / 2),
            (int)((isoPosition.y / (Map::TILE_HEIGHT / 2) - (isoPosition.x / (Map::TILE_WIDTH  / 2)) + 1) / 2)
    );
}