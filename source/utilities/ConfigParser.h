#ifndef IZOGAME_CONFIGPARSER_H
#define IZOGAME_CONFIGPARSER_H

#include <fstream>
#include <map>

typedef std::map<std::string, std::map<std::string, std::map<std::string, std::string>>> ConfigParserResult;

class ConfigParser {
private:

    std::string content;
    ConfigParserResult contentParsed;

    bool getFileContent(const std::string &filename);
    void parse();

public:
    ConfigParser() = default;
    explicit ConfigParser(const std::string &filename);
    void load(const std::string &filename);
    ConfigParserResult get();
};

#endif //IZOGAME_CONFIGPARSER_H
