#include "GameManager.h"

int main() {
    std::srand((unsigned int)std::time(nullptr));

    GameManager manager(1366, 768, "Falleswen - New Land");
    manager.runGame();

    return 0;
}