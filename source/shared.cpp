#include "shared.h"

bool testOverlap(const sf::FloatRect &a, const sf::FloatRect &b) {
    sf::Vector2f a_half = rectSize(a) / 2.0f;
    sf::Vector2f b_half = rectSize(b) / 2.0f;

    sf::Vector2f a_pos = rectPos(a);
    sf::Vector2f b_pos = rectPos(b);
   
    sf::Vector2f distance = (b_pos + b_half) - (a_pos + a_half);

    return
        (a_half.x + b_half.x) - fabsf(distance.x) > 0.0f &&
        (a_half.y + b_half.y) - fabsf(distance.y) > 0.0f;
}