#include "GameManager.h"

#include "stages/StageMenu.h"
#include "stages/StageGameplay.h"

#include <imgui.h>
#include <imgui-SFML.h>

GameManager::GameManager(unsigned int w, unsigned int h, const std::string& title) {
    Logger::setDebugMode(true);

    ResourceManager::getInstance().loadAll();

    settings.antialiasingLevel = 8;
    window.create(sf::VideoMode(w, h), title, sf::Style::Close, settings);
    //window.setFramerateLimit(120);

	menu     = new StageMenu(&window);
	gameplay = new StageGameplay(&window);

#ifdef _DEBUG
	{
		GameplayOptions options = {};
		options.mapSize = MAP_SIZE_SMALL;
		options.terrainType = MAP_CONTINENTS;
		options.numOfResources = MAP_RESOURCES_LITTLE;

		this->switchToGameplay(options);
	}
#else
	this->switchToMenu();
#endif
    Logger::info("Window was created!");

	ImGui::SFML::Init(window);
}

void GameManager::runGame() {
    while(window.isOpen()) {
		UIUpdate(window);
		if (stage) {
			stage->pollEvent(*this);
			stage->serve(*this);
		}
    }
};

void GameManager::setStage(Stage* newStage) {
	if (stage)
	    stage->exit();

    stage = newStage;
	stage->start();
}

Stage* GameManager::getStage() {
    return stage;
}

void GameManager::switchToMenu() {
	this->setStage(menu);
}

void GameManager::switchToGameplay(GameplayOptions &options) {
    gameplay->setMapSettings(options);

	this->setStage(gameplay);
}