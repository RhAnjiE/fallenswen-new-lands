#ifndef IZOGAME_MAP_H
#define IZOGAME_MAP_H

#include <SFML/Graphics/Texture.hpp>
#include "../utilities/Logger.h"
#include "../utilities/Camera.h"
#include "../utilities/graphics/ResourceManager.h"
#include "../utilities/graphics/ExColor.h"

#include "../players/PlayerData.h"

#include "MapInteraction.h"
#include "tiles/MapTile.h"
#include "objects/MapObject.h"
#include "objects/MapGrowablePlant.h"
#include "objects/buildings/MapBuilding.h"
#include "Shadow.h"

class Hud;
class Map {
public:
    enum Size{SMALL, MEDIUM, BIG, HUGE};
    enum Type{CONTINENTS, LAKES, ISLANDS, DESERT};
    enum ResourcesNum{LITTLE, NORMAL, RICH};
    enum CheckType{RECT, WHOLE, SURFACE};
    static const int TILE_WIDTH = 64, TILE_HEIGHT = 32;

    Map(Size size, Type type, ResourcesNum resourcesNum, PlayerData* player, Hud* hud);
    void setOptions(Size size, Type type, ResourcesNum resourcesNum);
    void generate();
    void generatePlants();

    void update(sf::Vector2f mousePosition, sf::View& view);
    void render(sf::RenderWindow &window);
    void interact(PlayerData& owner);

    bool isObjectVisible(sf::Vector2f objPos, sf::Vector2i objSize = sf::Vector2i(0, 0));

    unsigned int getTileIndex(sf::Vector2f position);
    MapTile* getTile(sf::Vector2f position, CheckType checkType);
    MapTile* getTile(unsigned int index);
    bool ifTileIsOnThePos(sf::Vector2f position, MapTile* tile, CheckType checkType);
    MapTile* getLowestTile();
    std::vector<MapTile*>* getChosenTiles();

    void setChosenTile(std::string newId);
    void setTile(const std::string newId, unsigned int index);

    std::string getObjectId(sf::Vector2f position);
    std::string getObjectIdFromAll(sf::Vector2f position);
    MapObject* getObject(sf::Vector2f position);
    MapObject* getObjectFromAll(sf::Vector2f position);
    MapObject* getObject(const std::string& id);
    MapObject* getChosenObject();

    MapObject* setObject(const std::string &newId, PlayerData& owner);
    MapObject* setObjectOnPosition(const std::string &newId, PlayerData& owner, sf::Vector2f position);
    void setChosenObject(MapObject* object);

    void removeSelectedObject();
    void removeObject(const std::string &id);

    MapInteraction &getInteraction();
    PlayerData* getPlayer();
    Hud* getHud();
    sf::Vector2i getMapSize();
    sf::Vector2i getMapCoordsFromIndex(unsigned int index);
    unsigned int getIndexFromMapCoords(sf::Vector2i position);

private:
    std::vector<MapTile> tiles;
    std::map<std::string, MapObject*> objects;

    MapInteraction mapInteraction;
    PlayerData* player;
    Hud* hud;

    int firstIndex = 0;
    int lastIndex = 0;

    std::vector<MapTile*>chosenTiles;
    MapObject* chosenObject = nullptr;

    Size size = SMALL;
    Type type = CONTINENTS;
    ResourcesNum resourcesNum = LITTLE;
    int sizeX = 35, sizeY = 35;

    int objectIndex = 0;
    sf::Vector2f viewableArea, viewableAreaSize;

    void choiceGoodObjectType(const std::string& typeId, const std::string& uniqueId,
            sf::Vector2f position, std::vector<MapTile*> &tilesUnder, PlayerData* owner);
};

#endif //IZOGAME_MAP_H
