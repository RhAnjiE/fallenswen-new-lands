#include "MapInteraction.h"
#include "Map.h"
#include "../players/PlayerData.h"
#include "../gui/hud/Hud.h"

MapInteraction::MapInteraction(Map* map)
    : map(map){
}

void MapInteraction::selectTileAndObject(Map* map, sf::Vector2f position) {
    for(auto &tile : *map->getChosenTiles()) {
        if (tile != nullptr) {
            if (map->getChosenObject() != nullptr) {
                map->getChosenObject()->resetColor();
                map->getChosenObject()->setShadowAlpha(255);
            }

            tile->resetColor();
        }
    }

    this->setNeighboringTiles(position);
    map->setChosenObject(map->getObject(position));

    if (map->getChosenObject() != nullptr) {
        map->getChosenObject()->saveCurrentColor();
        map->getChosenObject()->getSprite().setColor(sf::Color(100, 100, 100, 100));
        map->getChosenObject()->setShadowAlpha(80);
    }

    chosenObject.setColor(sf::Color(255, 255, 255, 120));
    canBuild = true;

    for(auto &tile : *map->getChosenTiles()) {
        if (tile != nullptr) {
            tile->saveCurrentColor();

            if (!this->checkRequiredResources(*map->getPlayer())) {
                chosenObject.setColor(sf::Color(255, 0, 0, 120));

                MessageLog &messageLog = map->getPlayer()->getMessageLog();
                messageLog.pushMessage("Not enough resources to building this object!");
            }

            MapObject* object = tile->getObjectOnTile();
            if (tile->getBuildBlock() || (object != nullptr && object->getBuildBlock())) {
                chosenObject.setColor(sf::Color(255, 255, 255, 0));
                tile->getSprite().setColor(sf::Color::Red);

                canBuild = false;
            }

            else tile->getSprite().setColor(sf::Color::Yellow);
        }

        else {
            canBuild = false;
        }
    }

    auto lowestTile = map->getLowestTile();
    if (!chosenId.empty() && lowestTile != nullptr) {
        position = sf::Vector2f(lowestTile->getPosition());
        position.x += Map::TILE_WIDTH / 2.0f;
        position.y += Map::TILE_HEIGHT / 2.0f;
        position.x -= ResourceManager::getTexture(chosenId).getWFrame() / 2.0f;
        position.y -= ResourceManager::getTexture(chosenId).getHFrame();

        sol::state& lua = *ResourceManager::getInstance().getLuaHandle();
        if (lua[chosenId] != sol::nil) {
            sol::table data = lua[chosenId];

            if (data["positionDiffY"] != sol::nil)
                position.y += (int)data["positionDiffY"];

            if (data["positionDiffY"] != sol::nil)
                position.x += (int)data["positionDiffX"];
        }

        chosenObject.setPosition(sf::Vector2f(position));
    }
}

MapObject* MapInteraction::buildObject(PlayerData& owner) {
    //TODO: Costs&stuff

    if (canBuild && this->checkRequiredResources(owner, true)) {
        auto object = map->setObject(chosenId, owner);
        
        return object;
    }

    return nullptr;
}

void MapInteraction::setChosenObject(const std::string& id) {
    chosenId = id;
    chosenObject = sf::Sprite();

    this->updateRequiredResourcesToBuild();

    if (!chosenId.empty()) {
        std::string type = "single";

        sol::state &lua = *ResourceManager::getInstance().getLuaHandle();
        if (lua[chosenId] != sol::nil) {
            sol::table data = lua[chosenId];

            if (data["textureType"] != sol::nil)
                type = data["textureType"];
        }

        if (type == "multi" || type == "animation")
            chosenObject = *ResourceManager::getMultiSprite(chosenId).getSprite(0, 0);

        else chosenObject.setTexture(ResourceManager::getTexture(chosenId));
    }
}

std::string MapInteraction::getChosenID() {
    return chosenId;
}

sf::Sprite &MapInteraction::getChosenObject() {
    return chosenObject;
}

void MapInteraction::setNeighboringTiles(sf::Vector2f position) {
    map->getChosenTiles()->clear();

    int tiledWidth = 1;
    int tiledHeight = 1;

    sol::state& lua = *ResourceManager::getInstance().getLuaHandle();
    if (lua[chosenId] != sol::nil) {
        sol::table data = lua[chosenId];

        if (data["tileWidth"] != sol::nil)
            tiledWidth = (int)data["tileWidth"];

        if (data["tileHeight"] != sol::nil)
            tiledHeight = (int)data["tileHeight"];
    }

    //Or (ceil(WH / 2.0f) - 1)

    MapTile* firstTile = map->getTile(position, Map::WHOLE);
    map->getChosenTiles()->emplace_back(firstTile);

    if (firstTile == nullptr)
        return;

    unsigned int index = firstTile->getIndex();
    unsigned int newIndex = 0;
    sf::Vector2i newPosition;

    //TODO: Add it
    double centeringX = floor(tiledWidth  / 2.0f);
    double centeringY = floor(tiledHeight / 2.0f);
    for(int y = 0; y < tiledHeight; y++) {
        for(int x = 0; x < tiledWidth; x++) {
            if (x == 0 && y == 0)
                continue;

            newPosition = map->getMapCoordsFromIndex(index);
            newPosition.x += x;
            newPosition.y += y;

            newIndex = map->getIndexFromMapCoords(newPosition);
            MapTile* nextTile = map->getTile(newIndex);

            if (nextTile == nullptr)
                return;

            if (find(map->getChosenTiles()->begin(), map->getChosenTiles()->end(), nextTile) == map->getChosenTiles()->end())
                map->getChosenTiles()->emplace_back(nextTile);
        }
    }
}

void MapInteraction::updateRequiredResourcesToBuild() {
    moneyCost = 0;
    woodCost  = 0;
    stoneCost = 0;

    sol::state* lua = ResourceManager::getInstance().getLuaHandle();

    if (chosenId.empty() || (*lua)[chosenId] == sol::nil)
        return;

    sol::table data = (*lua)[chosenId];

    if (data["moneyCost"] != sol::nil)
        moneyCost = (int)data["moneyCost"];

    if (data["woodCost"] != sol::nil)
        woodCost = (int)data["woodCost"];

    if (data["stoneCost"] != sol::nil)
        stoneCost = (int)data["stoneCost"];
}

bool MapInteraction::checkRequiredResources(PlayerData& owner, bool takeIt) {
    if(owner.getCityHall() == nullptr)
        return true;

    if (owner.getMoney() < moneyCost)
        return false;

    if (owner.getResource("wood") < woodCost ||
        owner.getResource("stone") < stoneCost)
        return false;

    if (takeIt) {
        owner.addMoney(-moneyCost);
        owner.addResource("wood", -woodCost);
        owner.addResource("stone", -stoneCost);
    }

    return true;
}

