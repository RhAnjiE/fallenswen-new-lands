#ifndef IZOGAME_SHADOW_H
#define IZOGAME_SHADOW_H


#include "../utilities/graphics/ExTexture.h"
#include "MapRenderedObject.h"

class Shadow : public MapRenderedObject {
public:
    Shadow(sf::Vector2f position, Map* mapHandle, float scale = 0);
};


#endif //IZOGAME_SHADOW_H
