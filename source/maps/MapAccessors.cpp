#include "Map.h"
#include "../gui/hud/Hud.h"

/* ---------------- [MAP TILES] ---------------- */

/**
 * Get the map tile index from the given screen position (in px)
 * @param sf::Vector2f position
 * @return unsigned int
 */
unsigned int Map::getTileIndex(sf::Vector2f position) {
    sf::Vector2i tiledPosition = Camera::getMapPosition(position);
    tiledPosition.x -= 1;

    int index = (tiledPosition.y * this->getMapSize().x + tiledPosition.x);

    if (index < 0)
        index = 0;

    if (index >= tiles.size())
        index = (int)(tiles.size() - 1);

    float height = tiles[index].getHeight();
    //float height = heights[index];
    //position.x += height / 2;
    position.y += height;

    tiledPosition = Camera::getMapPosition(position);
    tiledPosition.x -= 1;

    index = (tiledPosition.y * this->getMapSize().x + tiledPosition.x);

    if (index < 0)
        index = 0;

    if (index >= tiles.size())
        index = (int)(tiles.size() - 1);

    return (unsigned int)index;
}

/**
 * Get the map tile from the given screen position (in px)
 * @param sf::Vector2f position
 * @return MapTile*|nullptr
 */
MapTile* Map::getTile(sf::Vector2f position, CheckType checkType) {
    sf::Vector2f fixedPos = position;

    for (int i = lastIndex; i >= firstIndex; i--) {
        if (this->ifTileIsOnThePos(position, &tiles[i], checkType))
            return &tiles[i];
    }

    return nullptr;
}

/**
 * Get the map tile from the given index
 * @param unsigned int index
 * @return MapTile*|nullptr
 */
MapTile* Map::getTile(unsigned int index) {
    if (index < tiles.size() - 1)
        return &tiles[index];

    return nullptr;
}

bool Map::ifTileIsOnThePos(sf::Vector2f position, MapTile *tile, CheckType checkType) {
    if (tile->getSprite().getGlobalBounds().contains(position)) {
        switch(checkType) {
            case RECT:
                return true;

            case WHOLE: {
                sf::Image image = tile->getSprite().getTexture()->copyToImage();
                sf::Color color = image.getPixel(
                        (unsigned int) (position.x - tile->getPosition().x),
                        (unsigned int) (position.y - tile->getPosition().y));

                return ((int) color.a != 0);
            }

            case SURFACE: {
                sf::Vector2f fixedPos = position;
                fixedPos.x -= tile->getSprite().getPosition().x;
                fixedPos.y -= tile->getSprite().getPosition().y;

                return (fixedPos.x + fixedPos.y * 2 >= 32 &&
                        fixedPos.x - fixedPos.y * 2 <= 32 &&
                        fixedPos.y * 2 - fixedPos.x <= 32 &&
                        fixedPos.y * 2 + fixedPos.x <= 96);
            }
        }
    }

    return false;
}

/**
 * Change selected map tile
 * @param const std::string newId
 */
/*void Map::setChosenTile(const std::string newId) {
    if (chosenTiles[0] == nullptr)
        return;

    int index = this->getTileIndex(chosenTiles[0]->getPosition());
    if (index >= tiles.size())
        return;

    MapTile &tile = tiles[index];

    sf::Vector2f position;
    position.x = tile.getSprite().getPosition().x;
    position.y = tile.getSprite().getPosition().y;

    sol::state& lua = *ResourceManager::getInstance().getLuaHandle();
    if (lua[tile.getType()] != sol::nil) {
        sol::table data = lua[tile.getType()];

        if (data["positionDiffY"] != sol::nil)
            position.y -= (int)data["positionDiffY"];

        if (data["positionDiffY"] != sol::nil)
            position.x -= (int)data["positionDiffX"];
    }

    sol::table colors;
    sol::table colorsVar;
    int colorsInt[3];
    int colorsVarInt[3];

    if (lua[newId] != sol::nil) {
        sol::table data = lua[newId];

        if (data["colorBase"] != sol::nil) {
            colors = data["colorBase"];

            if (colors != sol::nil) {
                for (unsigned int i = 0; i < colors.size() && i < 3; i++) {
                    colorsInt[i] = colors[i + 1];
                }
            }
        }

        if (data["colorVariation"] != sol::nil) {
            colorsVar = data["colorVariation"];

            if (colorsVar != sol::nil) {
                for (unsigned int i = 0; i < colorsVar.size() && i < 3; i++) {
                    colorsVarInt[i] = colorsVar[i + 1];
                }
            }
        }

        bool isSynchronized = false;
        if (data["textureType"] != sol::nil && data["textureType"] == "animation") {
            if (data["synchronizedAnim"] != sol::nil)
                isSynchronized = data["synchronizedAnim"];
        }

        tile = MapTile(index, newId, ResourceManager::getTexture(newId), position, this, colorsInt, colorsVarInt, isSynchronized);
        tile.saveCurrentColor();
    }
}*/

/**
 * Change map tile from the given index
 * @param const std::string newId
 * @param unsigned int index
 */
/*void Map::setTile(const std::string newId, unsigned int index) {
    if (index >= tiles.size())
        return;

    MapTile &tile = tiles[index];

    sf::Vector2f position;
    position.x = tile.getSprite().getPosition().x;
    position.y = tile.getSprite().getPosition().y;

    sol::state& lua = *ResourceManager::getInstance().getLuaHandle();
    if (lua[tile.getType()] != sol::nil) {
        sol::table data = lua[tile.getType()];

        if (data["positionDiffY"] != sol::nil)
            position.y -= (int)data["positionDiffY"];

        if (data["positionDiffY"] != sol::nil)
            position.x -= (int)data["positionDiffX"];
    }

    sol::table colors;
    sol::table colorsVar;
    int colorsInt[3];
    int colorsVarInt[3];

    if (lua[newId] != sol::nil) {
        sol::table data = lua[newId];

        if (data["colorBase"] != sol::nil) {
            colors = data["colorBase"];

            if (colors != sol::nil) {
                for (unsigned int i = 0; i < colors.size() && i < 3; i++) {
                    colorsInt[i] = colors[i + 1];
                }
            }
        }

        if (data["colorVariation"] != sol::nil) {
            colorsVar = data["colorVariation"];

            if (colorsVar != sol::nil) {
                for (unsigned int i = 0; i < colorsVar.size() && i < 3; i++) {
                    colorsVarInt[i] = colorsVar[i + 1];
                }
            }
        }

        bool isSynchronized = false;
        if (data["textureType"] != sol::nil && data["textureType"] == "animation") {
            if (data["synchronizedAnim"] != sol::nil)
                isSynchronized = data["synchronizedAnim"];
        }

        tiles[index] = MapTile(index, newId, ResourceManager::getTexture(newId), position, this, colorsInt, colorsVarInt, isSynchronized);
        tiles[index].saveCurrentColor();
    }
}*/

std::vector<MapTile*> *Map::getChosenTiles() {
    return &chosenTiles;
}

MapTile* Map::getLowestTile() {
    return chosenTiles.back();
}

/* ---------------- [MAP OBJECTS] ---------------- */

/**
 * Get the map object id from the given screen position
 * @param sf::Vector2f position
 * @return std::string
 */
std::string Map::getObjectIdFromAll(sf::Vector2f position) {
    for(auto object = objects.rbegin(); object != objects.rend(); object++) {
        if ((object->second)->getSprite().getGlobalBounds().contains(position)) {
            sf::Image image = (object->second)->getSprite().getTexture()->copyToImage();
            sf::Color color = image.getPixel(
                    (unsigned int)(position.x - (object->second)->getPosition().x),
                    (unsigned int)(position.y - (object->second)->getPosition().y));

            if (color.a != 0)
                return object->first;
        }
    }

    return "";
}

/**
 * Get the map object id from the given screen position
 * @param sf::Vector2f position
 * @return std::string
 */
std::string Map::getObjectId(sf::Vector2f position) {
    /*for(auto object = drawable.rbegin(); object != drawable.rend(); object++) {
        if ((*object)->getSprite().getGlobalBounds().contains(position)) {
            sf::Image image = (*object)->getSprite().getTexture()->copyToImage();
            sf::Color color = image.getPixel(
                    (unsigned int)(position.x - (*object)->getPosition().x),
                    (unsigned int)(position.y - (*object)->getPosition().y));

            if (color.a != 0)
                return (*object)->getUniqueId();
        }
    }*/

    return "";
}

/**
 * Get the map object from the given screen position
 * @param sf::Vector2f position
 * @return MapObject*
 */
MapObject *Map::getObjectFromAll(sf::Vector2f position) {
    for(auto object = objects.rbegin(); object != objects.rend(); object++) {
        if (object->second == nullptr)
            continue;

        if ((object->second)->getSprite().getGlobalBounds().contains(position)) {
            sf::Image image = (object->second)->getSprite().getTexture()->copyToImage();
            sf::Color color = image.getPixel(
                    (unsigned int)(position.x - (object->second)->getPosition().x),
                    (unsigned int)(position.y - (object->second)->getPosition().y));

            if (color.a != 0)
                return object->second;
        }
    }

    return nullptr;
}

/**
 * Get only the visible and sorted map object from the given screen position
 * @param sf::Vector2f position
 * @return MapObject*
 */
MapObject *Map::getObject(sf::Vector2f position) {
    for (int i = lastIndex; i >= firstIndex; i--) {
        MapObject* object = tiles[i].getObjectOnTile();

        if (object == nullptr)
            continue;

        if (this->isObjectVisible(object->getPosition())) {
            if (object->getSprite().getGlobalBounds().contains(position)) {
                sf::Image image = object->getSprite().getTexture()->copyToImage();
                sf::FloatRect rect = object->getSprite().getLocalBounds();

                if ((unsigned int) (position.x - object->getPosition().x) <= rect.width - 1 &&
                    (unsigned int) (position.y - object->getPosition().y) <= rect.height - 1) {
                    sf::Color color = image.getPixel(
                            (unsigned int) (position.x - object->getPosition().x),
                            (unsigned int) (position.y - object->getPosition().y));

                    if (color.a != 0)
                        return object;
                }
            }
        }
    }

    return nullptr;
}

/**
 * Get the map object from the given id
 * @param const std::string &id
 * @return MapObject*
 */
MapObject *Map::getObject(const std::string &id) {
    auto foundObject = objects.find(id);

    return (foundObject != objects.end()) ? foundObject->second : nullptr;
}

/**
 * Set the new map object on chosen tile
 * @param const std::string &id
 * @param PlayerData& owner
 * @return MapObject*
 */
MapObject* Map::setObject(const std::string &newId, PlayerData& owner) {
    for(auto &tile : chosenTiles) {
        if (tile == nullptr || tile->getBuildBlock())
            return nullptr;
    }

    sf::Vector2f position = this->getLowestTile()->getPosition();
    position.x += Map::TILE_WIDTH / 2.0f;
    position.y += Map::TILE_HEIGHT / 2.0f;

    sol::state& lua = *ResourceManager::getInstance().getLuaHandle();
    std::string lowestTileType = this->getLowestTile()->getType();

    if (lua[lowestTileType] != sol::nil) {
        sol::table data = lua[lowestTileType];

        if (data["positionDiffY"] != sol::nil)
            position.y -= (int)data["positionDiffY"];

        if (data["positionDiffY"] != sol::nil)
            position.x -= (int)data["positionDiffX"];
    }

    float averageHeight = 0;
    for (auto &tile : chosenTiles) {
        MapObject* object = tile->getObjectOnTile();

        if (object != nullptr) {
            objects.erase(object->getUniqueId());
            delete object;
        }

        tile->setObjectOnTile(nullptr);

        averageHeight += tile->getHeight();
    }

    position.y += this->getLowestTile()->getHeight();

    averageHeight /= chosenTiles.size();
    for (auto &tile : chosenTiles) {
        tile->setHeight(averageHeight);
    }

    position.y -= averageHeight;

    std::string uniqueId = newId + std::to_string(objectIndex++);
    this->choiceGoodObjectType(newId, uniqueId, position, chosenTiles, &owner);

    return objects.find(uniqueId)->second;
}

MapObject *Map::setObjectOnPosition(const std::string &newId, PlayerData &owner, sf::Vector2f position) {
    MapTile* tile = this->getTile(position, Map::WHOLE);

    if (tile == nullptr)
        return nullptr;

    sol::state& lua = *ResourceManager::getInstance().getLuaHandle();
    std::string tileType = tile->getType();

    if (lua[tileType] != sol::nil) {
        sol::table data = lua[tileType];

        if (data["positionDiffY"] != sol::nil)
            position.y -= (int)data["positionDiffY"];

        if (data["positionDiffY"] != sol::nil)
            position.x -= (int)data["positionDiffX"];
    }

    MapObject* object = tile->getObjectOnTile();
    if (object != nullptr) {
        objects.erase(object->getUniqueId());

        tile->setObjectOnTile(nullptr);
    }

    std::vector<MapTile*>tiles;
    tiles.emplace_back(tile);

    std::string uniqueId = newId + std::to_string(objectIndex++);
    this->choiceGoodObjectType(newId, uniqueId, position, tiles, &owner);

    return objects.find(uniqueId)->second;
}

/**
 * Remove the selected map object
 */
void Map::removeSelectedObject() {
    for(auto &tileUnderObject : chosenObject->getTilesUnder())
        tileUnderObject->setObjectOnTile(nullptr);

    std::string id = chosenObject->getUniqueId();
    delete objects[id];
    objects.erase(id);

    chosenObject = nullptr;
}

/**
 * Remove the map object from the given id
 * @param const std::string &id
 */
void Map::removeObject(const std::string &id) {
    if (objects.find(id) != objects.end()) {
        for(int i = 0; i < objects[id]->getTilesUnder().size(); i++)
            objects[id]->getTilesUnder()[i]->setObjectOnTile(nullptr);

        delete objects[id];
        objects.erase(id);
    }
}

MapObject *Map::getChosenObject() {
    return chosenObject;
}

void Map::setChosenObject(MapObject *object) {
    chosenObject = object;
}

/* ---------------- [OTHER] ---------------- */

/**
 * Get the reference to the MapInteraction object that can manipulating map
 * @return MapInteraction&
 */
MapInteraction &Map::getInteraction() {
    return mapInteraction;
}

sf::Vector2i Map::getMapSize() {
    return sf::Vector2i(sizeX, sizeY);
}

PlayerData *Map::getPlayer() {
    return player;
}

Hud *Map::getHud() {
    return hud;
}

sf::Vector2i Map::getMapCoordsFromIndex(unsigned int index) {
    return sf::Vector2i(
            index % this->getMapSize().x,
            index / this->getMapSize().x);
}

unsigned int Map::getIndexFromMapCoords(sf::Vector2i position) {
    return (unsigned int) (position.x + this->getMapSize().x * position.y);
}
