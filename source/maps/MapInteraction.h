#ifndef IZOGAME_MAPINTERACTION_H
#define IZOGAME_MAPINTERACTION_H

#include <SFML/Graphics.hpp>

class Map;
class MapObject;
class PlayerData;
class MapInteraction {
public:
    explicit MapInteraction(Map* map);

    void selectTileAndObject(Map* map, sf::Vector2f position);
    void setChosenObject(const std::string& id);
    MapObject* buildObject(PlayerData& owner);

    std::string getChosenID();
    sf::Sprite &getChosenObject();

    bool canBuild = false;

private:
    std::string chosenId = "";
    sf::Sprite chosenObject = sf::Sprite();

    Map* map;

    int moneyCost = 0;
    int woodCost = 0;
    int stoneCost = 0;

    void setNeighboringTiles(sf::Vector2f position);
    void updateRequiredResourcesToBuild();
    bool checkRequiredResources(PlayerData& owner, bool takeIt = false);

};

#endif //IZOGAME_MAPINTERACTION_H
