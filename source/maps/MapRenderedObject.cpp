#include "MapRenderedObject.h"
#include "Map.h"

MapRenderedObject::MapRenderedObject(const std::string &type, const ExTexture &texture, sf::Vector2f position, Map* mapHandle)
    : type(type) {
    if (texture.getType() == "multi")
        sprite = *ResourceManager::getMultiSprite(type).getRandomSprite();

    else sprite.setTexture(texture);

    sprite.setPosition(position);
    this->loadLuaValues(texture);

    this->mapHandle = mapHandle;
}

MapRenderedObject::~MapRenderedObject() = default;

void MapRenderedObject::registerForLua() {
    sol::state* lua = ResourceManager::getInstance().getLuaHandle();
    lua->new_usertype<MapRenderedObject>(
            "MapRenderedObject", sol::constructors<>(),
            "type", sol::readonly_property(&MapRenderedObject::getType),
            "setColor", &MapRenderedObject::setColor,
            "red", sol::readonly_property(&MapRenderedObject::getRedColor),
            "green", sol::readonly_property(&MapRenderedObject::getGreenColor),
            "blue", sol::readonly_property(&MapRenderedObject::getBlueColor)

            //TODO: Register all properties and methods
    );
}

sf::Sprite &MapRenderedObject::getSprite() {
    return sprite;
}

/*void MapRenderedObject::setSprite(const sf::Texture &texture) {
    //sprite.setTexture(texture);
    //sprite = ResourceManager::getMultiSprite(id).getRandomSprite();
}*/

std::string MapRenderedObject::getType() const {
    return type;
}

sf::Vector2f MapRenderedObject::getPosition() const{
    return sprite.getPosition();
}

sf::Vector2f MapRenderedObject::getCenterPosition() const {
    return sf::Vector2f(sprite.getPosition().x + sprite.getGlobalBounds().width / 2, sprite.getPosition().y + sprite.getGlobalBounds().height / 2);
}

void MapRenderedObject::setPosition(sf::Vector2f newPosition) {
    sprite.setPosition(newPosition);
}

void MapRenderedObject::saveCurrentColor() {
    normalColor = sprite.getColor();
}

void MapRenderedObject::resetColor() {
    sprite.setColor(normalColor);
}

void MapRenderedObject::loadLuaValues(const ExTexture &texture) {
    sol::state& lua = *ResourceManager::getInstance().getLuaHandle();

    if (type == "shadow" || lua[type] == sol::nil)
        return;

    sol::table data = lua[type];

    positionDiff.x = data.get_or("positionDiffX", 0);
    positionDiff.y = data.get_or("positionDiffY", 0);

    sprite.move(positionDiff);
}

void MapRenderedObject::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    target.draw(this->sprite);
}

sf::Vector2i MapRenderedObject::getSize() const {
    return sf::Vector2i(
            (int)sprite.getGlobalBounds().width  / 2,
            (int)sprite.getGlobalBounds().height / 2);
}

Map *MapRenderedObject::getMapHandle() {
    return mapHandle;
}

void MapRenderedObject::setColor(int r, int g, int b) {
    sprite.setColor(sf::Color(r, g, b));
    this->saveCurrentColor();
}

int MapRenderedObject::getRedColor() {
    return normalColor.r;
}

int MapRenderedObject::getGreenColor() {
    return normalColor.g;
}

int MapRenderedObject::getBlueColor() {
    return normalColor.b;
}
