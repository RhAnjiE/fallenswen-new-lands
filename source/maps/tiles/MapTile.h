#ifndef IZOGAME_MAPTILE_H
#define IZOGAME_MAPTILE_H

#include "../MapRenderedObject.h"
#include "../../utilities/Node.h"
#include "../../utilities/graphics/Animation.h"

class MapObject;
class Human;
class MapTile : public MapRenderedObject {
public:
    enum State {NotCollision, Collision};

    MapTile(unsigned int index, const std::string &type, const ExTexture &texture, sf::Vector2f position, Map* mapHandle,
            sf::Color colorBase, sf::Color colorVar, bool synchronized, float height = 0);
    MapTile(const MapTile &mapTile);
    ~MapTile() override;

    static void registerForLua();

    void update() override;

    unsigned int getIndex();

    MapObject* getObjectOnTile();
    void setObjectOnTile(MapObject *object);

    void addHuman(Human* human);
    void removeHuman(Human *human);

    Animation* getAnimation();
    bool getBuildBlock();

    float getHeight();
    void setHeight(float height);
    sf::Vector2f getCenterPosition() const override;

    void setDefaultColor();

    void addNewPathfind(const std::string &id);
    void setNodeState(const std::string &id, Node::State newState);
    void setState(State newState);
    State getState();
    Node* getNode(const std::string &id);

    void setParent(const std::string &id, MapTile* newParent);
    MapTile* getParent(const std::string &id);

    std::vector<Human*>& getHumans();

protected:
    std::map<std::string, Node>nodes;
    std::map<std::string, MapTile*>parents;

    MapObject* object = nullptr;
    std::vector<Human*> humans;

    State state = State::NotCollision;
    Animation* animation = nullptr;
    sf::Color defaultColor;

    bool synchronized = false;
    bool buildBlock = false;
    float height = 0;
    unsigned int index = 0;

    void loadLuaValues(const ExTexture &texture) override;
};

#endif //IZOGAME_MAPTILE_H
