#include "MapTile.h"
#include "../objects/MapObject.h"
#include "../Map.h"
#include "../objects/people/Human.h"

MapTile::MapTile(unsigned int index, const std::string &type, const ExTexture &texture, sf::Vector2f position, Map* mapHandle,
                 sf::Color colorBase, sf::Color colorVar, bool synchronized, float height)
        : MapRenderedObject(type, texture, position, mapHandle) {
    this->loadLuaValues(texture);
    this->index = index;
    this->synchronized = synchronized;

    ExColor color(colorBase);
    color.r += randInt(-colorVar.r / 2, colorVar.r / 2);
    color.g += randInt(-colorVar.g / 2, colorVar.g / 2);
    color.b += randInt(-colorVar.b / 2, colorVar.b / 2);

    this->getSprite().setColor(color.getColor());
    this->setHeight(height);

    if (texture.getType() == "animation") {
        if (!synchronized) {
            animation = new Animation(texture);
            animation->loadAnimationsFromLua(type);
        }

        else animation = &ResourceManager::getSyncAnimation(type);

        animation->addSprite(&sprite);
    }

    defaultColor = sprite.getColor();
    this->saveCurrentColor();
}

MapTile::~MapTile() {
    if (animation != nullptr)
        animation->removeSprite(&sprite);
}

MapTile::MapTile(const MapTile &mapTile)
    : MapRenderedObject(mapTile){
    index           = mapTile.index;
    type            = mapTile.type;
    sprite          = mapTile.sprite;
    synchronized    = mapTile.synchronized;
    buildBlock      = mapTile.buildBlock;
    state           = mapTile.state;
    height          = mapTile.height;
    defaultColor    = mapTile.defaultColor;


    if (mapTile.animation != nullptr) {
        if ((mapTile.animation)->texture.getType() == "animation") {
            if (!synchronized) {
                animation = new Animation((mapTile.animation)->texture);
                animation->loadAnimationsFromLua(type);
            }

            else animation = &ResourceManager::getSyncAnimation(type);

            animation->addSprite(&sprite);
        }
    }
}

void MapTile::registerForLua() {
    sol::state* lua = ResourceManager::getInstance().getLuaHandle();
    lua->new_usertype<MapTile>(
            "MapTile", sol::constructors<>(),
            "centerPosition", sol::readonly_property(&MapTile::getCenterPosition),
            "setDefaultColor", &MapTile::setDefaultColor,
            sol::base_classes, sol::bases<MapRenderedObject>()

            //TODO: Register all properties and methods
    );
}

void MapTile::update() {
    MapRenderedObject::update();

    if (humans.size() > 1) {
        std::sort(humans.begin(), humans.end(), [](Human *first, Human *second) {
            return first->getPosition().y < second->getPosition().y;
        });
    }

    if (animation != nullptr)
        animation->play();
}

bool MapTile::getBuildBlock() {
    return buildBlock;
}

float MapTile::getHeight() {
    return height;
}

void MapTile::setHeight(float height) {
    sprite.move(0, this->height);

    this->height = height;
    sprite.move(0, -this->height);
}

MapObject *MapTile::getObjectOnTile() {
    return object;
}

void MapTile::setObjectOnTile(MapObject *object) {
    this->object = object;
}

void MapTile::loadLuaValues(const ExTexture &texture) {
    sol::state& lua = *ResourceManager::getInstance().getLuaHandle();
    if (lua[type] == sol::nil)
        return;

    sol::table data = lua[type];

    buildBlock = data.get_or("buildBlock", false);

    if (buildBlock)
        state = State::Collision;
}

Animation *MapTile::getAnimation() {
    return animation;
}

void MapTile::addNewPathfind(const std::string &id) {
    if (nodes.find(id) == nodes.end()) {
        nodes.insert(std::make_pair(id, Node(this->getPosition())));

        this->setState(state);
    }
}

Node* MapTile::getNode(const std::string &id) {
    auto foundNode = nodes.find(id);

    if (foundNode != nodes.end())
        return &foundNode->second;

    else return nullptr;
}

void MapTile::setNodeState(const std::string &id, Node::State newState) {
    auto foundNode = nodes.find(id);

    if (foundNode != nodes.end())
        foundNode->second.setState(newState);
}

void MapTile::setState(MapTile::State newState) {
    state = newState;
}

MapTile::State MapTile::getState() {
    return (this->getObjectOnTile() != nullptr && this->getObjectOnTile()->getMovingBlock()) ? State::Collision : state;
}

void MapTile::setParent(const std::string &id, MapTile *newParent) {
    parents[id] = newParent;
}

MapTile* MapTile::getParent(const std::string &id) {
    if (nodes.find(id) != nodes.end())
        return parents[id];

    else return nullptr;
}

void MapTile::setDefaultColor() {
    this->setColor((int)defaultColor.r, (int)defaultColor.g, (int)defaultColor.b);
}

unsigned int MapTile::getIndex() {
    return index;
}

void MapTile::addHuman(Human *human) {
    if (std::find(humans.begin(), humans.end(), human) == std::end(humans))
        humans.emplace_back(human);
}

void MapTile::removeHuman(Human *human) {
    auto it = std::find(humans.begin(), humans.end(), human);
    if (it != std::end(humans))
        humans.erase(it);
}

std::vector<Human*>& MapTile::getHumans() {
    return humans;
}

sf::Vector2f MapTile::getCenterPosition() const {
    return sf::Vector2f(
            sprite.getPosition().x + sprite.getGlobalBounds().width / 2,
            sprite.getPosition().y + sprite.getGlobalBounds().height / 2 - 10);
}
