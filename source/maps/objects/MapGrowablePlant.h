#ifndef IZOGAME_MAPGROWABLEPLANT_H
#define IZOGAME_MAPGROWABLEPLANT_H

#include "MapObject.h"
#include "../../utilities/DeltaTime.h"

class MapGrowablePlant : public MapObject {
public:
    MapGrowablePlant(const std::string &type, const std::string &uniqueId, const ExTexture &texture, sf::Vector2f position,
                Map* mapHandle, std::vector<MapTile*> &tilesUnder);
    ~MapGrowablePlant() override;

    void update() override;

    sf::Vector2f getPosition() const override;

protected:
    float timer = 0;
    float timeToGrow = 10;

    float rotation = 0;
    float rotationSpeed = 1.5;
    float rotationPower = 1;

    void loadLuaValues(const ExTexture &texture) override;
};

#endif //IZOGAME_MAPGROWABLEPLANT_H
