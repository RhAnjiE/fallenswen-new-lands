#ifndef IZOGAME_MAP_BUILDING_H
#define IZOGAME_MAP_BUILDING_H

#include "cmath"
#include <SFML/Graphics.hpp>
#include "../MapObject.h"
#include "../../../players/PlayerData.h"

class MapBuilding: public MapObject {
public:
    MapBuilding(const std::string &type, const std::string &uniqueId, const ExTexture &texture, sf::Vector2f position,
                Map* mapHandle, std::vector<MapTile*> &tilesUnder, PlayerData* owner);
    ~MapBuilding() override;

    void init() override;
    void update() override;
    void drawUI(UIWindow *window, UILayout *layout) override;

    static void registerForLua();

    Human* createWorker();
    Human* getWorker();

    void setEfficiency(float multiplier);
    void modifyEfficiency(float multiplier);
    float getEfficiency();

    int getOccupiedSpace();
    void setResource(int amount);
    int getCapacity();
    void setCapacity(int capacity);

    void setName(const std::string& name);
    std::string getName();
    void setFurtherDesc(const std::string& text);
    std::string getFurtherDesc();
    std::string getDesc();
    std::string getFullDesc();
    MapTile* getEntranceTile();

    int getHp();

protected:
    PlayerData* owner = nullptr;
    sf::Vector2i tileSize = sf::Vector2i(1, 1);
    bool isActive = true;
    Human* worker = nullptr;

    std::string name = "";
    std::string desc = "";
    std::string furtherDesc = "";
    std::string workerType = "";

    int occupiedSpace = 0;
    int capacity = 10;
    float efficiency = 2;
    int hp = 100;

    void onDestroy() override;
    void loadLuaValues(const ExTexture &texture) override;
};

#endif //IZOGAME_MAP_BUILDING_H
