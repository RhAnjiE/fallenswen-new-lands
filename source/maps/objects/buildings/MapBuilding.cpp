#include "MapBuilding.h"

MapBuilding::MapBuilding(const std::string &type, const std::string &uniqueId, const ExTexture &texture, sf::Vector2f position,
                         Map* mapHandle, std::vector<MapTile*> &tilesUnder, PlayerData* owner)
        : MapObject(type, uniqueId, texture, position, mapHandle, tilesUnder) {
    this->loadLuaValues(texture);

    this->owner = owner;

    if (texture.getType() == "multi")
        maxLevel = ResourceManager::getMultiSprite(type).getColumns();
    else maxLevel = 1;

    this->setLevel(1);
}

void MapBuilding::registerForLua() {
    sol::state* lua = ResourceManager::getInstance().getLuaHandle();
    lua->new_usertype<MapBuilding>(
            "MapBuilding", sol::constructors<>(),
            "name", sol::property(&MapBuilding::getName, &MapBuilding::setName),
            "desc", sol::readonly_property(&MapBuilding::getDesc),
            "worker", sol::readonly_property(&MapBuilding::getWorker),
            "entranceTile", sol::readonly_property(&MapBuilding::getEntranceTile),
            "entranceTile", sol::readonly_property(&MapBuilding::getEntranceTile),
            "furtherDesc", sol::property(&MapBuilding::getFurtherDesc, &MapBuilding::setFurtherDesc),
            "occupiedSpace", sol::property(&MapBuilding::getOccupiedSpace, &MapBuilding::setResource),
            "capacity", sol::property(&MapBuilding::getCapacity, &MapBuilding::setCapacity),
            "efficiency", sol::property(&MapBuilding::getEfficiency, &MapBuilding::setEfficiency),
            "modifyEfficiency", &MapBuilding::modifyEfficiency,

            "createWorker", &MapBuilding::createWorker,
            sol::base_classes, sol::bases<MapObject>()

            //TODO: Register all properties and methods
    );
}

void MapBuilding::init() {
    if (initFunction) {
        auto result = initFunction(*this, owner);

        if (!result.valid()) {
            sol::error err = result;
            Logger::error(err.what());
        }
    }

    else Logger::warning("Not found method body in lua script");
}

void MapBuilding::update() {
    if (worker != nullptr)
        worker->update();

    if (updateFunction) {
        auto result = updateFunction(*this, worker, owner);

        if (!result.valid()) {
            sol::error err = result;
            Logger::error(err.what());
        }
    }

    else Logger::warning("Not found method body in lua script");

    if (animation != nullptr)
        animation->play();
}

MapBuilding::~MapBuilding() {
    this->onDestroy();

    if (worker != nullptr) {
        owner->removeHuman(worker->getUniqueId());
        delete worker;
    }
}

void MapBuilding::onDestroy() {
    isDeleted = true;

    if (onDestroyFunction) {
        auto result = onDestroyFunction(*this, owner);

        if (!result.valid()) {
            sol::error err = result;
            Logger::error(err.what());
        }
    }

    else Logger::warning("Not found method body in lua script");
}

std::string MapBuilding::getName() {
    return name;
}

std::string MapBuilding::getDesc() {
    return desc + furtherDesc;
}

std::string MapBuilding::getFullDesc() {
    return desc + furtherDesc;
}

int MapBuilding::getHp() {
    return hp;
}

void MapBuilding::loadLuaValues(const ExTexture &texture) {
    sol::state& lua = *ResourceManager::getInstance().getLuaHandle();
    if (lua[type] == sol::nil) {
        Logger::warning("Building's data from lua not found!");

        return;
    }

    sol::table data = lua[type];

    name        = data["name"];
    desc        = data["description"];
    hp          = data.get_or("hp", 10);
    workerType  = data["workerType"];

    //TODO: Load entrance tile's index

    initFunction        = data["init"];
    updateFunction      = data["update"];
    drawUiFunction      = data["drawUi"];
    onDestroyFunction   = data["onDestroy"];
}

MapTile *MapBuilding::getEntranceTile() {
    return tilesUnder[tilesUnder.size() - 1];
}

void MapBuilding::setEfficiency(float multiplier) {
    this->efficiency = multiplier;

    if (efficiency < 0.2)
        efficiency = 0.2;

    if (worker != nullptr) {
        for (auto& [id, task] : worker->getTasks()) {
            task->setMultiplier(efficiency);
        }
    }
}

void MapBuilding::modifyEfficiency(float multiplier) {
    efficiency += multiplier;

    if (efficiency < 0.2)
        efficiency = 0.2;

    if (worker != nullptr) {
        for (auto& [id, task] : worker->getTasks()) {
            task->setMultiplier(efficiency);
        }
    }
}

float MapBuilding::getEfficiency() {
    return efficiency;
}

void MapBuilding::drawUI(UIWindow *window, UILayout *layout) {
    if (drawUiFunction) {
        auto result = drawUiFunction(*this, worker, owner, window, layout);

        if (!result.valid()) {
            sol::error err = result;
            Logger::error(err.what());
        }
    }

    else Logger::warning("Not found method body in lua script");
}

void MapBuilding::setFurtherDesc(const std::string &text) {
    this->furtherDesc = text;
}

int MapBuilding::getOccupiedSpace() {
    return occupiedSpace;
}

void MapBuilding::setResource(int amount) {
    occupiedSpace = amount;
}

int MapBuilding::getCapacity() {
    return capacity;
}

void MapBuilding::setCapacity(int capacity) {
    this->capacity = capacity;
}

void MapBuilding::setName(const std::string& name) {
    this->name = name;
}

Human* MapBuilding::createWorker() {
    if (worker != nullptr) {
        owner->removeHuman(worker->getUniqueId());
        delete worker;
    }

    worker = new Human(workerType, uniqueId + "_" + workerType, mapHandle, this, tilesUnder, owner);
    owner->addHuman(worker->getUniqueId(), worker);

    return worker;
}

Human *MapBuilding::getWorker() {
    return worker;
}

std::string MapBuilding::getFurtherDesc() {
    return furtherDesc;
}
