#ifndef IZOGAME_MAPPLANT_H
#define IZOGAME_MAPPLANT_H

#include <SFML/Graphics.hpp>
#include "../MapRenderedObject.h"
#include "../tiles/MapTile.h"
#include "../Shadow.h"

#include "../../gui/Gui.h"

class MapObject: public MapRenderedObject {
public:
    MapObject(const std::string &type, const std::string &uniqueId, const ExTexture &texture, sf::Vector2f position,
              Map* mapHandle, std::vector<MapTile*> &tilesUnder, bool registerOnTile = true, bool synchronized = false);
    ~MapObject();

    virtual void init();
    void update() override;
    virtual void drawUI(UIWindow *window, UILayout *layout);

    static void registerForLua();

    void levelUp();
    void setLevel(unsigned int newLevel);
    int getCurrentLevel();
    int getMaxLevel();
    bool isMaxLevel();

    void setShadowAlpha(sf::Uint8 alpha);

    std::string getUniqueId();
    bool getBuildBlock();
    bool getMovingBlock();
    sf::Vector2i getTiledSize();
    std::vector<MapTile*> getTilesUnder();
    MapTile* getLowestTile();

protected:
    //TODO: Look at this
    sf::Vector2i textureSize = sf::Vector2i(0, 0);
    sf::Vector2i tiledSize = sf::Vector2i(1, 1);

    std::string name = "Unnamed";
    std::string uniqueId = "";
    unsigned int currentLevel = 1;
    unsigned int maxLevel = 1;

    bool buildBlock = false;
    bool movingBlock = false;
    bool centerPosition = false;
    std::vector<MapTile*>tilesUnder;
    Shadow* shadow = nullptr;
    Animation* animation = nullptr;

    bool isDeleted = false;

    sol::protected_function initFunction;
    sol::protected_function updateFunction;
    sol::protected_function drawUiFunction;
    sol::protected_function onDestroyFunction;

    virtual void onDestroy();
    void updateLevel();

    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
    virtual void setCenteredPosition(sf::Vector2f position, bool centered);
    void loadLuaValues(const ExTexture &texture) override;
};


#endif //IZOGAME_MAPPLANT_H
