#include "MapGrowablePlant.h"
#include "../../utilities/Extensions.h"

MapGrowablePlant::MapGrowablePlant(const std::string &type, const std::string &uniqueId, const ExTexture &texture,
                  sf::Vector2f position, Map *mapHandle, std::vector<MapTile *> &tilesUnder)
        : MapObject(type, uniqueId, texture, position, mapHandle, tilesUnder) {
    this->loadLuaValues(texture);

    sprite.setOrigin(sprite.getGlobalBounds().width / 2, sprite.getGlobalBounds().height);
    sprite.move(sprite.getOrigin());
}

MapGrowablePlant::~MapGrowablePlant() = default;

void MapGrowablePlant::update() {
    MapObject::update();

    if (currentLevel < maxLevel) {
        timer += 1 * DeltaTime::getDt();

        if (timer >= timeToGrow) {
            this->levelUp();

            timer = 0;
        }
    }

    rotation += rotationSpeed / rotationPower * DeltaTime::getDt();
    if (currentLevel > 1 || currentLevel == maxLevel)
        sprite.setRotation((float) sin(rotation) * rotationPower);
}

sf::Vector2f MapGrowablePlant::getPosition() const {
    return MapRenderedObject::getPosition() - sprite.getOrigin();
}

void MapGrowablePlant::loadLuaValues(const ExTexture &texture) {
    sol::state* lua = ResourceManager::getInstance().getLuaHandle();
    if ((*lua)[type] == sol::nil) {
        Logger::warning("MapGrowablePlant's data from lua not found!");

        return;
    }

    sol::table data = (*lua)[type];

    if (data["rotationSpeed"] != sol::nil)
        rotationSpeed = data["rotationSpeed"];

    if (data["rotationSpeed"] != sol::nil)
        rotationSpeed = data["rotationSpeed"];

    if (data["rotationPower"] != sol::nil)
        rotationPower = data["rotationPower"];

    if (data["rotationRandomize"] != sol::nil)
        rotation = randInt(30);

    float growMinTime = 10.0f;
    float growMaxTime = 10.0f;

    if (data["growMinTime"] != sol::nil)
        growMinTime = data["growMinTime"];

    if (data["growMaxTime"] != sol::nil)
        growMaxTime = data["growMaxTime"];

    //Todo: Needed randFloat() function
    timeToGrow = (float)randInt((int)(growMinTime * 100), (int)(growMaxTime * 100)) / 100;
}
