#include "MapObject.h"
#include "../Map.h"

MapObject::MapObject(const std::string &type, const std::string &uniqueId, const ExTexture &texture, sf::Vector2f position,
                     Map* mapHandle, std::vector<MapTile*> &tilesUnder, bool registerOnTile, bool synchronized)
    : MapRenderedObject(type, texture, position, mapHandle) {
    this->loadLuaValues(texture);

    this->uniqueId = uniqueId;
    this->textureSize = sf::Vector2i(texture.getWFrame(), texture.getHFrame());
    this->tilesUnder = tilesUnder;

    if (registerOnTile) {
        for (auto tile : this->tilesUnder) {
            tile->setObjectOnTile(this);
        }
    }

    this->setCenteredPosition(sprite.getPosition(), centerPosition);

    maxLevel = 1;
    if (texture.getType() == "multi")
        maxLevel = ResourceManager::getMultiSprite(type).getColumns();

    else if (texture.getType() == "animation") {
        if (!synchronized) {
            animation = new Animation(texture);
            animation->loadAnimationsFromLua(type);
        }

        else animation = &ResourceManager::getSyncAnimation(type);

        animation->addSprite(&sprite);
    }

    this->setLevel(1);
}

MapObject::~MapObject() {
    if (animation != nullptr)
        animation->removeSprite(&sprite);

    if (!isDeleted)
        this->onDestroy();
}

void MapObject::onDestroy() {
    if (onDestroyFunction) {
        auto result = onDestroyFunction(*this);

        if (!result.valid()) {
            sol::error err = result;
            Logger::error(err.what());
        }
    }
}

void MapObject::registerForLua() {
    sol::state* lua = ResourceManager::getInstance().getLuaHandle();
    lua->new_usertype<MapObject>(
            "MapObject", sol::constructors<>(),
            "currentLevel", sol::property(&MapObject::getCurrentLevel, &MapObject::setLevel),
            "maxLevel", sol::readonly_property(&MapObject::getMaxLevel),
            "tilesUnder", &MapObject::tilesUnder,
            "lowestTile", sol::readonly_property(&MapObject::getLowestTile),
            sol::base_classes, sol::bases<MapRenderedObject>()

            //TODO: Register all properties and methods
    );
}

void MapObject::init() {
    if (initFunction) {
        auto result = initFunction(*this);

        if (!result.valid()) {
            sol::error err = result;
            Logger::error(err.what());
        }
    }
}

void MapObject::setCenteredPosition(sf::Vector2f position, bool centered) {
    float newX = sprite.getPosition().x - textureSize.x / 2;
    float newY = sprite.getPosition().y - textureSize.y;
    int yAdjustment = 0;

    for(auto tile : this->tilesUnder) {
        if (yAdjustment > tile->getHeight())
            yAdjustment = tile->getHeight();
    }

    this->setPosition(sf::Vector2f(newX, newY + yAdjustment));
}

void MapObject::update() {
    MapRenderedObject::update();

    if (updateFunction) {
        auto result = updateFunction(*this);

        if (!result.valid()) {
            sol::error err = result;
            Logger::error(err.what());
        }
    }

    if (animation != nullptr)
        animation->play();
}

std::string MapObject::getUniqueId() {
    return uniqueId;
}

sf::Vector2i MapObject::getTiledSize() {
    return tiledSize;
}

bool MapObject::getBuildBlock() {
    return buildBlock;
}

std::vector<MapTile*> MapObject::getTilesUnder() {
    return tilesUnder;
}

void MapObject::loadLuaValues(const ExTexture &texture) {
    sol::state& lua = *ResourceManager::getInstance().getLuaHandle();
    if (lua[type] == sol::nil)
        return;

    sol::table data = lua[type];

    movingBlock     = data.get_or("movingBlock", false);
    buildBlock      = data.get_or("buildBlock", false);
    tiledSize.x     = data.get_or("tileWidth", 1);
    tiledSize.x     = data.get_or("tileHeight", 1);
    centerPosition  = data.get_or("centerPos", false);

    initFunction        = data["init"];
    updateFunction      = data["update"];
    drawUiFunction      = data["drawUi"];
    onDestroyFunction   = data["onDestroy"];

    if (data.get_or("useShadow", false) == true) {
        float shadowX = sprite.getPosition().x + data.get_or("shadowX", 0);
        float shadowY = sprite.getPosition().y + data.get_or("shadowY", 0);
        float shadowScale = data.get_or("shadowScale", 0);

        shadow = new Shadow(sf::Vector2f(shadowX, shadowY), this->getMapHandle(), shadowScale);
    }

    if (data["colorVariation"] != sol::nil) {
        sol::table colorVariation = data["colorVariation"];

        sf::Color color(200,200,200);
        color.r += randInt(-static_cast<int>(colorVariation[0]), static_cast<int>(colorVariation[0]));
        color.g += randInt(-static_cast<int>(colorVariation[1]), static_cast<int>(colorVariation[1]));
        color.b += randInt(-static_cast<int>(colorVariation[2]), static_cast<int>(colorVariation[2]));
        sprite.setColor(color);
    }
}

void MapObject::setShadowAlpha(sf::Uint8 alpha) {
    if (shadow != nullptr) {
        sf::Color newColor = shadow->getSprite().getColor();
        newColor.a = alpha;

        shadow->getSprite().setColor(newColor);
    }
}

void MapObject::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    if (shadow != nullptr)
        target.draw(*shadow);

    MapRenderedObject::draw(target, states);
}

void MapObject::drawUI(UIWindow *window, UILayout *layout) {
    if (drawUiFunction) {
        auto result = drawUiFunction(*this, window, layout);

        if (!result.valid()) {
            sol::error err = result;
            Logger::error(err.what());
        }
    }
}

MapTile *MapObject::getLowestTile() {
    return (!this->getTilesUnder().empty() ? this->getTilesUnder().back() : nullptr);
}

bool MapObject::getMovingBlock() {
    return movingBlock;
}

void MapObject::levelUp() {
    if (currentLevel < maxLevel)
        currentLevel++;

    else currentLevel = maxLevel;

    this->updateLevel();
}

void MapObject::setLevel(unsigned int newLevel) {
    if (newLevel <= maxLevel)
        currentLevel = newLevel;

    else currentLevel = maxLevel;

    this->updateLevel();
}

int MapObject::getCurrentLevel() {
    return currentLevel;
}

void MapObject::updateLevel() {
    auto position = getPosition();
    auto origin = sprite.getOrigin();
    auto scale = sprite.getScale();

    if (maxLevel > 1) {
        sprite = *ResourceManager::getMultiSprite(type).getSprite(currentLevel - 1, 0);
        sprite.setOrigin(origin);
        sprite.setScale(scale);
        sprite.setPosition(position + sprite.getOrigin());
    }

    else sprite.setTexture(ResourceManager::getTexture(type));
}

int MapObject::getMaxLevel() {
    return maxLevel;
}

bool MapObject::isMaxLevel() {
    return currentLevel >= maxLevel;
}
