#ifndef IZOGAME_TASK_H
#define IZOGAME_TASK_H

#include <SFML/Graphics.hpp>
#include "../../../utilities/graphics/Animation.h"
#include "../../../utilities/DeltaTime.h"
#include <cmath>

///Todo: Add Lua support for workers' tasks

class Human;
class Task {
public:
    Task(const std::string& id, Human* executor, const std::string& animationId);

    enum Status {UNFINISHED, BROKEN, FINISHED};

    virtual void start();
    virtual void executeTask();

    static void registerForLua();

    std::string getId();
    Status getStatus();

    void setBehaviourIfSuccess(std::function<void()> ifSuccess);
    void setBehaviourIfFail(std::function<void()> ifFail);

    void setMultiplier(float multiplier);

    bool finishTask();

protected:
    std::string id;
    float multiplier = 2;

    Human* executor = nullptr;
    std::string animationId = "";

    std::function<void()> ifSuccess = [&]() {};
    std::function<void()> ifFail    = [&]() {};

    bool executedFinishCode = false;
    Status status = UNFINISHED;

    virtual void reset();
};

#endif //IZOGAME_TASK_H
