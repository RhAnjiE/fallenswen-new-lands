#ifndef IZOGAME_HUMAN_H
#define IZOGAME_HUMAN_H

#include <SFML/Graphics.hpp>
#include "../../../utilities/graphics/Animation.h"
#include "../../MapRenderedObject.h"
#include "TaskGoTo.h"
#include "TaskGather.h"
#include "TaskWait.h"
//#include "../../utilities/Camera.h"

class MapBuilding;
class PlayerData;
class Camera;
class Map;
class Human: public MapObject {
public:
    Human(const std::string &type, const std::string &uniqueId, Map* mapHandle, MapBuilding* building,
          std::vector<MapTile*> &tilesUnder, PlayerData* owner);
    ~Human();

    void update() override;
    static void registerForLua();

    void defineTaskGoTo(const std::string &id, TaskGoTo& task);
    void defineTaskGather(const std::string &id, TaskGather& task);

    void changeTask(const std::string &id);
    void resetCurrentTask();
    void finishCurrentTask();
    std::shared_ptr<Task> getTask(const std::string &id);
    std::shared_ptr<Task> getCurrentTask();
    std::map<std::string, std::shared_ptr<Task>>& getTasks();

    void setAnimation(const std::string& id);

    void move(sf::Vector2f offset);
    void setTileUnderFeet(MapTile* tile);
    sf::Vector2i getTiledPosition();
    sf::Vector2f getPosition() const override;
    void setPosition(sf::Vector2f newPosition) override;

    PlayerData* getOwner();
    std::string getName();
    float getSpeed();

    void wait(float seconds);

    bool isIdle();
    bool isWaiting();

private:
    Animation* animation;
    sf::Vector2i positionOnTile;

    MapBuilding* building;
    PlayerData* owner;

    sf::Vector2i lastTiledPosition = sf::Vector2i(0, 0);

    //TODO: Scriptable test
    std::map<std::string, std::shared_ptr<Task>>definedTasks;
    std::shared_ptr<Task> currentTask = nullptr;
    //sol::state luaHandle;
    float timer = 0;
    float timeToWait = 0;

    //TODO: Add names list
    int health = 100;
    float speed = 50;

    void loadLuaValues(const ExTexture &texture) override;
};

#endif //IZOGAME_HUMAN_H
