#include "TaskGoTo.h"
#include "Human.h"
#include "../../../utilities/Camera.h"

TaskGoTo::TaskGoTo(const std::string& id, Human* executor, sf::Vector2f newPosition, const std::string& animationId)
        : Task(id, executor, animationId) {

    this->setPosition(newPosition);
}

TaskGoTo::TaskGoTo(const std::string &id, Human *executor, const std::string& targetId, unsigned int range, const std::string& animationId)
        : Task(id, executor, animationId) {

    this->setTargetId(targetId);
    this->setRange(range);
}

void TaskGoTo::registerForLua() {
    sol::state* lua = ResourceManager::getInstance().getLuaHandle();
    lua->new_usertype<TaskGoTo>(
            "TaskGoTo", sol::constructors<
                    TaskGoTo(const std::string&, Human*, const std::string&, unsigned int, const std::string&),
                    TaskGoTo(const std::string&, Human*, sf::Vector2f, const std::string&)>(),
            sol::base_classes, sol::bases<Task>()
            //TODO: Register all properties and methods
    );
}

void TaskGoTo::start() {
    Task::start();

    if (!targetId.empty())
        this->newPosition = this->findClosestObject(targetId, range);

    if (status != UNFINISHED)
        return;

    if (pathfinder == nullptr) {
        this->findNewPath(executor->getPosition(), newPosition);

        executor->setAnimation("idle");
    }

    if (sf::Vector2i(executor->getPosition()) == sf::Vector2i(newPosition)) {
        status = FINISHED;

        return;
    }
}

void TaskGoTo::executeTask() {
    Task::executeTask();

    if (pathfinder == nullptr || status != UNFINISHED)
        return;

    if (pathfinder->getStatus() == Pathfinder::FoundPath || found) {
        if (!found) {
            found = true;
            executor->setAnimation(animationId);

            path = pathfinder->getFoundPath();
            if (!path.empty()) {
                currentTile = path.back();

                if (currentTile == nullptr) {
                    status = BROKEN; return;
                }
            }

            else { status = BROKEN; return; }
        }

        executorSpeed = executor->getSpeed() * DeltaTime::getDt();
        sf::Vector2f moveOffset = sf::Vector2f(0, 0);

        if (executor->getPosition().x < currentTile->getCenterPosition().x)
            moveOffset.x = executorSpeed;
        else if (executor->getPosition().x > currentTile->getCenterPosition().x)
            moveOffset.x = -executorSpeed;

        if (executor->getPosition().y < currentTile->getCenterPosition().y)
            moveOffset.y = executorSpeed / 2;
        else if (executor->getPosition().y > currentTile->getCenterPosition().y)
            moveOffset.y = -executorSpeed / 2;

        if (moveOffset != sf::Vector2f(0, 0))
            executor->move(moveOffset);

        if ((executor->getPosition().x < currentTile->getCenterPosition().x && executor->getPosition().x + executorSpeed > currentTile->getCenterPosition().x) ||
            (executor->getPosition().x > currentTile->getCenterPosition().x && executor->getPosition().x - executorSpeed < currentTile->getCenterPosition().x))
            executor->setPosition(sf::Vector2f(currentTile->getCenterPosition().x, executor->getPosition().y));

        if ((executor->getPosition().y < currentTile->getCenterPosition().y && executor->getPosition().y + executorSpeed > currentTile->getCenterPosition().y) ||
            (executor->getPosition().y > currentTile->getCenterPosition().y && executor->getPosition().y - executorSpeed < currentTile->getCenterPosition().y))
            executor->setPosition(sf::Vector2f(executor->getPosition().x, currentTile->getCenterPosition().y));

        MapTile* tileUnderFeet = executor->getLowestTile();
        if (tileUnderFeet != currentTile && !path.empty()) {
            //TODO: Smoothly correct worker's Y position
            if (executor->getMapHandle()->ifTileIsOnThePos(executor->getPosition(), currentTile, Map::SURFACE)) {
                executor->move(sf::Vector2f(0.f, tileUnderFeet->getHeight() - currentTile->getHeight()));

                executor->setTileUnderFeet(currentTile);
            }
        }

        //TODO: Slightly randomize workers' path
        if (executor->getPosition() == currentTile->getCenterPosition()) {
            path.erase(path.end() - 1);

            probability = 0;

            if (path.empty()) {
                status = FINISHED;
                return;
            }

            currentTile = path.back();
            if (currentTile == nullptr) {
                status = FINISHED;
                return;
            }

            if (currentTile->getObjectOnTile() != nullptr && currentTile->getObjectOnTile()->getMovingBlock())
                this->findNewPath(executor->getPosition(), newPosition);
        }
    }

    else if (pathfinder->getStatus() == Pathfinder::NoPath)
        status = BROKEN;

    else pathfinder->update();
}

void TaskGoTo::findNewPath(sf::Vector2f start, sf::Vector2f target) {
    delete pathfinder;
    unsigned int index = 0;

    Map *map = executor->getOwner()->getMapHandle();
    std::string uniqueId = executor->getName() + std::to_string(start.x) + std::to_string(start.y) +
                           std::to_string(randInt(1000)) + std::to_string(target.x) + std::to_string(target.y);

    pathfinder = new Pathfinder(uniqueId, *map);

    index = map->getTileIndex(start);
    pathfinder->setStart(*map->getTile(index));

    index = map->getTileIndex(target);
    pathfinder->setTarget(*map->getTile(index));
    pathfinder->setRange(10000);

    executor->setAnimation("idle");
    found = false;
}

sf::Vector2f TaskGoTo::findClosestObject(const std::string &id, int steps) {
    sf::Vector2i tiledPosition = Camera::getMapPosition(executor->getPosition());

    for (int step = 1; step < steps + 1; step++) {
        for (int y = tiledPosition.y - step; y < tiledPosition.y - step + (1 + step * 2); y++){
            MapTile *tile = this->checkObject(sf::Vector2i(tiledPosition.x - step, y), id);

            if (tile != nullptr)
                return tile->getCenterPosition();
        }

        for (int y = tiledPosition.y - step; y < tiledPosition.y - step + (1 + step * 2); y++){
            MapTile *tile = this->checkObject(sf::Vector2i(tiledPosition.x + step, y), id);

            if (tile != nullptr)
                return tile->getCenterPosition();
        }

        for (int x = tiledPosition.x - step; x < tiledPosition.x - step + (1 + step * 2); x++){
            MapTile *tile = this->checkObject(sf::Vector2i(x, tiledPosition.y - step), id);

            if (tile != nullptr)
                return tile->getCenterPosition();
        }

        for (int x = tiledPosition.x - step; x < tiledPosition.x - step + (1 + step * 2); x++){
            MapTile *tile = this->checkObject(sf::Vector2i(x, tiledPosition.y + step), id);

            if (tile != nullptr)
                return tile->getCenterPosition();
        }
    }

    status = BROKEN;
    return executor->getPosition();
}

MapTile *TaskGoTo::checkObject(sf::Vector2i tiledPosition, const std::string &id) {
    sf::Vector2f position = Camera::getIsoPosition(sf::Vector2i(tiledPosition.x, tiledPosition.y));
    MapTile* tile = executor->getOwner()->getMapHandle()->getTile(position, Map::RECT);

    if (tile != nullptr) {
        MapObject *object = tile->getObjectOnTile();

        if (object != nullptr && object->getType() == id && object->isMaxLevel())
            return tile;
    }

    return nullptr;
}

void TaskGoTo::reset() {
    path.clear();

    pathfinder = nullptr;
    currentTile = nullptr;

    executorSpeed = 0.0f;
    probability = 0;
    found = false;

    Task::reset();
}

void TaskGoTo::setPosition(sf::Vector2f newPosition) {
    this->targetId = "";
    this->newPosition = newPosition;
}

void TaskGoTo::setTargetId(const std::string &targetId) {
    this->targetId = targetId;
}

void TaskGoTo::setRange(unsigned int range) {
    this->range = range;
}
