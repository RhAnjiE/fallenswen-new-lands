#include "TaskWait.h"

#include "TaskGather.h"
#include "Human.h"
#include "../../../players/PlayerData.h"
#include "../../Map.h"

TaskWait::TaskWait(const std::string& id, Human* executor, float seconds, const std::string& animationId)
        : Task(id, executor, animationId) {

    this->timeToEnd = seconds;
}

void TaskWait::start() {
}

void TaskWait::executeTask() {
    Task::executeTask();

    timer += 1 * DeltaTime::getDt();

    if (timer >= timeToEnd) {
        status = FINISHED;
    }
}

void TaskWait::reset() {
    timer = 0;

    Task::reset();
}
