#ifndef IZOGAME_TASKWAIT_H
#define IZOGAME_TASKWAIT_H

#include "Task.h"
//#include "../../utilities/Camera.h"
#include "../../../utilities/Pathfinder.h"

class Human;
class TaskWait: public Task {
public:
    TaskWait(const std::string& id, Human* executor, float seconds, const std::string& animationId = "idle");

    void start() override;
    void executeTask() override;

private:
    float timer = 0;

    float timeToEnd = 0;

    void reset() override;
};

#endif //IZOGAME_TASKWAIT_H
