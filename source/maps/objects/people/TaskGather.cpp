#include "TaskGather.h"
#include "Human.h"
#include "../../../players/PlayerData.h"
#include "../../Map.h"

TaskGather::TaskGather(const std::string& id, Human* executor, const std::string& targetType, float seconds, bool remove, const std::string& animationId)
        : Task(id, executor, animationId) {

    this->targetType = targetType;
    this->howLong = seconds;
    this->remove = remove;
}

void TaskGather::registerForLua() {
    sol::state* lua = ResourceManager::getInstance().getLuaHandle();
    lua->new_usertype<TaskGather>(
            "TaskGather", sol::constructors<TaskGather(const std::string&, Human*, const std::string&, float, bool, const std::string&)>(),
            "target", sol::readonly_property(&TaskGather::getTarget),
            sol::base_classes, sol::bases<Task>()
            //TODO: Register all properties and methods
    );
}

void TaskGather::start() {
    Task::start();

    unsigned int index = executor->getOwner()->getMapHandle()->getTileIndex(executor->getPosition());
    target = executor->getOwner()->getMapHandle()->getTile(index)->getObjectOnTile();

    if (target == nullptr || target->getType() != targetType)
        status = BROKEN;

    //else executor->setAnimation(animationId);
}

void TaskGather::executeTask() {
    Task::executeTask();

    if (status != UNFINISHED)
        return;

    timer += 1 * DeltaTime::getDt();

    if (timer >= howLong * multiplier) {
        unsigned int index = executor->getOwner()->getMapHandle()->getTileIndex(executor->getPosition());
        target = executor->getOwner()->getMapHandle()->getTile(index)->getObjectOnTile();

        if (target != nullptr) {
            if (remove)
                executor->getOwner()->getMapHandle()->removeObject(target->getUniqueId());

            status = FINISHED;
        }

        else status = BROKEN;
    }
}

void TaskGather::reset() {
    timer = 0;
    target = nullptr;

    Task::reset();
}

MapObject *TaskGather::getTarget() {
    //TODO: Target nullptr if deleted code below
    unsigned int index = executor->getOwner()->getMapHandle()->getTileIndex(executor->getPosition());
    target = executor->getOwner()->getMapHandle()->getTile(index)->getObjectOnTile();

    return target;
}
