#include "Task.h"
#include "Human.h"

Task::Task(const std::string& id, Human* executor, const std::string& animationId) {
    this->id = id;
    this->executor = executor;
    this->animationId = animationId;
}

void Task::registerForLua() {
    sol::state* lua = ResourceManager::getInstance().getLuaHandle();
    lua->new_usertype<Task>(
            "Task", sol::constructors<Task(const std::string&, Human*, const std::string&)>(),
            "id", sol::readonly_property(&Task::getId),

            "setBehaviourIfSuccess", &Task::setBehaviourIfSuccess,
            "setBehaviourIfFail", &Task::setBehaviourIfFail
            //TODO: Register all properties and methods
    );
}

void Task::start() {
    this->reset();
}

void Task::executeTask() {
    if (finishTask())
        return;

    executor->setAnimation(animationId);
}

void Task::reset() {
    status = UNFINISHED;
    executedFinishCode = false;
}

std::string Task::getId() {
    return id;
}

Task::Status Task::getStatus() {
    return status;
}

void Task::setBehaviourIfSuccess(std::function<void()> ifSuccess) {
    this->ifSuccess = ifSuccess;
}

void Task::setBehaviourIfFail(std::function<void()> ifFail) {
    this->ifFail = ifFail;
}

bool Task::finishTask() {
    if (!executedFinishCode && status != UNFINISHED) {
        executedFinishCode = true;

        switch (status) {
            case FINISHED:
                this->ifSuccess();
                return true;

            case BROKEN:
                this->ifFail();
                executor->setAnimation("idle");
                return true;

            default:
                return false;
        }
    }

    else return false;
}

void Task::setMultiplier(float multiplier) {
    this->multiplier = multiplier;

    if (multiplier < 0.2)
        multiplier = 0.2;
}
