#ifndef IZOGAME_TASKGATHER_H
#define IZOGAME_TASKGATHER_H

#include "Task.h"
#include "../MapObject.h"

class Human;
class TaskGather: public Task {
public:
    TaskGather(const std::string& id, Human* executor, const std::string& targetType, float seconds, bool remove, const std::string& animationId = "gather");

    void start() override;
    void executeTask() override;

    MapObject* getTarget();

    static void registerForLua();

private:
    //TODO: Fix clock
    double timer = 0;
    double howLong = 0;
    bool remove = false;

    std::string targetType = "";
    MapObject* target = nullptr;

    void reset() override;
};

#endif //IZOGAME_TASKGATHER_H
