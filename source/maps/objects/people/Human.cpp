#include "Human.h"
#include "../../../players/PlayerData.h"
#include "../buildings/MapBuilding.h"
#include "../../../utilities/Camera.h"
#include "../../Map.h"

Human::Human(const std::string &type, const std::string &uniqueId, Map* mapHandle,
             MapBuilding* building, std::vector<MapTile*> &tilesUnder, PlayerData* owner)
    : MapObject(type, uniqueId, ResourceManager::getTexture(type), owner->getCityHall()->getCenterPosition(), mapHandle, tilesUnder, false) {
    this->loadLuaValues(ResourceManager::getTexture(type));

    this->animation = new Animation(ResourceManager::getTexture(type));
    this->animation->loadAnimationsFromLua(type);
    this->animation->addSprite(&sprite);

    sprite.setOrigin(sprite.getGlobalBounds().width / 2, sprite.getGlobalBounds().height / 2);

    this->building = building;
    this->owner = owner;

    //TODO: Better names generator
    std::string names[] = {"John", "James", "Rob", "Thomas", "Jack", "Steve", "Harry", "Jacob", "Charlie", "William", "Richard"};
    std::string surnames[] = {"Baker", "Huxley", "Wood", "Silver", "Brown", "O'Brien", "Evans", "Clayton", "Fawcett", "Eastoft", "Crawford"};

    this->name = names[randInt(10)] + " " + surnames[randInt(10)];
    //this->sprite.setColor(this->owner->getColor());

    this->positionOnTile = Camera::getMapPosition(sprite.getPosition());
    this->lastTiledPosition = Camera::getMapPosition(this->getPosition());

    this->setTileUnderFeet(tilesUnder[0]);
}

void Human::registerForLua() {
    sol::state* lua = ResourceManager::getInstance().getLuaHandle();
    lua->new_usertype<Human>(
            "Human", sol::constructors<>(),
            "name", sol::readonly_property(&Human::getName),
            "currentTask", sol::readonly_property(&Human::getCurrentTask),
            "isIdle", sol::readonly_property(&Human::isIdle),
            "isWaiting", sol::readonly_property(&Human::isWaiting),

            "defineTaskGoTo", &Human::defineTaskGoTo,
            "defineTaskGather", &Human::defineTaskGather,
            "changeTask", &Human::changeTask,
            "resetCurrentTask", &Human::resetCurrentTask,
            "finishCurrentTask", &Human::finishCurrentTask,
            "wait", &Human::wait

            //TODO: Register all properties and methods
    );
}

Human::~Human() {
    //TODO: Sometimes crash
    tilesUnder[0]->removeHuman(this);
    tilesUnder.clear();

    delete animation;
}

void Human::update() {
    if (timeToWait == 0) {
        if (currentTask != nullptr)
            currentTask->executeTask();

        else this->animation->set("idle");
    }

    else {
        timer += 1 * DeltaTime::getDt();

        if (timer >= timeToWait) {
            timer = 0;
            timeToWait = 0;
        }

        else this->animation->set("idle");
    }

    animation->play();
}

void Human::setAnimation(const std::string &id) {
    animation->set(id);
}

void Human::setTileUnderFeet(MapTile *tile) {
    if(tile != nullptr && tile != tilesUnder[0]) {
        tilesUnder[0]->removeHuman(this);
        tilesUnder.clear();
        tilesUnder.emplace_back(tile);
        tilesUnder[0]->addHuman(this);

        lastTiledPosition = mapHandle->getMapCoordsFromIndex(tile->getIndex());

        //tilesUnder[0]->setColor(randInt(50, 200), randInt(50, 200), randInt(50, 200));
    }
}

void Human::move(sf::Vector2f offset) {
    if ((offset.x < 0.0f && sprite.getScale().x == 1.0f) ||
        (offset.x > 0.0f && sprite.getScale().x == -1.0f))
        sprite.scale(-1.0f, 1.0f);

    sprite.move(offset);
}

sf::Vector2i Human::getTiledPosition() {
    return positionOnTile;
}

sf::Vector2f Human::getPosition() const {
    return sf::Vector2f(sprite.getPosition().x + sprite.getGlobalBounds().width / 2 - sprite.getOrigin().x, sprite.getPosition().y + sprite.getGlobalBounds().height - sprite.getOrigin().y);
}

void Human::setPosition(sf::Vector2f newPosition) {
    sprite.setPosition(newPosition.x - sprite.getGlobalBounds().width / 2 + sprite.getOrigin().x, newPosition.y - sprite.getGlobalBounds().height + sprite.getOrigin().y);
    positionOnTile = Camera::getMapPosition(newPosition);
}

std::string Human::getName() {
    return name;
}

float Human::getSpeed() {
    return speed;
}

PlayerData *Human::getOwner() {
    return owner;
}

bool Human::isIdle() {
    return currentTask == nullptr;
}

std::shared_ptr<Task> Human::getCurrentTask() {
    return currentTask;
}

void Human::defineTaskGather(const std::string &id, TaskGather& task) {
    if (definedTasks.find(id) != definedTasks.end())
        Logger::debug("Found another task used id '" + id + "'! Replaced...");

    //TODO: Problem with inheritance
    definedTasks[id] = std::make_shared<TaskGather>(task);

    //std::shared_ptr<Task>test(task);
    //definedTasks[id].reset(task);
}

void Human::defineTaskGoTo(const std::string &id, TaskGoTo &task) {
    if (definedTasks.find(id) != definedTasks.end())
        Logger::debug("Found another task used id '" + id + "'! Replaced...");

    definedTasks[id] = std::make_shared<TaskGoTo>(task);
}

void Human::changeTask(const std::string &id) {
    std::shared_ptr<Task> task = this->getTask(id);

    if (task != nullptr) {
        currentTask = task;
        currentTask->start();
    }

    else currentTask = nullptr;
}

void Human::resetCurrentTask() {
    if (currentTask != nullptr)
        currentTask->start();
}

void Human::finishCurrentTask() {
    currentTask = nullptr;
}

std::shared_ptr<Task> Human::getTask(const std::string &id) {
    if (definedTasks.find(id) == definedTasks.end()) {
        Logger::warning("Task with id '" + id + "' not found!");

        return std::shared_ptr<Task>(nullptr);
    }

    return definedTasks[id];
}

void Human::wait(float seconds) {
    timeToWait = seconds;
}

void Human::loadLuaValues(const ExTexture &texture) {
    sol::state* lua = ResourceManager::getInstance().getLuaHandle();
    if ((*lua)[type] == sol::nil) {
        Logger::warning("Human's data from lua not found!");

        return;
    }

    sol::table data = (*lua)[type];

    if (data["health"] != sol::nil)
        health = data["health"];

    if (data["speed"] != sol::nil)
        speed = data["speed"];
}

bool Human::isWaiting() {
    return (timeToWait != 0);
}

std::map<std::string, std::shared_ptr<Task>>& Human::getTasks() {
    return definedTasks;
}
