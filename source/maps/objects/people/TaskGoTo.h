#ifndef IZOGAME_TASKGOTO_H
#define IZOGAME_TASKGOTO_H

#include "Task.h"
//#include "../../utilities/Camera.h"
#include "../../../utilities/Pathfinder.h"

class Human;
class TaskGoTo: public Task {
public:
    TaskGoTo(const std::string& id, Human* executor, sf::Vector2f newPosition, const std::string& animationId = "walk");
    TaskGoTo(const std::string& id, Human* executor, const std::string& targetId, unsigned int range, const std::string& animationId = "walk");

    void start() override;
    void executeTask() override;

    static void registerForLua();

    void setPosition(sf::Vector2f newPosition);
    void setTargetId(const std::string& targetId);
    void setRange(unsigned int range);

private:
    std::string targetId = "";
    sf::Vector2f newPosition;
    float executorSpeed = 0.0f;

    std::vector<MapTile*> path;
    Pathfinder *pathfinder = nullptr;
    MapTile *currentTile = nullptr;

    float probability = 0;
    bool found = false;
    int range = 1;

    sf::Vector2f findClosestObject(const std::string& id, int steps);
    void findNewPath(sf::Vector2f start, sf::Vector2f target);

    MapTile *checkObject(sf::Vector2i tiledPosition, const std::string &id);

    void reset() override;
};

#endif //IZOGAME_TASKGOTO_H
