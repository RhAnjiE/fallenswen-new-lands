#ifndef IZOGAME_MAPRENDEREDOBJECT_H
#define IZOGAME_MAPRENDEREDOBJECT_H

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include "../utilities/graphics/ExTexture.h"
#include "../utilities/graphics/ManySprites.h"
#include "../utilities/graphics/ResourceManager.h"

class Map;
class MapRenderedObject: public sf::Drawable {
public:
    MapRenderedObject(const std::string &type, const ExTexture &texture, sf::Vector2f position, Map* mapHandle);
    virtual ~MapRenderedObject();

    static void registerForLua();

    sf::Sprite &getSprite();
    //void setSprite(const sf::Texture &texture);

    std::string getType() const;

    //virtual because humans' position is centered on his feet
    virtual sf::Vector2f getPosition() const;
    virtual void setPosition(sf::Vector2f newPosition);

    virtual sf::Vector2f getCenterPosition() const;
    sf::Vector2i getSize() const;

    virtual void update(){};

    void setColor(int r, int g, int b);
    void saveCurrentColor();
    void resetColor();

    int getRedColor();
    int getGreenColor();
    int getBlueColor();

    Map* getMapHandle();

protected:
    std::string type = "";
    Map* mapHandle = nullptr;

    sf::Sprite sprite;
    sf::Color normalColor = sf::Color::White;
    sf::Vector2f positionDiff = sf::Vector2f(0, 0);

    virtual void loadLuaValues(const ExTexture &texture);
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};

#endif //IZOGAME_MAPRENDEREDOBJECT_H
