#include "Shadow.h"

Shadow::Shadow(sf::Vector2f position, Map* mapHandle, float scale)
    : MapRenderedObject("shadow", ResourceManager::getTexture("shadow"), position, mapHandle) {
    sprite.setOrigin(7,3);

    if (scale != 0)
        sprite.setScale(scale, scale);

    sprite.setColor(sf::Color(255,255,255,140));
}
