#include "Map.h"
#include "../gui/hud/Hud.h"

/**
 * Loading map assets
 */

Map::Map(Size size, Type type, ResourcesNum resourcesNum, PlayerData* player, Hud* hud)
    : mapInteraction(this)
    , player(player)
    , hud(hud) {

    this->setOptions(size, type, resourcesNum);
}

void Map::setOptions(Size size, Type type, ResourcesNum resourcesNum) {
    this->size = size;                      //SMALL, MEDIUM, BIG, HUGE
    this->type = type;                      //CONTINENTS, LAKES, ISLANDS, ARCHIPELAGO
    this->resourcesNum = resourcesNum;  //LITTLE, MEDIUM, HIGH

    if      (this->size == SMALL)   sizeX = sizeY = 35;
    else if (this->size == MEDIUM)  sizeX = sizeY = 80;
    else if (this->size == BIG)     sizeX = sizeY = 150;
    else if (this->size == HUGE)    sizeX = sizeY = 300;

    /** [RESET ZONE] **/
    chosenTiles.clear();
    chosenObject = nullptr;

    mapInteraction.setChosenObject("");
    player->reset();

    this->generate();
}

/**
 * Updating the map
 * @param view
 */
void Map::update(sf::Vector2f mousePosition, sf::View &view) {
    hud->setStatement("");
    mapInteraction.selectTileAndObject(this, mousePosition);

    //TODO: Improve performance
    firstIndex = this->getTileIndex(
            sf::Vector2f(view.getCenter().x + view.getSize().x / 2,
                         view.getCenter().y - view.getSize().y / 2));
    lastIndex  = this->getTileIndex(
            sf::Vector2f(view.getCenter().x - view.getSize().x / 2 + TILE_WIDTH,
                         view.getCenter().y + view.getSize().y / 2 + TILE_HEIGHT));

    for (int i = firstIndex; i < lastIndex; i++) {
        if (this->isObjectVisible(tiles[i].getPosition())) {
            tiles[i].update();

            /*MapObject* object = tiles[i].getObjectOnTile();
            if (object!= nullptr && object->getLowestTile() == &tiles[i])
                object->update();

            for (auto& [id, human] : tiles[i].humans)
                window.draw(*human);
            */
        }
    }

    for (auto& [id, object] : objects)
        object->update();

    // we can store pointer to sf::View inside the Map class instead of this variables
    viewableArea = view.getCenter();
    viewableAreaSize = view.getSize();
}

/**
 * Displaying the map
 * @param sf::RenderWindow window
 */
void Map::render(sf::RenderWindow &window) {
    for (int i = firstIndex; i <= lastIndex; i++) {
        if (this->isObjectVisible(tiles[i].getPosition())) {
            window.draw(tiles[i]);

            for (auto& human : tiles[i].getHumans())
                window.draw(*human);

            MapObject* object = tiles[i].getObjectOnTile();
            if (object!= nullptr && object->getLowestTile() == &tiles[i])
                window.draw(*object);
        }
    }

    window.draw(mapInteraction.getChosenObject());
}

/**
 * Check if object is visible
 * @param sf::Vector2i objPos
 * @return true|false
 */
bool Map::isObjectVisible(sf::Vector2f objPos, sf::Vector2i objSize) {
    return (objPos.x >= viewableArea.x - viewableAreaSize.x/2 - objSize.x - 100 &&
            objPos.x <= viewableArea.x + viewableAreaSize.x/2 + 100 &&
            objPos.y >= viewableArea.y - viewableAreaSize.y/2 - objSize.y - 100 &&
            objPos.y <= viewableArea.y + viewableAreaSize.y/2 + 100);
}

/**
 * Choice the map object type from the given arguments
 * @param const std::string& typeId
 * @param const std::string& uniqueId
 * @param sf::Vector2f position
 * @param std::vector<MapTile*> &tilesUnder
 * @param PlayerData* owner
 */
void Map::choiceGoodObjectType(const std::string& typeId, const std::string& uniqueId, sf::Vector2f position, std::vector<MapTile*> &tilesUnder, PlayerData* owner) {
    sol::state& lua = *ResourceManager::getInstance().getLuaHandle();

    if (lua[typeId] != sol::nil) {
        sol::table data = lua[typeId];

        if (owner != nullptr) {
            if (data.get<std::string>("objectType") == "building") {
                objects.insert(std::make_pair(uniqueId, new MapBuilding(typeId, uniqueId,
                        ResourceManager::getTexture(typeId), position, this, tilesUnder, owner)));

                return;
            }
        }

        if (data.get<std::string>("objectType") == "plant") {
            objects.insert(std::make_pair(uniqueId, new MapGrowablePlant(typeId, uniqueId,
                    ResourceManager::getTexture(typeId), position, this, tilesUnder)));

            objects[uniqueId]->init();

            return;
        }

        bool isSynchronized = false;
        if (data.get<std::string>("textureType") == "animation") {
            if (data["synchronizedAnim"] != sol::nil)
                isSynchronized = data["synchronizedAnim"];
        }

        objects.insert(std::make_pair(uniqueId, new MapObject(typeId, uniqueId,
                ResourceManager::getTexture(typeId), position, this, tilesUnder, true, isSynchronized)));

        objects[uniqueId]->init();
    }
}

/**
 * Method to handle player's action on the map
 */
void Map::interact(PlayerData& owner) {
    hud->setTargetInfo("", "");

    std::string name = "";
    MapObject* object = nullptr;

    if (mapInteraction.getChosenID().empty()) {
        if (chosenObject != nullptr) {
            auto building = dynamic_cast<MapBuilding*>(chosenObject);
            if ( building != nullptr) {
                 hud->setTargetInfo(building->getName(), building->getFullDesc(), building);
            }

            else hud->setTargetInfo(chosenObject->getType(), "");
        }

        else if(chosenTiles[0] != nullptr)
            hud->setTargetInfo(chosenTiles[0]->getType(), "");
    }

    //TODO: Improve deleting objects
    else if (mapInteraction.getChosenID() == "delete") {
        if (chosenObject != nullptr && chosenObject->getType() != "cityHall") {
            for (auto tileUnder : chosenObject->getTilesUnder()) {
                if (tileUnder != nullptr) {
                    auto tilePosition = getMapCoordsFromIndex(tileUnder->getIndex());

                    hud->getMinimap().clearPixel(tilePosition.x, tilePosition.y, tileUnder);
                }
            }

            auto building = dynamic_cast<MapBuilding*>(chosenObject);
            if ( building != nullptr)
                 owner.deleteBuilding(building);

            this->removeSelectedObject();
        }
    }

    else if ((object = mapInteraction.buildObject(owner)) != nullptr){
        auto building = dynamic_cast<MapBuilding*>(object);

        for (auto tileUnder : chosenTiles) {
            if (tileUnder != nullptr) {
                auto tilePosition = getMapCoordsFromIndex(tileUnder->getIndex());

                if (building != nullptr)
                     hud->getMinimap().setPixel(tilePosition.x, tilePosition.y, owner.getColor());
                else hud->getMinimap().setPixel(tilePosition.x, tilePosition.y, sf::Color::Green);
            }
        }

        if(building != nullptr) {
            if(mapInteraction.getChosenID() == "cityHall" && owner.getCityHall() == nullptr) {
                mapInteraction.setChosenObject("");
            }

            owner.registerBuilding(building);
        }
    }
}