#include <cmath>
#include "Map.h"
#include "../utilities/Noise.h"

/**
 * Generating tiles on the map
 */
void Map::generate() {
    tiles.clear();

    std::string id;
    sf::Vector2f position;

    sol::table rawColor;
    sol::table rawColorVar;
    ExColor mainColor;
    ExColor colorScatter;
    Noise noise;

    float step = 50.f;
    float height = 0.f;
    float minHeight = 0.f;

    std::vector<MapTile> tempTiles;

    switch(type) {
        case Map::CONTINENTS:
            noise.setFrequency(0.7f);
            noise.setOctaves(15);
            noise.setPersistence(0.5);
            step = 30.f;
            break;
        case Map::LAKES:
            noise.setFrequency(1.0f);
            noise.setOctaves(15);
            noise.setPersistence(0.5);
            step = 20.f;
            break;

        case Map::ISLANDS:
            noise.setFrequency(0.9f);
            noise.setOctaves(15);
            noise.setPersistence(0.5);
            step = 40.f;
            break;

        case Map::DESERT:
            noise.setFrequency(1);
            noise.setOctaves(15);
            noise.setPersistence(0.5);
            step = 30.f;
            break;
    }

    for (int y = 0; y < sizeY; y++) {
        for (int x = 0; x < sizeX; x++) {
            height = noise.getValue(x / step, y / step);

            switch(type) {
                case Map::CONTINENTS:
                    minHeight = 0.45f;

                    if      (height <= minHeight)   id = "water";
                    else if (height <= 0.55f)       id = "sand";
                    else if (height <= 0.60f)       id = "dirt";
                    else                            id = "grass";

                    break;

                case Map::LAKES:
                    minHeight = 0.35f;

                    if      (height <= minHeight)   id = "water";
                    else if (height <= 0.45f)       id = "sand";
                    else if (height <= 0.50f)       id = "dirt";
                    else                            id = "grass";

                    break;

                case Map::ISLANDS:
                    minHeight = 0.55f;

                    if      (height <= minHeight)   id = "water";
                    else if (height <= 0.65f)       id = "sand";
                    else if (height <= 0.67f)       id = "dirt";
                    else                            id = "grass";

                    break;

                case Map::DESERT:
                    minHeight = 0.25f;

                    if      (height <= minHeight)   id = "water";
                    else if (height <= 0.32f)       id = "grass";
                    else if (height <= 0.37f)       id = "dirt";
                    else                            id = "sand";

                    break;
            }

            position = Camera::getIsoPosition(sf::Vector2i(x,y));
            mainColor.setColor(sf::Color::Black);
            colorScatter.setColor(sf::Color::Black);

            sol::state& lua = *ResourceManager::getInstance().getLuaHandle();
            if (lua[id] != sol::nil) {
                sol::table data = lua[id];

                if (data["colorBase"] != sol::nil) {
                    rawColor = data["colorBase"];

                    if (rawColor != sol::nil && rawColor.size() == 3) {
                        mainColor.r = rawColor[1];
                        mainColor.g = rawColor[2];
                        mainColor.b = rawColor[3];
                    }
                }
            }

            float newHeight = 0.f;
            if (id != "water" && height >= minHeight) {
                newHeight = ((height - minHeight) * 80) + randInt(-2, 2);
                mainColor.r += 50 - (int)newHeight;
            }

            else if (id == "water") {
                mainColor.r -= 100 - (height * 220) + randInt(-2, 2);
                mainColor.g -= 100 - (height * 220) + randInt(-2, 2);
                mainColor.b -= 100 - (height * 220) + randInt(-2, 2);
            }

            bool isSynchronized = false;
            if (lua[id] != sol::nil) {
                sol::table data = lua[id];

                if (data["colorVariation"] != sol::nil) {
                    rawColorVar = data["colorVariation"];

                    if (rawColorVar != sol::nil && rawColorVar.size() == 3) {
                        colorScatter.r = rawColorVar[1];
                        colorScatter.g = rawColorVar[2];
                        colorScatter.b = rawColorVar[3];
                    }
                }

                if (data["textureType"] != sol::nil && data.get<std::string>("textureType") == "animation") {
                    if (data["synchronizedAnim"] != sol::nil)
                        isSynchronized = data["synchronizedAnim"];
                }
            }

            tiles.emplace_back(MapTile((int)tiles.size(), id, ResourceManager::getTexture(id),
                    position, this, mainColor.getColor(), colorScatter.getColor(), isSynchronized, newHeight));
        }
    }

    this->generatePlants();
}

//TODO: Refactor this method
void Map::generatePlants() {
    for (auto& object : objects)
        delete object.second;

    objects.clear();

    std::string id;
    std::string type;
    sf::Vector2f position;

    unsigned short random;
    unsigned short probability[3];

    int index = 0;

    std::vector<MapTile*>tileVector;
    MapTile* tile;

    for (int y = 0; y < sizeY; y++) {
        for (int x = 0; x < sizeX; x++) {
            position = Camera::getIsoPosition(sf::Vector2i(x+1, y));

            tile = &tiles[index++];
            if (tile->getBuildBlock())
                continue;

            type = tile->getType();
            position.y -= tile->getHeight();

            /** Flowers and details **/
            id = "";
            random = (unsigned short) randInt(100);

            if (type == "grass" && random <= 5)
                id = "flowers" + std::to_string(randInt(1,6));

            if (!id.empty()) {
                std::string uniqueId = id + std::to_string(objectIndex++);
                sol::state& lua = *ResourceManager::getInstance().getLuaHandle();

                bool isSynchronized = false;
                if (lua[id] != sol::nil) {
                    sol::table data = lua[id];

                    if (data["textureType"] != sol::nil && data["textureType"] == "animation") {
                        if (data["synchronizedAnim"] != sol::nil)
                            isSynchronized = data["synchronizedAnim"];
                    }
                }

                tileVector.clear();
                tileVector.emplace_back(tile);
                objects.insert(std::make_pair(uniqueId, new MapObject(id, uniqueId,
                        ResourceManager::getTexture(id), position, this, tileVector, true, isSynchronized)));

                objects[uniqueId]->init();
            }

            /** Objects and plants **/
            id = "";

            switch (resourcesNum) {
                case LITTLE: {
                    if (type == "grass") {
                        probability[0] = 5;
                        probability[1] = 2;
                        probability[2] = 2;
                    } else if (type == "dirt") {
                        probability[0] = 0;
                        probability[1] = 5;
                        probability[2] = 5;
                    } else if (type == "sand") {
                        probability[0] = 0;
                        probability[1] = 0;
                        probability[2] = 0;
                    }

                    break;
                }

                case NORMAL: {
                    if (type == "grass") {
                        probability[0] = 20;
                        probability[1] = 5;
                        probability[2] = 5;
                    } else if (type == "dirt") {
                        probability[0] = 0;
                        probability[1] = 10;
                        probability[2] = 2;
                    } else if (type == "sand") {
                        probability[0] = 0;
                        probability[1] = 0;
                        probability[2] = 0;
                    }

                    break;
                }

                case RICH: {
                    if (type == "grass") {
                        probability[0] = 40;
                        probability[1] = 10;
                        probability[2] = 10;
                    } else if (type == "dirt") {
                        probability[0] = 0;
                        probability[1] = 20;
                        probability[2] = 5;
                    } else if (type == "sand") {
                        probability[0] = 0;
                        probability[1] = 0;
                        probability[2] = 0;
                    }

                    break;
                }
            }

            random = (unsigned short) randInt(100);
            if (random < probability[0])
                id = "tree";

            else if (random < probability[0] + probability[1])
                id = (randInt(100) <= 50) ? "bush" : "fruitBush";

            else if (random <= probability[0] + probability[1] + probability[2])
                id = (randInt(100) <= 50) ? "smallBush" : "fruitSmallBush";

            if (this->type == Map::DESERT) {
                if (type == "sand" && randInt(100) < 5)
                    id = "cactus";
            }

            if (!id.empty()) {
                std::string uniqueId = id + std::to_string(objectIndex++);

                tileVector.clear();
                tileVector.emplace_back(tile);
                this->choiceGoodObjectType(id, uniqueId, position, tileVector, nullptr);
            }
        }
    }
}

