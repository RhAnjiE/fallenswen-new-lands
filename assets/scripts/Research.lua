research = {
    stone_tools = {
        textureName = "research_1",
        name = "Stone Tools",
        description = "Better workers' performance",
        type = "BOTH",

        effect = function(playerData)
            print("\"" .. research.stone_tools.name .. "\" activated!")

            playerData:updateEfficiency("woodcamp", 10)
        end
    },

    cultivation = {
        textureName = "research_2",
        name = "Cultivation",
        description = "New building - farms",
        type = "SUMMER",

        parents = {"stone_tools"},
        effect = function(playerData)
            print("\"Cultivation\" activated!")

            playerData:addNewBuilding("farm")
            playerData:addNewBuilding("farmland")
        end
    },

    fertilizer = {
        textureName = "research_2",
        name = "Fertilizer",
        description = "Farms are 20% more efficient",
        type = "SUMMER",

        parents = {"cultivation"},
        effect = function(playerData)
            print("\"Fertilizer\" activated!")

            playerData:updateEfficiency("farm", 20)
        end
    },

    fire = {
        textureName = "research_3",
        name = "Fire",
        description = "Survive the winter",
        type = "WINTER",

        parents = {"stone_tools"},
        effect = function(playerData)
            print("\"Fire\" activated!")

            playerData:modifyFoodConsumingInWinter(80)
        end
    },

    storing_up = {
        textureName = "research_3",
        name = "Storing Up",
        description = "Reduce the speed of consuming food",
        type = "WINTER",

        parents = {"fire"},
        effect = function(playerData)
            print("\"Storing Up\" activated!")

            playerData:modifyFoodConsuming(10)
            playerData:addNewBuilding("stockpile")
        end
    },

    antiquity = {
        textureName = "research_1",
        name = "Antiquity",
        description = "New age - new buildings appearance",
        type = "BOTH",

        parents = {"storing_up", "fertilizer"},
        effect = function(playerData)
            print("\"Antiquity\" activated!")

            playerData:levelUpBuildings("")
        end
    },

    currency = {
        textureName = "research_1",
        name = "Currency",
        description = "Discover money and the way of trading using currency",
        type = "BOTH",

        parents = {"antiquity"},
        effect = function(playerData)
            print("\"Currency\" activated!")

            -- playerData.addMaterial("money")
            playerData:addMoney(100)
        end
    }
}