house = {
    textureType = "multi",
    rows = 1,
    maxColumns = 2,
    objectType = "building",
    iconTexture = "icon_house",
    positionDiffY = 14,
    buildBlock = true,
    movingBlock = true,
    tileWidth = 1,
    tileHeight = 1,

    name = "House",
    description = "House for your people. Can accommodate five men.",
    moneyCost = 0,
    woodCost = 5,
    stoneCost = 2,

    init = function(building, owner)
        owner:addMaxPeople(5)
    end,

    update = function(building, worker, owner)
    end,

    drawUi = function(building, worker, owner, window, layout)
    end,

    onDestroy = function(object, owner)
        owner:addMaxPeople(-5)
    end
}