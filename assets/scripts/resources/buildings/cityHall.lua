cityHall = {
    textureType = "single",
    objectType = "building",
    iconTexture = "icon_cityhall",
    positionDiffY = 5,
    buildBlock = true,
    movingBlock = true,
    tileWidth = 2,
    tileHeight = 2,

    name = "City Hall",
    description = "A heart of the entire city.",
    moneyCost = 0,
    woodCost = 0,
    stoneCost = 0,

    foodClock = Clock.new(),
    peopleClock = Clock.new(),

    init = function(building, owner)
        owner:addMaxPeople(5)
        owner:addBasicBuildings()
    end,

    update = function(building, worker, owner)
        local multiplier = owner.foodMultiplier

        if owner.people > 0 then
            if cityHall.foodClock:getElapsedTime():asSeconds() >= 5 * multiplier * (1.0 / owner.people) then
                if owner:getResource("food") > 0 then
                    owner:addResource("food", -1)

                else
                    --TODO: Game over
                end

                cityHall.foodClock:restart()
            end
        end

        if cityHall.peopleClock:getElapsedTime():asSeconds() >= 1 then
            if owner.people < owner.maxPeople then
                owner:addPeople(1)
            end

            cityHall.peopleClock:restart()
        end
    end,

    drawUi = function(building, worker, owner, window, layout)
        local size = Vector2f.new(150.0, 25.0)

        layout.at.x = UIWindowGetCenter(window, size, layout.at.y).left

        if UIButton(window, size, layout, "Add Money", nil, nil) == 2 then
            owner:addMoney(100)
        end
    end,

    onDestroy = function(object, owner)
        print("\"onDestroy\" activated!")

        -- GAME END
    end
}
