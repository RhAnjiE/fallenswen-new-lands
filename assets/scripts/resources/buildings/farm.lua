farm = {
    textureType = "single",
    objectType = "building",
    iconTexture = "icon_farm",
    positionDiffY = 14,
    buildBlock = true,
    movingBlock = true,
    tileWidth = 1,
    tileHeight = 1,

    name = "Farm",
    description = "A wheat farm.",

    hp = 1000,
    moneyCost = 0,
    woodCost = 3,
    stoneCost = 0,
    buildTime = 0,
    workerType = "farmer",

    init = function(building, owner)
        local worker = building:createWorker()

        local backToWork = TaskGoTo.new("Returning to the work", worker, building.entranceTile.centerPosition, "walk")
        backToWork:setBehaviourIfSuccess(function()
            worker:changeTask("findWheat")
        end )

        backToWork:setBehaviourIfFail(function()
            worker:wait(2)
            worker:resetCurrentTask()
        end )

        local findWheat = TaskGoTo.new("Finding the ripe wheat", worker, "farmland", 2, "walk")
        findWheat:setBehaviourIfSuccess(function()
            worker:changeTask("gatherWheat")
        end )

        findWheat:setBehaviourIfFail(function()
            worker:wait(2)
            worker:resetCurrentTask()
        end )

        local gatherWheat = TaskGather.new("Gathering", worker, "farmland", 2, false, "gather")
        gatherWheat:setBehaviourIfSuccess(function()
            local target = gatherWheat.target;

            if target ~= nil then
                target.currentLevel = 1;
            end

            worker:changeTask("bringWheat")
        end )

        gatherWheat:setBehaviourIfFail(function()
            worker:wait(2)
            worker:changeTask("backToWork")
        end )

        local bringWheat = TaskGoTo.new("Carrying the wheat", worker, building.entranceTile.centerPosition, "walkWithStuff")
        bringWheat:setBehaviourIfSuccess(function()
            building.occupiedSpace = building.occupiedSpace + 2
            worker:finishCurrentTask()
        end )

        bringWheat:setBehaviourIfFail(function()
            worker:wait(2)
            worker:resetCurrentTask()
        end )

        local stockpilePosition = owner.cityHall.entranceTile.centerPosition
        local backToStockpile = TaskGoTo.new("Taking wheat to the stockpile", worker, stockpilePosition, "walkWithStuff")
        backToStockpile:setBehaviourIfSuccess(function()
            owner:addResource("food", building.occupiedSpace)
            worker:finishCurrentTask()
        end )

        backToStockpile:setBehaviourIfFail(function()
            worker:wait(2)
            worker:resetCurrentTask()
        end )

        worker:defineTaskGoTo("backToWork", backToWork)
        worker:defineTaskGoTo("findWheat", findWheat)
        worker:defineTaskGather("gatherWheat", gatherWheat)
        worker:defineTaskGoTo("bringWheat", bringWheat)
        worker:defineTaskGoTo("backToStockpile", backToStockpile)

        worker:changeTask("backToWork")
    end,

    update = function(building, worker, owner)
        if building.worker ~= nil and building.worker.isIdle then
            if building.occupiedSpace >= building.capacity then
                building.worker:changeTask("backToStockpile")
                building.occupiedSpace = 0
            else
                building.worker:changeTask("backToWork")
            end
        end

        --[[
        Map* mapHandle = worker->getOwner()->getMapHandle();
        sf::Vector2i tiledPos = Camera::getMapPosition(this->tilesUnder[0]->getPosition());
        tiledPos.x += 1; //TODO: Fix that

        std::vector<sf::Vector2i>neighboringTilePositions;
        neighboringTilePositions = {sf::Vector2i(-1, 1), sf::Vector2i(0, 1), sf::Vector2i(1, 1),
                                    sf::Vector2i(-1, 0), sf::Vector2i(1, 0),
                                    sf::Vector2i(-1, -1), sf::Vector2i(0, -1), sf::Vector2i(1, -1)};

        for (auto &tilePositionOffset : neighboringTilePositions) {
            MapTile *tile = mapHandle->getTile(Camera::getIsoPosition(tiledPos + tilePositionOffset));

            if (tile != nullptr && !tile->getBuildBlock() &&
               (tile->getObjectOnTile() == nullptr || !tile->getObjectOnTile()->getBuildBlock())) {
                if (tile->getObjectOnTile() != nullptr && tile->getObjectOnTile()->getType() == "farmland")
                    wheatFields.emplace_back(tile->getObjectOnTile());

                else wheatFields.emplace_back(mapHandle->setObjectOnPosition("farmland", *owner,
                        Camera::getIsoPosition(tiledPos + tilePositionOffset)));

                //TODO: Register old tile color
                tile->getSprite().setColor(sf::Color(114+randInt(-5,5),61+randInt(-3,7),30+randInt(-10,7)));
            }
        }
        ]]
    end,

    drawUi = function(building, worker, owner, window, layout)
        UILabel(window, layout, "Collected wheat: " .. tostring(building.occupiedSpace) .. "/" .. tostring(building.capacity), 1)

        local size = Vector2f.new(200.0, 25.0)
        layout.at.x = UIWindowGetCenter(window, size, layout.at.y).left;

        if building.worker ~= nil then
            local newLayout = UIInitLayout(Vector2f.new(layout.at.x, layout.at.y + 5), 0)

            UIImage(window, newLayout, Vector2f.new(20, 20), getTexture("farmer"), Color.new(255, 255, 255))
            UILabel(window, newLayout, "Worker: " .. worker.name, 0)

            layout.at.y = newLayout.at.y + 20;

            if building.worker.isWaiting then
                UILabel(window, layout, "Current task: Idle (Lack of ripe wheat)", 1)
            else
                UILabel(window, layout, "Current task: " .. worker.currentTask.id, 1)
            end
        end

        layout.at.y = layout.at.y + 5

        if UIButton(window, size, layout, "Add 50 food", nil, nil) == 2 then
            owner:addResource("food", 50)
        end
    end,

    onDestroy = function(object, owner)
    end
}