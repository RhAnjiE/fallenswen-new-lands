woodCamp = {
    textureType = "single",
    objectType = "building",
    iconTexture = "icon_woodcamp",
    positionDiffX = -32,
    positionDiffY = 0,
    buildBlock = true,
    movingBlock = true,
    tileWidth = 2,
    tileHeight = 1,

    name = "Wood Camp",
    description = "A woodcutter's camp.\n",
    moneyCost = 0,
    woodCost = 3,
    stoneCost = 0,
    workerType = "woodcutter",

    init = function(building, owner)
        local worker = building:createWorker()

        pushMessage(owner.messageLog, "%s gathered %i wood!", worker.name, 123)
        --owner.messageLog.pushMessage("%s gathered %i wood!", worker.name, woodAmount)

        local backToWork = TaskGoTo.new("Returning to the work", worker, building.entranceTile.centerPosition, "walk")
        backToWork:setBehaviourIfSuccess(function()
            worker:changeTask("findTree")
        end )

        backToWork:setBehaviourIfFail(function()
            worker:wait(2)
            worker:resetCurrentTask()
        end )

        local findTree = TaskGoTo.new("Finding the trees", worker, "tree", 5, "walk")
        findTree:setBehaviourIfSuccess(function()
            worker:changeTask("cutTree")
        end )

        findTree:setBehaviourIfFail(function()
            worker:wait(3)
            worker:resetCurrentTask()
        end )

        local cutTree = TaskGather.new("Cutting", worker, "tree", 3.0, true, "gather")
        cutTree:setBehaviourIfSuccess(function()
            worker:changeTask("bringWood")
        end )

        cutTree:setBehaviourIfFail(function()
            worker:wait(2)
            worker:changeTask("backToWork")
        end )

        local bringWood = TaskGoTo.new("Carrying the wood logs", worker, building.entranceTile.centerPosition, "walkWithStuff")
        bringWood:setBehaviourIfSuccess(function()
            worker:wait(2)
            worker:changeTask("bringPlanks")
        end )

        bringWood:setBehaviourIfFail(function()
            worker:wait(2)
            worker:resetCurrentTask()
        end )

        local bringPlanks = TaskGoTo.new("Taking wood planks to the stockpile", worker, owner.cityHall.entranceTile.centerPosition, "walkWithStuff")
        bringPlanks:setBehaviourIfSuccess(function()
            local woodAmount = math.random(5, 10)
            owner:addResource("wood", woodAmount)

            pushMessage(owner.messageLog, "%s gathered %i wood!", worker.name, 123)
            worker:finishCurrentTask()
        end )

        bringPlanks:setBehaviourIfFail(function()
        worker:wait(2)
        worker:resetCurrentTask()
        end )

        bringPlanksFail = function()
            owner:addResource("wood", 10)
            worker:finishCurrentTask()
        end

        bringPlanks:setBehaviourIfSuccess(bringPlanksFail)

        worker:defineTaskGoTo("backToWork", backToWork)
        worker:defineTaskGoTo("findTree", findTree)
        worker:defineTaskGather("cutTree", cutTree)
        worker:defineTaskGoTo("bringWood", bringWood)
        worker:defineTaskGoTo("bringPlanks", bringPlanks)
        worker:changeTask("backToWork")
        end,

    update = function(building, worker, owner)
        if worker ~= nil and worker.isIdle then
            worker:changeTask("backToWork")
        end
    end,

    drawUi = function(building, worker, owner, window, layout)
        local size = Vector2f.new(200.0, 25.0)
        layout.at.x = UIWindowGetCenter(window, size, layout.at.y).left;

        if worker ~= nil then
            local newLayout = UIInitLayout(Vector2f.new(layout.at.x, layout.at.y + 5), 0)

            UIImage(window, newLayout, Vector2f.new(20, 20), getTexture("woodcutter"), Color.new(255, 255, 255))
            UILabel(window, newLayout, "Worker: " .. worker.name, 0)

            layout.at.y = newLayout.at.y + 20;

            if worker.isWaiting then
                UILabel(window, layout, "Current task: Idle (No trees nearby)", 1)
            else
                UILabel(window, layout, "Current task: " .. worker.currentTask.id, 1)
            end
        end

        layout.at.y = layout.at.y + 5

        if UIButton(window, size, layout, "Add 50 stone", nil, nil) == 2 then
            owner:addResource("stone", 50)
        end
    end,

    onDestroy = function(object, owner)
    end
}