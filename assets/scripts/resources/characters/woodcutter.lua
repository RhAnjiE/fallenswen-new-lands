woodcutter = {
    textureType = "animation",
    rows = 4,
    maxColumns = 4,
    animations = {
        idle = {0, 2, 0.5},
        walk = {1, 4, 0.25},
        walkWithStuff = {2, 4, 0.25},
        gather = {3, 2, 0.5}
    },
    health = 10,
    speed = 35
}