bush = {
    colorVariation = {35, 35, 35},
    textureType = "single",
    useShadow = true,

    shadowX = -7,
    shadowY = -5,
    shadowScale = 0.8,
    objectType = "plant",
    rotationSpeed = 4,
    rotationPower = 2,
    rotationRandomize = true,
}