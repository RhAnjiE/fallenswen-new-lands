cactus = {
    colorVariation = {50, 50, 50},
    textureType = "single",
    positionDiffY = 0,

    objectType = "plant",
    growMinTime = 10,
    growMaxTime = 20,
    rotationSpeed = 1.2,
    rotationPower = 1,
    rotationRandomize = true,

    buildBlock = false,
    movingBlock = true,

    useShadow = true,
    shadowX = -7,
    shadowY = -6,
    shadowScale = 0.5,
}
