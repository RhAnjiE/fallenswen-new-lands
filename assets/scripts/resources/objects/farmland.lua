farmland = {
    textureType = "multi",
    iconTexture = "icon_farmland",
    rows = 1,
    maxColumns = 4,
    positionDiffY = 14,
    colorVariation = {35, 35, 35},

    objectType = "plant",
    growMinTime = 10,
    growMaxTime = 20,
    rotationSpeed = 3,
    rotationPower = 1.5,
    rotationRandomize = false,

    buildBlock = false,
    useShadow = false,

    init = function(object)
        local tile = object.lowestTile

        if tile ~= nil then
            local r = 114 + math.random(-5, 5)
            local g = 61  + math.random(-3, 7)
            local b = 30  + math.random(-10, 7)

            tile:setColor(r, g, b)
        end
    end,

    onDestroy = function(object)
        local tile = object.tilesUnder[1]

        if tile ~= nil then
            tile:setDefaultColor()
        end
    end
}