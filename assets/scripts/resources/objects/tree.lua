tree = {
    colorVariation = {50, 50, 50},
    textureType = "multi",
    rows = 4,
    maxColumns = 1,
    positionDiffY = 6,

    objectType = "plant",
    growMinTime = 10,
    growMaxTime = 20,
    rotationSpeed = 2,
    rotationPower = 1.2,
    rotationRandomize = true,

    buildBlock = false,
    movingBlock = true,

    useShadow = true,
    shadowX = -8,
    shadowY = -10,
    shadowScale = 1.5,
}