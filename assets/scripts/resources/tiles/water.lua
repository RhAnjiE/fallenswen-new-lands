water = {
    colorBase = {116, 247, 250},
    positionDiffY = 3,
    textureType = "animation",
    synchronizedAnim = true,
    rows = 1,
    maxColumns = 4,
    animations = {idle = {0, 4, 0.8}},
    buildBlock = true
}
