dirt = {
    customTextureName = "terrain",
    colorBase = {184, 174, 143},
    colorVariation = {20, 20, 40},
    textureType = "multi",
    rows = 1,
    maxColumns = 3
}