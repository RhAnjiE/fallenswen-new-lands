sand = {
    customTextureName = "terrain",
    colorBase = {205, 207, 89},
    colorVariation = {20, 10, 40},
    textureType = "multi",
    rows = 1,
    maxColumns = 3
}