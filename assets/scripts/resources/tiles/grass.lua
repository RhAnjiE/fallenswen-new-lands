grass = {
    customTextureName = "terrain",
    colorBase = {141, 199, 60},
    colorVariation = {10, 30, 10},
    textureType = "multi",
    rows = 1,
    maxColumns = 3
}