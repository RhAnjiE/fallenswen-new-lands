cmake_minimum_required(VERSION 3.12)

# Project name
project(izoGame)
set(EXECUTABLE_NAME izoGame)

# Directories
set(DIR_SOURCE source)
set(DIR_BIN bin)

# C++17
set(CMAKE_CXX_STANDARD 17)
#set(GCC_COVERAGE_COMPILE_FLAGS "-D_GLIBCXX_DEBUG")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${GCC_COVERAGE_COMPILE_FLAGS}" )

# EXE
file(GLOB ALL_SOURCE_FILES
    "${DIR_SOURCE}/*"
    "${DIR_SOURCE}/utilities/*"
    "${DIR_SOURCE}/utilities/imgui/*"
    "${DIR_SOURCE}/utilities/graphics/*"
    "${DIR_SOURCE}/stages/*"
    "${DIR_SOURCE}/maps/*"
    "${DIR_SOURCE}/maps/tiles/*"
    "${DIR_SOURCE}/maps/objects/*"
    "${DIR_SOURCE}/maps/objects/people/*"
    "${DIR_SOURCE}/maps/objects/buildings/*"
    "${DIR_SOURCE}/gui/*"
    "${DIR_SOURCE}/gui/hud/*"
    "${DIR_SOURCE}/players/*"
    "${DIR_SOURCE}/people/*"
    "${DIR_SOURCE}/people/test/*"
)
add_executable(${EXECUTABLE_NAME} ${ALL_SOURCE_FILES})
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_CURRENT_LIST_DIR}/${DIR_BIN})
set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake-modules" ${CMAKE_MODULE_PATH})
include(${CMAKE_CURRENT_LIST_DIR}/cmake-modules/Config.cmake)

# filesystem
target_link_libraries(${EXECUTABLE_NAME} stdc++fs)

# LUA
include_directories(${LUA_ROOT})
target_link_libraries(${EXECUTABLE_NAME} ${LUA_LIBRARIES})

# Sol2 Library
include_directories(${SOL2_ROOT})

# SFML
include_directories("${CMAKE_CURRENT_LIST_DIR}/${SFML_ROOT}/include")
find_package(SFML COMPONENTS system window graphics network audio)
if(SFML_FOUND)
    include_directories(${SFML_INCLUDE_DIR})
    target_link_libraries(${EXECUTABLE_NAME} ${SFML_LIBRARIES})
endif()

# OpenGL
find_package(OpenGL REQUIRED)
target_link_libraries(${EXECUTABLE_NAME} ${OPENGL_LIBRARIES})

# ImGUI
include_directories(${IMGUI_ROOT})
file(GLOB IMGUI_LIBRARIES "${IMGUI_ROOT}/*")
#target_link_libraries(${EXECUTABLE_NAME} ${IMGUI_LIBRARIES})

